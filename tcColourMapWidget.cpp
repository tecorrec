/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcColourMapWidget.cpp
 * @brief Colour mapping widget.
 */

#include "tcColourMapWidget.h"

#include <QGridLayout>
#include <QButtonGroup>
#include <QSignalMapper>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>

/*
 * Constructors + destructor
 */

/// Construct with colour information.
tcColourMapWidget::tcColourMapWidget(const QStringList& outputBands, QWidget* parent)
: QScrollArea(parent)
, m_layout(0)
, m_inputSignalMapper(new QSignalMapper(this))
, m_inputGroupSignalMapper(new QSignalMapper(this))
, m_groupSignalMapper(new QSignalMapper(this))
, m_outputBandButtonGroups()
, m_numInputBands(0)
, m_numInputBandsPerGroup()
{
  QWidget* widget = new QWidget(this);
  m_layout = new QGridLayout(widget);
  setWidget(widget);
  setWidgetResizable(true);

  foreach (QString band, outputBands)
  {
    QButtonGroup* buttonGroup = new QButtonGroup(this);
    connect(buttonGroup, SIGNAL(buttonClicked(int)), m_groupSignalMapper, SLOT(map()));
    m_groupSignalMapper->setMapping(buttonGroup, m_outputBandButtonGroups.size());
    m_outputBandButtonGroups << buttonGroup;

    m_layout->addWidget(new QLabel(band, this), 0, m_outputBandButtonGroups.size(), Qt::AlignHCenter);
  }
  connect(m_groupSignalMapper, SIGNAL(mapped(int)), this, SLOT(inputBandChangedSlot(int)));
  connect(m_inputSignalMapper, SIGNAL(mapped(int)), this, SLOT(inputBandClickedSlot(int)));
  connect(m_inputGroupSignalMapper, SIGNAL(mapped(int)), this, SIGNAL(inputGroupClicked(int)));
}

/// Destructor.
tcColourMapWidget::~tcColourMapWidget()
{
}

/*
 * Main interface
 */

/// Clear bands.
void tcColourMapWidget::clearInputBands()
{
}

/// Add an input band.
void tcColourMapWidget::addInputBand(const QString& name, const QString& description)
{
  ++m_numInputBands;
  ++m_numInputBandsPerGroup.last();
  QPushButton* mainLabel = new QPushButton(name, this);
  mainLabel->setToolTip(description);
  m_layout->addWidget(mainLabel, m_numInputBands+m_numInputBandsPerGroup.size(), 0);
  connect(mainLabel, SIGNAL(clicked()), m_inputSignalMapper, SLOT(map()));
  m_inputSignalMapper->setMapping(mainLabel, m_numInputBands-1);
  for (int i = 0; i < m_outputBandButtonGroups.size(); ++i)
  {
    QRadioButton* radio = new QRadioButton(this);
    m_outputBandButtonGroups[i]->addButton(radio, m_numInputBands-1);
    m_layout->addWidget(radio, m_numInputBands+m_numInputBandsPerGroup.size(), 1 + i, Qt::AlignHCenter);
  }
}

/// Add an input band group separator.
void tcColourMapWidget::addInputGroupSeparator(const QString& name)
{
  m_numInputBandsPerGroup.push_back(0);
  QPushButton* mainLabel = new QPushButton(QObject::tr("*** %1 ***").arg(name), this);
  connect(mainLabel, SIGNAL(clicked()), m_inputGroupSignalMapper, SLOT(map()));
  m_inputGroupSignalMapper->setMapping(mainLabel, m_numInputBandsPerGroup.size()-1);
  m_layout->addWidget(mainLabel, m_numInputBands+m_numInputBandsPerGroup.size(), 0);
}

/*
 * Accessors
 */

/// Get the index of the input band assigned to an output band.
int tcColourMapWidget::inputBand(int outputBand) const
{
  return m_outputBandButtonGroups[outputBand]->checkedId();
}

/*
 * Public slots
 */

/// Set the input band assigned to an output.
void tcColourMapWidget::setInputBand(int output, int input)
{
  m_outputBandButtonGroups[output]->button(input)->setChecked(true);
}

/*
 * Private slots
 */

/// Indicates that an input band has been clicked.
void tcColourMapWidget::inputBandClickedSlot(int input)
{
  int group = inputGroup(&input);
  emit inputBandClicked(input, group);
}

/// Indicates that the input band assigned to an output has changed.
void tcColourMapWidget::inputBandChangedSlot(int output)
{
  int band = m_outputBandButtonGroups[output]->checkedId();
  int group = inputGroup(&band);
  emit inputBandChanged(output, band, group);
}

/*
 * Private functions
 */

/// Get the input group and adjust the band number to be relative to the input group.
int tcColourMapWidget::inputGroup(int* band)
{
  int result = -1;
  foreach (int bands, m_numInputBandsPerGroup)
  {
    ++result;
    if (*band < bands)
    {
      return result;
    }
    *band -= bands;
  }
  return result;
}
