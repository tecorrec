/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#include <QString>
#include <QByteArray>
#include <QFile>
#include <QRegExp>
#include <QtDebug>

#include <cmath>
#include <inttypes.h>

double readDouble(QFile& file)
{
  QByteArray line = file.readLine();
  static QRegExp intLine("^\\s*\\w+\\s+([-+]?\\d+(\\.\\d+)?)\\s*$");
  if (intLine.exactMatch(line))
  {
    return intLine.cap(1).toDouble();
  }
  else
  {
    return -1;
  }
}

int readInt(QFile& file)
{
  QByteArray line = file.readLine();
  static QRegExp intLine("^\\s*\\w+\\s+([-+]?\\d+)\\s*$");
  if (intLine.exactMatch(line))
  {
    return intLine.cap(1).toInt();
  }
  else
  {
    return -1;
  }
}

int arcToHgt(const QString filename, char* voids)
{
  qDebug() << "Reading" << filename;

  // first open arcinfo file
  QFile af(filename);
  if (!af.open(QIODevice::ReadOnly))
  {
    return 1;
  }

  // and read the header info
  int ncols = readInt(af);
  int nrows = readInt(af);
  double xllcorner = readDouble(af);
  double yllcorner = readDouble(af);
  double cellsize = readDouble(af);
  int nodata_value = readInt(af);

  // find corresponding HGT tiles
  int lonMin = (int)floor(xllcorner);
  int lonMax = (int)floor(xllcorner + cellsize*(ncols-1));
  int latMin = (int)floor(yllcorner);
  int latMax = (int)floor(yllcorner + cellsize*(nrows-1));
  int tilesX = 1+lonMax-lonMin;
  int tilesY = 1+latMax-latMin;
  QFile* srtm = new QFile[tilesX*tilesY];
  QFile* voidfs = 0;
  if (0 != voids)
  {
    voidfs = new QFile[tilesX*tilesY];
  }
  for (int lon = lonMin; lon <= lonMax; ++lon)
  {
    for (int lat = latMin; lat <= latMax; ++lat)
    {
      QString filename = QString("%3%4%5%6.hgt")
                                    .arg(lat >= 0 ? 'N' : 'S')
                                    .arg(abs(lat), 2, 10, QLatin1Char('0'))
                                    .arg(lon >= 0 ? 'E' : 'W')
                                    .arg(abs(lon), 3, 10, QLatin1Char('0'));
      int index = tilesX*(lat-latMin) + lon-lonMin;
      srtm[index].setFileName(filename);
      srtm[index].open(QIODevice::ReadWrite);
      if (0 != voids)
      {
        voidfs[index].setFileName(QString("%1/%2").arg(voids).arg(filename));
        voidfs[index].open(QIODevice::ReadOnly);
      }
      // if it doesn't exist, initialize with void
      if (srtm[index].atEnd())
      {
        qDebug() << "  Creating" << filename;
        QDataStream stream(&srtm[index]);
        stream.setByteOrder(QDataStream::BigEndian);
        uint16_t sample = 0x8000;
        for (int i = 0; i < 1201*1201; ++i)
        {
          stream << sample;
        }
      }
      else
      {
        qDebug() << "  Writing to" << filename;
      }
    }
  }

  // overwrite directly with new values

  QTextStream in(&af);
  // go through the rows
  for (int row = nrows-1; row >= 0; --row)
  {
    int y = (int)floor(0.5 + yllcorner/cellsize - latMin/cellsize + row);
    int latDiv = y/1200;
    int latMod = y%1200;
    for (int col = 0; col < ncols; ++col)
    {
      int x = (int)floor(0.5 + xllcorner/cellsize - lonMin/cellsize + col);
      int lonDiv = x/1200;
      int lonMod = x%1200;

      int value;
      in >> value;
      uint16_t sample = value;

      bool inX = (lonDiv >= 0 && lonDiv < tilesX);
      bool inY = (latDiv >= 0 && latDiv < tilesY);
      bool inXm1 = (lonDiv > 0 && lonDiv <= tilesX);
      bool inYm1 = (latDiv > 0 && latDiv <= tilesY);
#define FILEPOS(X,Y) (((1200-(Y))*1201 + (X))*2)
      if (inX && inY)
      {
        int index = latDiv*tilesX + lonDiv;
        // if void flag is set, adjust sample
        if (0 != voidfs && voidfs[index].isOpen())
        {
          QDataStream stream(&voidfs[index]);
          stream.setByteOrder(QDataStream::BigEndian);
          voidfs[index].seek(FILEPOS(lonMod, latMod));
          uint16_t mask;
          stream >> mask;
          if (mask & 0x8000)
          {
            sample |= 0x8000;
          }
        }
        QDataStream stream(&srtm[index]);
        stream.setByteOrder(QDataStream::BigEndian);
        srtm[index].seek(FILEPOS(lonMod, latMod));
        stream << sample;
      }
      bool edgeX = (lonMod == 0);
      bool edgeY = (latMod == 0);
      if (edgeX && inXm1 && inY)
      {
        int index = latDiv*tilesX + lonDiv-1;
        QDataStream stream(&srtm[index]);
        stream.setByteOrder(QDataStream::BigEndian);
        srtm[index].seek(FILEPOS(1200, latMod));
        stream << sample;
      }
      if (edgeY && inX && inYm1)
      {
        int index = (latDiv-1)*tilesX + lonDiv;
        QDataStream stream(&srtm[index]);
        stream.setByteOrder(QDataStream::BigEndian);
        srtm[index].seek(FILEPOS(lonMod, 1200));
        stream << sample;
      }
      if (edgeX && edgeY && inXm1 && inYm1)
      {
        int index = (latDiv-1)*tilesX + lonDiv-1;
        QDataStream stream(&srtm[index]);
        stream.setByteOrder(QDataStream::BigEndian);
        srtm[index].seek(FILEPOS(1200, 1200));
        stream << sample;
      }
    }
  }

  delete [] srtm;
  if (0 != voidfs)
  {
    delete [] voidfs;
  }

  return 0;
}

int main(int argc, char** argv)
{
  qDebug() << "Welcome to arcToHgt";
  int error = 0;
  // Convert the file in each argument to HGT, overwriting any existing data
  char * voids = 0;
  for (int i = 1; i < argc; ++i)
  {
    if (0 == strcmp(argv[i], "--voids"))
    {
      // next argument is voids directory
      if (++i < argc)
      {
        voids = argv[i];
        qDebug() << "Using voids from " << voids;
      }
    }
    else
    {
      error |= arcToHgt(argv[i], voids);
    }
  }
  return error;
}
