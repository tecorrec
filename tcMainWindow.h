/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcMainWindow_h_
#define _tcMainWindow_h_

/**
 * @file tcMainWindow.h
 * @brief Main application window.
 */

#include <QMainWindow>

class tcViewportWidget;
class tcColourMapWidget;
class tcGlobe;
class tcExportText;

/// Main application window.
class tcMainWindow : public QMainWindow
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcMainWindow();

    /// Destructor.
    virtual ~tcMainWindow();

  private slots:

    /*
     * Private slots
     */

    /// Show configuration for a channel.
    void configureChannel(int channel, int group);

    /// Export as text.
    void exportText();

  private:

    /*
     * Widgets
     */

    /// Main viewport.
    tcViewportWidget* m_viewport;

    /// Colour mapper.
    tcColourMapWidget* m_colourMap;

    /*
     * Objects
     */

    /// Globe object.
    tcGlobe* m_globe;

    /// Text export object.
    tcExportText* m_exportText;
};

#endif

