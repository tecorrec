/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcViewportWidget_h_
#define _tcViewportWidget_h_

/**
 * @file tcViewportWidget.h
 * @brief OpenGL viewport widget.
 */

#include <tcGeo.h>
#include <Vector.h>

#include <QGLWidget>
#include <QPointF>

class tcObserver;
class tcGlobe;

class QString;

/// OpenGL viewport widget.
class tcViewportWidget : public QGLWidget
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcViewportWidget(QWidget* parent);

    /// Destructor.
    virtual ~tcViewportWidget();

    /*
     * Modifiers
     */

    /// Set the globe object.
    void setGlobe(tcGlobe* globe);

    /*
     * Accessors
     */

    /// Find the coordinates under the mouse.
    tcGeo geoAt(float x, float y, bool* ok = 0);

  public slots:

    /*
     * Observer control
     */

    /// Change to sun view.
    void sunView(int imagery);

    /*
     * Interaction control
     */

    /// Enable navigation mode.
    void navigationMode();

    /// Enable texture point selection mode.
    void texturePointMode(QObject* receiver, const char* member);

    /// Set slice.
    void setSlice(const tcGeo& sw, const tcGeo& ne);

    /// Set cross section.
    void setCrossSection(const tcGeo& p1, const tcGeo& p2);

    /*
     * General rendering slots
     */

    /// Change the quality to adaptive.
    void setQualityAdaptive();

    /// Change the quality to full resolution.
    void setQualityFull();

    /// Change the polygon mode to normal.
    void setPolygonModeNormal();

    /// Change the polygon mode to wireframe.
    void setPolygonModeWireframe();

    /// Remove colour coding.
    void setColourCodingNone();

    /// Set colour coding to elevation sample alignment.
    void setColourCodingElevationSampleAlignment();

    /// Set colour mapping for an output channel to an input band.
    void setColourMapping(int output, int input, int inputGroup);

    /*
     * Elevation modification slots
     */

    /// Set the primary elevation data set name.
    void setPrimaryElevationDataSet(const QString& name);

    /// Set the secondary elevation data set name.
    void setSecondaryElevationDataSet(const QString& name);

    /// Go to flat primary elevation mode.
    void setPrimaryElevationFlat();

    /// Go to flat secondary elevation mode.
    void setSecondaryElevationFlat();

    /// Go to raw SRTM primary elevation mode.
    void setPrimaryElevationRaw();

    /// Go to raw SRTM secondary elevation mode.
    void setSecondaryElevationRaw();

    /// Go to corrected primary elevation mode.
    void setPrimaryElevationCorrected();

    /// Go to corrected secondary elevation mode.
    void setSecondaryElevationCorrected();

    /// Set the interpolation value.
    void setElevationInterpolation(int interpolation);

  signals:

    /*
     * Signals
     */

    /// Emitted when the mouse moves to a new position on the globe.
    void mouseGeoChanged(const tcGeo& geo);

    /** Emitted when the mouse moves to a new position on the globe.
     * This has an int so it is compatible with QStatusBar::showMessage slot.
     */
    void mouseGeoTextChanged(const QString& geo);

    /// Emitted when a texture point is selected.
    void texturePointSelected(const maths::Vector<2,float>& coord);

  protected:

    /*
     * Rendering
     */

    // Reimplemented
    virtual void initializeGL();

    // Reimplemented
    virtual void resizeGL(int w, int h);

    // Reimplemented
    virtual void paintGL();

    /*
     * Mouse events
     */

    // Reimplemented
    virtual void mouseMoveEvent(QMouseEvent* event);

    // Reimplemented
    virtual void mousePressEvent(QMouseEvent* event);

    // Reimplemented
    virtual void mouseReleaseEvent(QMouseEvent* event);

    // Reimplemented
    virtual void wheelEvent(QWheelEvent* event);

  private:

    /*
     * Rendering options
     */

    /// Adaptive quality.
    bool m_adaptiveQuality;

    /// OpenGL polygon mode.
    GLenum m_polygonMode;

    /*
     * Variables
     */

    /// Observer.
    tcObserver* m_observer;

    /// Globe to display.
    tcGlobe* m_globe;

    /*
     * Interaction
     */

    /// Interaction mode.
    enum
    {
      Navigation,
      TexturePoint
    } m_interactionMode;

    /// Are we currently interacting?
    bool m_mouseInteracting;

    /// Previous mouse position.
    QPointF m_mousePos;

    /// Mouse intersecting globe?
    bool m_mouseIntersecting;

    /// Current mouse coordinate.
    tcGeo m_mouseCoord;

    /// Slicing with the mouse?
    bool m_mouseSlicing;

    /// Cross sectioning with mouse?
    bool m_mouseCrossing;

    /// Start of mouse slicing action.
    tcGeo m_mouseStartCoord;

    /// Sliced?
    bool m_sliced;

    /// Coordinates of slice.
    tcGeo m_slice[2];

    /// Whether a cross section is selected.
    bool m_crossSectioned;

    /// Cross section.
    tcGeo m_crossSection[2];

    /// Texture point object.
    QObject* m_texturePointObject;

    /// Texture point slot/signal.
    const char* m_texturePointMember;
};

#endif

