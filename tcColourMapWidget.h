/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcColourMapWidget_h_
#define _tcColourMapWidget_h_

/**
 * @file tcColourMapWidget.h
 * @brief Colour mapping widget.
 */

#include <QScrollArea>
#include <QList>

class QSignalMapper;
class QGridLayout;
class QButtonGroup;

/// Colour mapping widget.
class tcColourMapWidget : public QScrollArea
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Construct with colour information.
    tcColourMapWidget(const QStringList& outputBands, QWidget* parent);

    /// Destructor.
    virtual ~tcColourMapWidget();

    /*
     * Main interface
     */

    /// Clear bands.
    void clearInputBands();

    /// Add an input band.
    void addInputBand(const QString& name, const QString& description);

    /// Add an input band group separator.
    void addInputGroupSeparator(const QString& name);

    /*
     * Accessors
     */

    /// Get the index of the input band assigned to an output band.
    int inputBand(int outputBand) const;

  public slots:

    /*
     * Public slots
     */

    /// Set the input band assigned to an output.
    void setInputBand(int output, int input);

  signals:

    /*
     * Signals
     */

    /// Emitted when an input band is clicked.
    void inputBandClicked(int input, int inputGroup);

    /// Emitted when an input group is clicked.
    void inputGroupClicked(int group);

    /// Emitted when the input band assigned to an output is changed.
    void inputBandChanged(int output, int input, int inputGroup);

  private slots:

    /*
     * Private slots
     */

    /// Indicates that an input band has been clicked.
    void inputBandClickedSlot(int input);

    /// Indicates that the input band assigned to an output has changed.
    void inputBandChangedSlot(int output);

  private:

    /*
     * Private functions
     */

    /// Get the input group and adjust the band number to be relative to the input group.
    int inputGroup(int* band);

  private:

    /*
     * GUI objects
     */

    /// Main layout of the widget.
    QGridLayout* m_layout;

    /// Input label signal mapper.
    QSignalMapper* m_inputSignalMapper;

    /// Input group signal mapper.
    QSignalMapper* m_inputGroupSignalMapper;

    /// Group signal mapper.
    QSignalMapper* m_groupSignalMapper;

    /*
     * Variables
     */

    /// Radio button groups for each output band.
    QList<QButtonGroup*> m_outputBandButtonGroups;

    /// Number of input colour bands.
    int m_numInputBands;

    /// Number of input bands per group.
    QList<int> m_numInputBandsPerGroup;
};

#endif

