% Load in the data
lit = load("-ascii", "shading_lit.adf");
unlit = load("-ascii", "shading_unlit.adf");
% apply acos to column 1 (shading)
lit(:,1) = acos(lit(:,1)')';
unlit(:,1) = acos(unlit(:,1)')';

sizel = size(lit);
sizeu = size(unlit);

lit_b = lit(:,2);
lit_g = lit(:,3);
lit_ = lit(:,4);
lit_mir1 = lit(:,6);
lit_ltir = lit(:,7);
lit_htir = lit(:,8);
lit_mir2 = lit(:,9);

A=(0:0.005:1.55)';

C=140*cos(A);
lit_choice = lit_ltir;

%C=115*cos(A);
%lit_choice = lit_htir;

C=100*cos(A);
lit_choice = (lit_mir1 + lit_mir2);

scatter([lit_choice(1:20:sizel(1),:);C], [lit(1:20:sizel(1),1);A]);

unlit_b = unlit(:,2);
unlit_g = unlit(:,3);
unlit_r = unlit(:,4);
unlit_nir = unlit(:,5);
unlit_mir1 = unlit(:,6);
unlit_ltir = unlit(:,7);
unlit_htir = unlit(:,8);
unlit_mir2 = unlit(:,9);

C=140*cos(A);
unlit_choice = unlit_ltir;

scatter([unlit_choice(1:5:sizeu(1),:);C], [unlit(1:5:sizeu(1),1);A]);
