Efficient rendering of terrain elevation model

start with some top level cells

Thursday 20th Nov
  terrain elev (errors)
    naive interpolation
  evaluation
    views from google earth
    qualitative
  image data + info viewing / illumination
    >1? - resampling
  resampling together

Thurday 27th Nov
  projection of imagery is not linear lon/lat
    use gdal library to open file and get projection, and translate between projections
  found another source of elevation data
    3arcsec srtm with voids filled in using high resolution relief maps
    1arcsec same format but made before the above with same maps
      higher res, but SRTM is more accurate
  elevation interpolation
    tried bilinear and bicubic
    quite a few anomolies

  meeting
    segmentation
      [ ] comparing two regions in different datasets
      [ ] get source from shadow guys

Monday 1st dec
  using discontinuity measure with mid range IR
    mir lights up rocks not in shadow, so discontinuity *(1-mir) could work
  initially working with discontinuity measurement
    saturation screws up with a lot of it
    its then that i realised that of course shadow is never saturated
    any saturated area can be discounted as non-shadow
    shadows are generally dark throughout the channels
    so multiply by (1- each channel value)
    low (unsnowed) vegetation still shows up as shadow
      is that because its shadows in the complexity of trees etc?
      these areas have accurate SRTM data
      perhaps we can use this to prove that they cannot be in shadow
    characteristics of a cast shadow as travel away from light
      area of light
      area of self shadow - we can find this from the elevation data
      area of cast shadow
      area of light
    therefore an area of light followed by an area of shadow
      without self shadow in between is false

Thursday 4th Dec
  raytracing
    simple, per pixel towards light until hits terrain
  edgetracing
    edges of sun visibility projected into floor
  lambertian shading
  meeting
    negative product might not work as with rgb, but quite good here
    doesn't work well in non snowy areas
    constraints transititions along light path of illuminated, self shadow, cast shadow

Monday 8th Dec
  shadow classification isn't great using simple method
  we could use some PAT techniques
    features could be log chromaticity values
    classifications are shadow/non-shadow
    could restrict to only pixels negative-product classifies as shadow
  interface
    menu export->condition in selection
    condition is channel and threshold (=0.5)
    list of available channels
    list of output channels
    buttons to transfer between, and move up and down
  generalised negative product
    now product of powers of negative channels
    can bias channels, so thermal IR has more effect at eliminating shadows than the others

Tuesday 9th Dec
  [ ] improve shadow classification using self-shadow -> cast-shadow constraint
  [ ] start terrain improvements
      [ ] firstly apply 

thursday 11th dec
  [X] error function in terms of parameters
  [ ] find derivitive (numerically)
  [X] gradient descent
  [X] threshold as another parameter or minimise threshold each step
  [ ] could plot wavelength - importance to shadow classification
  [X] whether throwing away channels would help - minimiser would solve
  [ ] test that constraint equations hold on training data (russion relief maps)
  [ ] take some lines in direction of sun, and do the plot
      [ ] smooth out small variations between shadow types
  [ ] ambient occlusion with hemispheres
      [ ] "shape from shading on a cloudy day"
  [ ] shape from shading - two images - intersection of cones
  [ ] scatter plot between angle and colour in shadows
      [ ] look good on report
  [ ] lit review before christmas

Sunday 14th Dec
  gradient descent
    hold off on thresholding
    error = number of incorrect classifications
    as threshold goes from 0 to 1, crossing a pixel value
      pixels in shadow go incorrect
      pixels in light go correct
    create a number of bins in range, e.g. for each slider value
    single pass through pixels
      if classified as shadow: +1 to bin
        as threshold hits that value, error will increase
      if classified as non-shadow: -1 from bin
        as threshold hits that value, error will decrease

mon 15th dec
  gradient descent comes up with something like:
    threshold: 2.47319e-6
    0.9, 1.2, 0.8, 2.6, 19.5, 14, 10.7, 8.5, 3.3
    when adapted to larger terrain, optimal threshold is: 1.8692e-6
    error ~6.32%
  another gradient descent:
    threshold: 0.000581442
    0.1, 0.8, 0.9, 0.9, 15.8, 7.2, 6.2, 6.9, 2.8
    error ~6.26%

tue 16th dec
  added elevation channel, showing void
  when change lambertian shading to light from top, and go high resolution
    do voids appear darker, i.e. significantly steeper?
    there's definitely some relationship, but its not entirely accurate

Thu 18th dec
  lit review
    about 6 papers per section
    terrain
      any attempt at estimation
      Edwin did radar shape from shading
      "francot" "chalaper" - surface integration
      shape from shading from snow covered terrain
    multispectral stuff
    srtm paper?
      yes
      section on data sources
        sensors etc
        adv/disadv, accuracy

  try gradient descent on big terrain area
    check if it holds generally
  scatter plots
    normal angle to sun / value
    normal z component / value

Thu 15th jan
  shadow edge dictionary - low level (finding edges) -> higher level (meaning)
  shadow rules
    will ?[cg]ause? any problem
    tree/graph/finite state automata
    tree of allowed rules
    Consistency - what to do when not
    cost with moving
    confidence in how good
    minimal cost change
    smoothness across
  general rules - state machine
    do sample - check consistent
    add shading to cost

cost function
  variables to include
    change of height values (appropriately scaled)
      (h-h0)^2 * certainty
    shadow entrance constraints
      difference of gradients to tangential
        (-dh/dw - sin(elev_sun))^2
      negative change in gradient
        max(0,d^2h/dw^2)^2
      elevation relative to transition exit, from elevation of sun
        (h_exit+(w_enter-w_exit)*sin(elev_sun) - h)^2
    total rate of change of gradient (smoothness) in both directions
      (d^2h/dx^2)^2
      (d^2h/dy^2)^2
    shadow elevation constraint - below line between entrance and exit
      (max(0, h - (h_exit*(w_exit-w) + h_enter*(w-w_enter)) / (w_exit-w_enter)))^2
    lit pixels must be facing the sun
      min(0, -dh/dw - sin(elev_sun))^2
    angle between normals and shape-from-shading/occlusion normals
      factoring in reliability of shape-from-shading (e.g. snow/rock)
  requirements
    quick access to a pixel's sun direction shadow entrance/exit
    elevation field upsampled to match imagery?

Thur 22nd Jan
  [ ] plot theta(N,L)/I to check shading is accurate
  for shadow and non shadow
  should look like cos in sun
  tell us albedo (x=0), reflection when face on
  [ ] what shape for shadow region?
  integrate over L for occlusion
    imagine hemisphere on surface (slanted)
    the bottom part is cut off (horizon)
    -- we integrate from one side to the other, across the slope
    -- integrate the L.N across the points in the arc of a circle
    albedo*L*int(0,pi, int(theta,pi, L.N dx) dy)
    -- N is (0,0,1)
    -- L is (cos(x)*sin(y), cos(y), sin(x)*sin(y))
    -- L.N = sin(x)*sin(y)
    albedo*L*int(0,pi, int(theta,pi, sin(x)*sin(y) dx) dy)
    -- sin(y) doesn't depend on x
    albedo*L*int(0,pi, sin(y)*int(theta,pi, sin(x) dx) dy)
    -- int sin(x) dx = -cos(x)
    albedo*L*int(0,pi, sin(y)*[-cos(pi)+cos(theta)] dy)
    -- cos(pi) = -1
    albedo*L*int(0,pi, sin(y)*[1+cos(theta)] dy)
    -- [1+cos(theta)] doesn't depend on y
    albedo*L*[1+cos(theta)]*int(0,pi, sin(y) dy)
    albedo*L*[1+cos(theta)]*[-cos(pi) + cos(0)]
    -- cos(pi) = -1, cos(0) = 1
    albedo*L*[1+cos(theta)]*2
    -- rearrange
    2*albedo*L*(1+cos(theta))

Monday 26th Jan
  so to implement the above cost function, we need at each pixel:
    current elevation (turn into height map)
    gradient in light directions
    rate of change of gradient in x, y, light direction
    whether classified as shadow, or lit
    whether on a boundary of shadow and light
    sun elevation and direction
    if in shadow:
      distance to and current elevation at shadow entrance and exit in light direction

Tue 27th Jan
  now we have shadow depth channel in each direction
  new channel to optimise elevation
    must be able to take multiple datasets
    also adjusts corrected elevation values in DEM
    inputs
      previous elevation (internal)
      shadow classification, shadow depths

Thur 29th Jan
  We really need to know distances accurately
  coordinate spaces
    geographical
      can calculate short distances easily, given radius
      can convert to texture space
    texture space
      can convert to geographical
      can't directly calculate distances
    ideally need to optimise in a geographical texture space
      resample shadow
  19:13 - tried with roughness, variance, lit facing and travision elev
    hard to get coefficients right so that it does the smoothing
    better result when learning rate is constant

Wed 4th Feb
  earlier in the week got all channels into geographical space, resampled
    on loading. this should make it more convenient to optimise using multiple
    data sets later on.
  now updates digital elevation model with optimisation
  now need to get optimisation improved!

Thur 5th Feb
  [ ] graphs lit/nonlit
  [ ] project title
      "shadow segmentation"
      "satellite imagery"
      "multispectral"
      "elevation correction"
      "void filling"
      "correction of terrain elevation data using shadow segmentation of multispectral imagery"
      "fine scale correction of terrain elevation data using terrain imagery"
  [ ] statement of ethics talk by alistair
  [ ] elevation correction
      [ ] tackling wall problem
          [ ] intermediate smoothing
          [ ] start at low resolution, and increase resolution as we get closer
              [ ] moving blocks of pixels at a time
          [ ] when moving pixels, move others in range, like blender does

Mon 9th Feb
  used falloff function when editing verticies
    gaussian is very very slow
    linear is much faster, still slow, but converges faster than before
    massive dip on each side because forgot to max(0,x)
  overshoots because no constraint to keep shadow areas below shadow line
    add shadow line constraint - not working yet, pushing terrain down

Tue 10th Feb
  landsat 5 data now loads
  need to make it possible to work on multiple datasets
    colour map widget
      [X] add groups to ColourMapWidget
          simple addGroupSeparator(const QString& name)
    [X] global channel manager for optimisation and terrain channels
    [X] elevation optimisation of multiple image datasets
    [ ] fix export to work on multiple datasets

Thur 12th Feb
  works on multiple datasets, slowly
  new problem, not updating textures during mid-optimization refresh
  optimization gui
    [ ] for each weight, also have constant/increase/decrease option
    [ ] setting for dh
    [ ] setting for linear falloff limit
  DEM gui
    [ ] keep DEM in memory when switching
    [ ] save corrected values
    [X] compare DEMs
    [ ] simplify - corrected values are separate DEM (primary, secondary)
        [X] select primary and secondary DEM
        [ ] void-fill performed on primary DEM
        [ ] corrections written to secondary DEM
        [ ] initialised with new secondary DEM copied from primary
        [ ] new elevation data
            [ ] select custom resolution
            [ ] copy from another DEM
        [X] select to display primary or secondary DEM, OR...
        [X] compare primary, secondary DEM
            [O] slider with partial updating of textures too (periodically)
            [ ] channel to compare primary and secondary

Mon 16th Feb
  need to get on with write up
  added DEM comparison channel - allows quantitative comparison of std deviation
  John Clark in CRY L10 Non standard cracking
    when optimizing, make problem harder to get better results
    in context of perceptron problem
    move boundary of punishment so values are further from border and
      less likely to cross it again soon
      may be very unlikely to solve strict problem but gives us better
        solution to our problem in the process
  Negative product optimization implications
    instead of error being number of incorrect pixels
      error is sum distance on wrong side of boundary
      e.g. if np=0.2, shadow, th=0.3, err=th/np=3/2
    [ ] could also try simulated anealing

thu 19th feb (02:10)
  got more image datasets with much lower solar elevation
    very pronounced shadows (a bit too big in fact!)
    haven't tried optimizing yet
    ideally need to get a good set of different solar positions
    plot solar positions in 2d plot to show variation
  it also occurred to me that for the purposes of testing the algorithm
    could generate shadow classification directly from raytracing with
    some specially chosen sun direction (pretending we've done the
    shadow classification correctly on some fictional image dataset)
    e.g. to get a higher azimuth than 160deg, its always morning atm

tue 24th feb (01:46)
  hard constrain to hard set some elevations from shadow constraints
    works well at reducing standard deviation
    accompanied by a bilinear filtering to reduce peaks

wed 25th feb
  found blackart - uses GTOPO30 data in interpolation, and gets better results
  cannot compare unless I also use GTOPO30 data
  easy way to accomplish this is if I export the elevation data
    read in data using blackart and see if the result is improved

thur
  scale intensity so brightest pixel is 1
  I=a(N.L) + b
  need to estimate a,b
  ACCV 07 - videoed sun shadows over day
    shape reconstruction from cast shadows using coplainarities and metric constraints
  graphs - using qwt
    spectrum under mouse as graph or histogram
    scatter plots
      choose axis channels
      choose axis functions (linear, acos)

thu 5th mar
  optimization working well, downloaded some cgiar data (a good interpolation method)
    void std deviation down by over 25%
    overall std deviation down by over 20%
    non-void tends to stay about the same
  results of shadow comparisons pretty good
  time to get some hard results
    [X] record (and load) exact lon lat selection
    [X] graph of error decreasing
    [ ] cross-section of ridge decreasing
    [X] snapshot terrain at intervals
    [ ] snapshot viewport at intervals
    [X] load snapshots
  what
    optimization from basic
    optimization from cgiar

fix list
[ ] seems - not important
[ ] when creating texture for region, don't up sample, select larger area
[ ] bilinear/bicubic filtering at edge of SRTM tile results in ridges

todo
[X] proper texturing
    [X] selection of channels to display
[ ] more sensible sampling of elevation data
    [X] edges of box should interpolate
    [X] inner vertices should be sensible
[ ] srtm
    [X] basic linear interpolation
    [X] spline interpolation
    [X] gui to toggle {flat, unprocessed, corrected -> refined}
    [ ] self shadow lines
        [ ] different colours for ridges and valleys
[ ] interaction
    [X] focus extended altitude
    [X] mouse click -> lon lat transformation
    [ ] terrain grab while dragging
    [X] selection of region
    [ ] region image preview (full resolution)
    [ ] multiple viewports
        [ ] interaction (observers) can be locked together
        [ ] data view can be locked together
[ ] image processing
    [ ] should be able to add processing bands to those on the colour mapper
    [X] input bands should be derivable from digital elevation model
        e.g. could do calculations in world space instead of texture space
        [X] get texture space normal at a pixel
        [X] get texture space lighting direction at a pixel
        [X] get elevation at a pixel
        [X] get geographical coordinate at a pixel
        examples
          [X] shadow from DEM
          [X] lambertian shading (automatic self shadow detection)
          [ ] combination of the two (pixel product)
          [ ] chromaticity based on lambertian shading shows colour
[ ] shadow detection
    [X] in region, detect shadow areas
        [X] from imagery
        [X] from elevation data using raytracing
    [ ] vectorisation
    [ ] border and highlight in both views

texturing
  problems
    textures are BIG
      8401 x 7461 - 63MB  x6
      4201 x 3731 - 16MB  x2
      16801 x 14921 - 250MB x1
      660MB
  solution
    downsample entire image to a reasonable size
      1/8 x 1/8 = 1/64th size = ~10MB
      or use the thumbnail in each image
    when a portion of the image is desired in higher detail
      reload portion of the image at full resolution
      1/8 x 1/8 = 1/64th size of image
    when a portion of the image is no longer being used
      discard of it

  class tcBigImage
    width, height
    thumbnail texture
    rows, cols
    cells[rows][cols]
      texture
      image data

image processing
  image data should be made up of channels
    some channels are source bands from GeoTIFF files
    some are derived from other channels
  tcChannelManager
    manages a set of channels that are referred to in a colour map widget
  tcChannel
    functionality for keeping data around so as not to have to reload
      after creating GL texture
    results can be floating point, bytes, bits
    histograms and various plots based on the pixel data
    data changed event triggers invalidation of textures
      and invalidation of channels derived from this channel
  tcChannelConstant : tcImageChannel
    constant colour - e.g. black (for a null channel to avoid clicking rgb each time)
  tcChannelFile : tcImageChannel
    data from a GeoTIFF file
  tcChannelProcess
    acts on a number of input channels to produce a number of output channels
    can be configured with a gui
  tcChannelProcessChromaticity : tcChannelProcess
    divide a set of channels by another channel or a value
    out: float
  tcChannelProcessIlluminantInvariant : tcChannelProcess
    input log chromaticities
    can configure illuminant direction
      if two dimentions, use a dial
      for > 2 dimentions, select a shadow / non shadow adjascent bit of image
        or select multiple such known shadow changes and average
        find variation vector from this
        easy to reuse this vector
  tcChannelProcessIlluminantDiscontinuity : tcChannelProcess
    see cic13
    shows discontinuity between adjascent pixels in x or y direction
    i.e. you might show two of these in different colour channels

renderCell
  determine if we need to subdivide quality in this cell
    if we're close, then we do
    find distance from each corner 
    use distance[i] < max(dist(corner[i], corner[j]))*factor
  if we should subdivide
    renderCell on each of the 4 subdivisions
  determine the quality at this level
    find distance to center
    apply magic function to find detail level
    render the cell in this detail level

