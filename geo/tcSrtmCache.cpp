/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcSrtmCache.cpp
 * @brief Manages cache of SRTM data.
 */

#include "tcSrtmCache.h"
#include "tcSrtmData.h"
#include "tcElevationData.h"

#include <QString>
#include <QStringList>
#include <QFile>

#include <cstdlib>

/*
 * Static variables
 */

QDir tcSrtmCache::s_dataDirectory("/home/james/cs/pro/data/srtm");

/*
 * Static functions
 */

/// Get a list of datasets.
QStringList tcSrtmCache::dataSets()
{
  return s_dataDirectory.entryList(QDir::AllDirs | QDir::NoDotAndDotDot, QDir::Name);
}

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcSrtmCache::tcSrtmCache(const QString& dataSet)
: m_dataSet(dataSet)
{
  for (int i = 0; i < 360; ++i)
  {
    for (int j = 0; j < 180; ++j)
    {
      m_cache[i][j] = 0;
    }
  }
}

/// Destructor.
tcSrtmCache::~tcSrtmCache()
{
  for (int i = 0; i < 360; ++i)
  {
    for (int j = 0; j < 180; ++j)
    {
      if ((tcSrtmData*)0x1 != m_cache[i][j])
      {
        delete m_cache[i][j];
      }
    }
  }
}

/*
 * Main interface
 */

/// Load raw SRTM height file.
tcSrtmData* tcSrtmCache::loadData(int lon, int lat)
{
  tcSrtmData*& cacheItem = cache(lon, lat);
  if (0 == cacheItem)
  {
    QString filename = QString("%1/%2/%3%4%5%6.hgt")
                                  .arg(s_dataDirectory.path())
                                  .arg(m_dataSet)
                                  .arg(lat >= 0 ? 'N' : 'S')
                                  .arg(abs(lat), 2, 10, QLatin1Char('0'))
                                  .arg(lon >= 0 ? 'E' : 'W')
                                  .arg(abs(lon), 3, 10, QLatin1Char('0'));
    QFile file(filename);
    if (file.open(QIODevice::ReadOnly))
    {
      cacheItem = new tcSrtmData(lon, lat, file, this, SIGNAL(progressing(const QString&)));
    }
    else
    {
      // 0x1 represents a previous failed attempt
      cacheItem = (tcSrtmData*)0x1;
    }
  }
  if ((tcSrtmData*)0x1 != cacheItem)
  {
    return cacheItem;
  }
  else
  {
    return 0;
  }
}

/// Load raw SRTM height file.
tcSrtmData* tcSrtmCache::loadData(const tcGeo& coord)
{
  int lon,lat;
  toIntLonLat(coord, &lon, &lat);
  return loadData(lon, lat);
}

/** Find some SRTM height data overlapping an area.
 * @param swCorner Geographical coordinate of south-west corner.
 * @param neCorner Geographical coordinate of north-east corner.
 * @returns An arbitrary SRTM data for a chunk in the lon lat range.
 */
tcSrtmData* tcSrtmCache::findData(const tcGeo& swCorner, const tcGeo& neCorner)
{
  int swLon,swLat;
  int neLon,neLat;
  toIntLonLat(swCorner, &swLon, &swLat);
  toIntLonLat(neCorner, &neLon, &neLat);
  for (int lon = swLon; lon <= neLon; ++lon)
  {
    for (int lat = swLat; lat <= neLat; ++lat)
    {
      tcSrtmData* result = loadData(lon,lat);
      if (0 != result)
      {
        return result;
      }
    }
  }
  return 0;
}

/// Update any samples affected by the elevation field.
void tcSrtmCache::updateFromElevationData(const tcElevationData* elevation)
{
  tcGeo swCorner = elevation->swCorner();
  tcGeo neCorner = elevation->neCorner();
  int mini = (int)floor(swCorner.lon()*180.0/M_PI) + 180;
  int maxi = (int) ceil(neCorner.lon()*180.0/M_PI) + 180;
  int minj = (int)floor(swCorner.lat()*180.0/M_PI) + 90;
  int maxj = (int) ceil(neCorner.lat()*180.0/M_PI) + 90;
  for (int ii = mini; ii < maxi; ++ii)
  {
    int i = ii % 360;
    for (int j = minj; j < maxj; ++j)
    {
      if (0 != m_cache[i][j] && (tcSrtmData*)0x1 != m_cache[i][j])
      {
        m_cache[i][j]->updateFromElevationData(elevation);
      }
    }
  }
}

/*
 * Private functions
 */

/// Convert a geographical coordinate into lon lat ints.
void tcSrtmCache::toIntLonLat(const tcGeo& coord, int* const outLon, int* const outLat)
{
  int lat = (int)floor(coord.lat() * 180.0/M_PI);
  int lon = (int)floor(coord.lon() * 180.0/M_PI);
  lat = lat % 360;
  if (lat < -90)
  {
    lat += 360;
  }
  if (lat >= 90)
  {
    lat -= 180;
    lon += 180;
  }
  lon = lon % 360;
  if (lon < -180)
  {
    lon += 360;
  }
  else if (lon >= 180)
  {
    lon -= 360;
  }
  *outLon = lon;
  *outLat = lat;
}

/// Cache index.
tcSrtmData*& tcSrtmCache::cache(int lon, int lat)
{
  Q_ASSERT(lon >= -180 && lon < 180);
  Q_ASSERT(lat >= -90 && lat < 90);
  return m_cache[180 + lon][90 + lat];
}
