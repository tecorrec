/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcChannelFile_h_
#define _tcChannelFile_h_

/**
 * @file tcChannelFile.h
 * @brief An image channel from an image file.
 */

#include "tcChannel.h"
#include "tcGeo.h"
#include "tcAffineTransform.h"

class GDALDataset;
class OGRCoordinateTransformation;

/// An image channel from an image file.
class tcChannelFile : public tcChannel
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcChannelFile(const QString& filename, const QString& name, const QString& description, float radianceOffset = 0.0f, float radianceBias = 1.0f);

    /// Destructor.
    virtual ~tcChannelFile();

    /*
     * Accessors
     */

    /// Load the transformation from projection to texture.
    const tcAffineTransform2<double> geoToTex() const;

    /// Load the transformation from projection to texture.
    const tcAffineTransform2<double> texToGeo() const;

    /*
     * Main image interface
     */

    // Reimplemented
    virtual GLuint thumbnailTexture();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  private:

    /*
     * Variables
     */

    /// Width of the image file.
    int m_width;

    /// Height of the image file.
    int m_height;

    /// GDAL dataset object.
    GDALDataset* m_dataset;

    /// Thumbnail texture.
    GLuint m_thumbnail;

    /// Transformation from projection to geographical coordinates.
    OGRCoordinateTransformation* m_projToGeo;

    /// Transformation from geographical coordinates to projection.
    OGRCoordinateTransformation* m_geoToProj;

    /// Transformation from projection to pixel, lines.
    tcAffineTransform2<double> m_pixelsToProj;

    /// Transformation from pixel, lines to projection.
    tcAffineTransform2<double> m_projToPixels;

    /// Transformation from texture to geo.
    tcAffineTransform2<double> m_texToGeo;

    /// Transformation from geo to texture.
    tcAffineTransform2<double> m_geoToTex;
};

#endif
