/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcShadowDepth.cpp
 * @brief 2D depth into shadow along light direction.
 */

#include "tcShadowDepth.h"

#include <QObject>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcShadowDepth::tcShadowDepth(tcChannel* lit, tcSrtmModel* dem, tcGeoImageData* imagery, bool exitDepth)
: tcChannelDem(dem, imagery,
               tr("Shadow depth"),
               tr("2D depth into shadows along light direction."),
               false)
, m_litChannel(lit)
, m_exitDepth(exitDepth)
{
  m_litChannel->addDerivitive(this);
}

/// Destructor.
tcShadowDepth::~tcShadowDepth()
{
  m_litChannel->removeDerivitive(this);
}

/*
 * Interface for derived class to implement
 */

#include <QtDebug>
void tcShadowDepth::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  m_litChannel->roundPortion(x1,y1,x2,y2);
}

tcAbstractPixelData* tcShadowDepth::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  Reference<tcAbstractPixelData> channelData = m_litChannel->loadPortion(x1, y1, x2, y2, changed);
  int width = channelData->width();
  int height = channelData->height();

  // Create a new pixel buffer
  tcPixelData<float>* data = new tcPixelData<float>(width, height);
  startProcessing(tr("Depthing shadows"));
  /*for (int i = 0; i < width*height; ++i)
  {
    data->buffer()[i] = -2.0f;
  }*/

  tempData temp;
  temp.x1 = x1;
  temp.y1 = y1;
  temp.x2 = x2;
  temp.y2 = y2;
  temp.width = width;
  temp.height = height;
  temp.lit = channelData;
  temp.shadowDepths = data;

  int maxWidthHeight = qMax(width, height);
  double minXY12 = qMin(x2-x1, y2-y1);

  Reference<tcPixelData<float> > litData = dynamicCast<tcPixelData<float>*>(channelData);
  if (litData != 0)
  for (int j = 0; j < height; ++j)
  {
    progress((float)j/(height-1));
    for (int i = 0; i < width; ++i)
    {
      //pixelShadowDepth(&data, i, j);

      int index = j*width + i;

      // Not actually in shadow
      if (litData->buffer()[index] > 0.5f)
      {
        data->buffer()[index] = 0.0f;
      }
      // In shadow
      else
      {
        maths::Vector<2,float> coord      (x1 + (x2-x1)*i / (width  - 1),
                                           y1 + (y2-y1)*j / (height - 1));
        /*
         * Cx=x1+(x2-x1)*Tx
         * Cy=1-y1-(y2-y1)*Ty
         * Tx=(Cx-x1)/(x2-x1)
         * Ty=(1-y1-Cy)/(y2-y1)
         */

        // Find local light direction
        tcGeo geoCoord = geoAt(coord);
        maths::Vector<3,float> light = lightDirectionAt(geoCoord);

        // This appears to be almost accurate: the light vector in texture space
        // Perhaps this is the size of the texture not being taken into account
        tcGeo offset2(geoAt(coord + maths::Vector<2,float>(-light[0]/(maxWidthHeight-1)*minXY12,
                                                           light[1]/(maxWidthHeight-1)*minXY12)));

        // This doesn't appear to be accurate: the light vector in geographical space
        //float magn = maths::Vector<2,float>(offset1.lon()-geoCoord.lon(), offset1.lat()-geoCoord.lat()).mag();
        //tcGeo offset2(geoCoord + tcGeo(light[0]*magn,
        //                               light[1]*magn));

        // Trace in sun direction until we reach non shadow.
        tcGeo pos = geoCoord;
        tcGeo delta;
        if (m_exitDepth)
        {
          delta = offset2 - geoCoord;
        }
        else
        {
          delta = geoCoord - offset2;
        }
        pos = pos + delta;
        maths::Vector<2,float> tex = coord;
        while (tex[0] >= x1 && tex[0] <= x2 && tex[1] >= y1 && tex[1] <= y2)
        {
          if (channelData->sampleFloat((tex[0]-x1)/(x2-x1), (tex[1]-y1)/(y2-y1)) > 0.5f)
          {
            break;
          }
          pos = pos + delta;
          tex = textureAt(pos);
        }
        data->buffer()[index] = (tex-coord).mag()*200;
      }
    }
  }
  endProcessing();
  return data;
}

/*
 * Private functions
 */

/// Calculate the shadow depth of a pixel, recursively as necessary.
float tcShadowDepth::pixelShadowDepth(const tempData* data, int i, int j)
{
  int index = j*data->width + i;
  float& depth = data->shadowDepths->buffer()[index];

  // Already set, just return that
  if (depth >= 1.0f)
  {
    return depth;
  }

  // Not actually in shadow
  if (data->lit->sampleFloat(i, j) > 0.5f)
  {
    return 0.0f;
  }

  // In shadow, not yet calculated depth

  // Find local light direction
  maths::Vector<2,float> coord      (data->x1 + (data->x2-data->x1)*i / (data->width  - 1),
                                     data->y1 + (data->y2-data->y1)*j / (data->height - 1));
  tcGeo geoCoord = geoAt(coord);
  maths::Vector<3,float> light = lightDirectionAt(geoCoord);

  int coords[2][2];
  float weights[2];

  // Pick an axis direction
  if (light[1] < light[0]) // SE
  {
    if (-light[1] < light[0]) // E
    {
      coords[0][0] = i+1; coords[0][1] = j;
      weights[1] = fabs(light[1]/light[0]);
    }
    else // S
    {
      coords[0][0] = i; coords[0][1] = j+1;
      weights[1] = fabs(light[0]/light[1]);
    }
  }
  else // NW
  {
    if (-light[1] < light[0]) // W
    {
      coords[0][0] = i-1; coords[0][1] = j;
      weights[1] = fabs(light[1]/light[0]);
    }
    else // N
    {
      coords[0][0] = i; coords[0][1] = j-1;
      weights[1] = fabs(light[0]/light[1]);
    }
  }

  // Pick a diagonal direction
  if (light[0] > 0.0f) // E (NE,SE)
  {
    if (light[1] > 0.0f) // NE
    {
      coords[1][0] = i+1; coords[1][1] = j-1;
    }
    else // SE
    {
      coords[1][0] = i+1; coords[1][1] = j+1;
    }
  }
  else // W (NW,SW)
  {
    if (light[1] > 0.0f) // NW
    {
      coords[1][0] = i-1; coords[1][1] = j+1;
    }
    else // SW
    {
      coords[1][0] = i-1; coords[1][1] = j+1;
    }
  }
  weights[0] = 1.0f - weights[1];

  // Get depths of two points.
  float depths[2];
  for (int ii = 0; ii < 2; ++ii)
  {
    if (coords[ii][0] >= 0 && coords[ii][0] < data->width &&
        coords[ii][1] >= 0 && coords[ii][1] < data->height)
    {
      depths[ii] = pixelShadowDepth(data, coords[ii][0], coords[ii][1]);
    }
    else
    {
      depths[ii] = -1.0f;
    }
  }

  // Each depth can be:
  // -1 (out of range or unknown)
  // zero (lit)
  // positive (unlit, x 2d distance from boundary)

  // If both are unknown, result is unknown
  if (depths[0] == -1.0f && depths[1] == -1.0f)
  {
    depth = -1.0f;
  }
  // If one is unknown, use whichever has highest weight
  else if (depths[0] == -1.0f || depths[1] == -1.0f)
  {
    
  }
  // Normal weighting
  else
  {
    depth = depths[0]*weights[0] + depths[1]*weights[1];
  }
  return depth;
}
