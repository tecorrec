/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcShadowFreeChromaticities.cpp
 * @brief Remove the shadows from a set of chromaticities.
 */

#include "tcShadowFreeChromaticities.h"
#include "tcChannel.h"

#include <QObject>

#include <cmath>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcShadowFreeChromaticities::tcShadowFreeChromaticities(const QList<tcChannel*>& chromaticities)
: tcIlluminantDirection(chromaticities, chromaticities.size(), tr("Shadow free chromaticities"), tr("Re-chromatised illuminant invariant"))
{
  for (int i = 0; i < chromaticities.size(); ++i)
  {
    channels()[i]->setName(tr("sfc(%1)").arg(chromaticities[i]->name()));
    channels()[i]->setDescription(tr("Shadow-free chromaticity of %1").arg(chromaticities[i]->name()));
  }
}

/// Destructor.
tcShadowFreeChromaticities::~tcShadowFreeChromaticities()
{
}

/*
 * Interface for derived class to implement
 */

void tcShadowFreeChromaticities::loadPortions(const QList< Reference< tcPixelData<float> > >& chromaticities, int width, int height)
{
  QList< Reference< tcPixelData<float> > > newChromaticityData;
  for (int i = 0; i < numChromaticities(); ++i)
  {
    Reference< tcPixelData<GLfloat> > newPixelData = new tcPixelData<GLfloat>(width, height);
    newChromaticityData += newPixelData;
    m_portions += newPixelData;
  }

  // Go through the pixels
  maths::VarVector<float> logChromaticity(numChromaticities());
  maths::VarVector<float> temp(numChromaticities());
  for (int i = 0; i < width*height; ++i)
  {
    // Fill log chromaticity vector
    for (int channel = 0; channel < numChromaticities(); ++channel)
    {
      logChromaticity[channel] = logf(chromaticities[channel]->buffer()[i]);
    }

    // Find the dot of the chromaticity values with the illuminant direction vector
    float illuminant = logChromaticity * illuminantDirection();

    // Subtract this * illuminant direction vector from original value to cancel out
    temp = illuminantDirection();
    temp *= illuminant;
    logChromaticity -= temp;

    // Copy the data for now
    for (int channel = 0; channel < numChromaticities(); ++channel)
    {
      newChromaticityData[channel]->buffer()[i] = expf(logChromaticity[channel]);
    }
  }
}
