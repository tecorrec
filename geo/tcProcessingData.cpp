/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcProcessingData.cpp
 * @brief Processing data channels.
 */

#include "tcProcessingData.h"
#include "tcShadowClassifyingData.h"
#include "tcSrtmModel.h"
#include "tcChannelManager.h"
#include "tcLambertianShading.h"
#include "tcElevation.h"
#include "tcElevationDifference.h"
#include "tcElevationOptimization.h"
#include "tcNormals.h"
#include "tcPixelOp.h"

#include <QObject>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcProcessingData::tcProcessingData(const QList<tcShadowClassifyingData*>& datasets, tcSrtmModel* dem)
: tcGeoImageData()
{
  setName(QObject::tr("Processing channels"));
  //setTexToGeo(datasets[0]->texToGeo());

  tcGeoImageData* refImagery = datasets[0];
  tcChannel* reference = refImagery->channelManager()->channel(0);

  tcLambertianShading* upshading = new tcLambertianShading(reference, dem, this, maths::Vector<3,float>(0.0f, 0.0f, 1.0f));
  channelManager()->addChannel(upshading);

  tcElevation* elevation1 = new tcElevation(refImagery, reference, dem, this);
  channelManager()->addChannel(elevation1);

  tcElevation* elevation2 = new tcElevation(refImagery, reference, dem+1, this);
  channelManager()->addChannel(elevation2);

  tcNormals* normals = new tcNormals(refImagery, reference, dem, this);
  channelManager()->addChannel(normals);

  tcElevationDifference* elevationDifference = new tcElevationDifference(refImagery, reference, dem, this);
  channelManager()->addChannel(elevationDifference);

  tcElevationOptimization* elevationOpt = new tcElevationOptimization(datasets, dem, this);
  channelManager()->addChannel(elevationOpt);

  tcPixelOp* elevDiff = new tcPixelOp(tcPixelOp::Diff, QList<tcChannel*>() << elevationOpt << elevation2);
  channelManager()->addChannel(elevDiff);
}

/// Destructor.
tcProcessingData::~tcProcessingData()
{
}
