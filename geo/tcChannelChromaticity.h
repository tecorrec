/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcChannelChromaticity_h_
#define _tcChannelChromaticity_h_

/**
 * @file tcChannelChromaticity.h
 * @brief An image processing channel of the chromaticity between two channels.
 */

#include "tcChannel.h"

class tcChannelConfigWidget;

/// An image processing channel of the chromaticity between two channels.
class tcChannelChromaticity : public tcChannel
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcChannelChromaticity(tcChannel* numerator, tcChannel* denominator);

    /// Destructor.
    virtual ~tcChannelChromaticity();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  private:

    /*
     * Variables
     */

    /// Numerator channel.
    tcChannel* m_numerator;

    /// Denominator channel.
    tcChannel* m_denominator;
};

#endif
