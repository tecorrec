/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcSensor_h_
#define _tcSensor_h_

/**
 * @file tcSensor.h
 * @brief Description of an electromagnetic sensor.
 */

class tcSpectrum;

/// Description of an electromagnetic sensor.
class tcSensor
{
  public:

    /*
     * Types
     */

    /// Types of data.
    enum SensorType
    {
      Photographic = 0,
    };

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcSensor(SensorType type);

    /// Destructor.
    virtual ~tcSensor();

  private:

    /*
     * Variables
     */

    /// Type of sensor.
    SensorType m_type;

    /// Wavelength sensitivity distribution.
    tcSpectrum* m_sensitivity;

};

#endif

