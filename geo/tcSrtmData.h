/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcSrtmData_h_
#define _tcSrtmData_h_

/**
 * @file tcSrtmData.h
 * @brief Block of raw SRTM elevation data.
 */

#include "tcGeo.h"

#include <QObject>

#include <inttypes.h>

class tcElevationData;

class QFile;
class QString;

/// Block of raw SRTM elevation data.
class tcSrtmData : public QObject
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /** Load SRTM data.
     * @param lon Longitude in degrees.
     * @param lat Latitude in degrees.
     * @param file File to read data from.
     */
    tcSrtmData(int lon, int lat, QFile& file, QObject* progressObj, const char* progressMember);

    /// Destructor.
    virtual ~tcSrtmData();

    /*
     * Accessors
     */

    /// Get the longitude in degrees.
    int lon() const;

    /// Get the latitude in degrees.
    int lat() const;

    /// Get the number of scan lines.
    int lines() const;

    /// Get the number of samples in each scan line.
    int samples() const;

    /// Find the geographical angles between samples.
    tcGeo sampleResolution() const;

    /// Get the geographical coordinate of a sample.
    tcGeo sampleCoordinate(int line, int sample) const;

    /// Does data exist for a sample?
    bool sampleExists(int line, int sample) const;

    /// Get the altitude at a sample (interpolating gaps).
    uint16_t sampleAltitude(int line, int sample, bool corrected = false, bool* exists = 0) const;

    /// Get the altitude at a coordinate.
    float sampleAltitude(const tcGeo& coord, bool corrected = false, bool* exists = 0) const;

    /*
     * Mutators
     */

    /// Correct the altitude at a sample.
    void setSampleAltitude(int line, int sample, uint16_t altitude);

    /// Update any samples affected by the elevation field.
    void updateFromElevationData(const tcElevationData* elevation);

    /// Fill voids using bilinear interpolation.
    void fillVoidsBilinear();

    /// Fill voids using bicubic interpolation.
    void fillVoidsBicubic();

  signals:

    /*
     * Signals
     */

    /// Process notifier.
    void progressing(const QString& message);

  private:

    /*
     * Private functions
     */

    /// Index into the data array.
    unsigned int indexData(int corrected, int line, int sample) const;

  private:

    /*
     * Variables
     */

    /// Longitude in degrees.
    int m_lat;

    /// Latitude in degrees.
    int m_lon;

    /// Number of scan lines.
    int m_lines;

    /// Number of samples per scan line.
    int m_samples;

    /** Fixed resolution elevation data.
     * Indexed by correction, scanline, sample.
     * Most significant bit means that original data is missing.
     */
    uint16_t* m_data;
};

#endif

