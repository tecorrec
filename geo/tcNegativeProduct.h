/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcNegativeProduct_h_
#define _tcNegativeProduct_h_

/**
 * @file tcNegativeProduct.h
 * @brief Highlights the parts of an image which are dark in all channels.
 */

#include "tcChannel.h"

#include <QList>

class QSlider;
class QSpinBox;
class QLabel;
class QComboBox;

/// Highlights the parts of an image which are dark in all channels.
class tcNegativeProduct : public tcChannel
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcNegativeProduct(const QList<tcChannel*>& inputChannels);

    /// Destructor.
    virtual ~tcNegativeProduct();

    /*
     * Main image interface
     */

    // Reimplemented
    virtual tcChannelConfigWidget* configWidget();

    /*
     * Parameters
     */

    /// Load parameters.
    void loadParams(const QString& filename);

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  signals:

    /*
     * Signals
     */

    /// Emitted when the threshold text changes.
    void thresholdChanged(const QString& threshold);

  private slots:

    /*
     * Private slots
     */

    /// Set whether the theshold is enabled.
    void setThresholdEnabled(bool thresholdEnabled);

    /// Move the theshold value slider.
    void moveThreshold(int threshold);

    /// Set the theshold value.
    void setThreshold(int threshold);

    /// Indicates that the power slider of a channel has moved.
    void movePower(int channel);

    /// Indicates that the power of a channel has changed.
    void changePower(int channel);

    /// Optimize the threshold for the parameters using a classification channel.
    void optimizeThreshold();

    /// Save threshold errors.
    void saveThresholdErrors();

    /// Optimize the parameters using a classification channel.
    void optimizeParameters();

    /// Calculate errors.
    void calculateError();

    /// Save parameters to a file.
    void saveParams();

    /// Load parameters from a file.
    void loadParams();

  private:

    /*
     * Private functions
     */

    /// Calculate an error using some channel as a classifier.
    float calculateError(tcChannel* classifier);

  private:

    /*
     * Variables
     */

    /// Input channels.
    QList<tcChannel*> m_inputChannels;

    /// Whether threshold is enabled.
    bool m_thresholdEnabled;

    /// Threshold to classify shadows.
    float m_threshold;

    /// Powers to raise the negative channels to.
    float* m_powers;

    /// Filename in which to save threshold-error values.
    QString m_saveThresholdError;

    /// Filename in which to save parameter-error values.
    QString m_saveParametersError;

    /// Filename in which to save parameters.
    QString m_saveParameters;

    /// Configuration widget.
    tcChannelConfigWidget* m_configWidget;

    /// Threshold slider.
    QSlider* m_sliderThreshold;

    /// Power sliders in config widget.
    QList<QSlider*> m_powerSliders;

    /// Power sliders labelsin config widget.
    QList<QLabel*> m_powerLabels;

    /// Classifier channel.
    QComboBox* m_comboClassifier;

    /// Slider for log learning rate.
    QSlider* m_sliderLearningRate;

    /// Spinbox for subsampling.
    QSpinBox* m_subsample;

    /// Label to show error.
    QLabel* m_labelError;

    /// Last calculated optimal threshold error.
    int m_optimalThresholdError;
};

#endif
