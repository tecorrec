/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcObserver.cpp
 * @brief Viewing information for an observer.
 */

#include "tcObserver.h"
#include <glMatrix.h>

#include <GL/gl.h>

#include <cmath>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcObserver::tcObserver()
: m_focus(0.0, 0.0)
, m_focusAltitude(1000e3)
, m_view(0, 0.99f * M_PI/2)
, m_range(100e3)
{
}

/// Destructor.
tcObserver::~tcObserver()
{
}

/*
 * Main interface
 */

/// Set up the OpenGL projection matrix for this observer.
void tcObserver::setupProjection(double aspect) const
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  #define SIZE 10.0
  #define NEAR_DISTANCE 10.0
  glFrustum(-SIZE*aspect, SIZE*aspect, -SIZE, SIZE, NEAR_DISTANCE, 20000e3);
  glMatrixMode(GL_MODELVIEW);
}

void drawAxes(float size)
{
  glPushMatrix();
  glScalef(size, size, size);
  glBegin(GL_LINES);
  {
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, 0.0f, 0.0f);

    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);

    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
  }
  glEnd();

  glPointSize(5);
  glEnable(GL_POINT_SMOOTH);
  glBegin(GL_POINTS);
  {
    glColor3f(1.0f, 1.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);

    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, 0.0f, 0.0f);

    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);

    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
  }
  glEnd();
  glPointSize(1);
  glPopMatrix();
}

/// Set up the OpenGL modelview matrix for this observer.
void tcObserver::setupModelView() const
{
  glLoadIdentity();

  // Translate to focus
  glTranslatef(0.0f, 0.0f, -m_range);

  //drawAxes(50e3f);

  // Rotate to surface space
  glMultMatrix(m_view);

  //drawAxes(50e3f);

  // Translate to origin
  glTranslatef(0.0f, 0.0f, -m_focusAltitude);

  // Rotate to model space
  glMultMatrix(m_focus);

  //drawAxes(1000e3f);
}

/*
 * Mutators
 */

/// Adjust the focus directly.
void tcObserver::setFocus(const tcGeo& focus, double altitude)
{
  m_focus = focus;
  m_focusAltitude = altitude;
}

/// Adjust the focus altitude.
void tcObserver::setFocusAltitude(double altitude)
{
  m_focusAltitude = altitude;
}

/// Make a local transformation of the focus.
void tcObserver::moveFocusRelative(double dx, double dy)
{
  dx *= 2.0 * m_range / m_focusAltitude;
  dy *= 2.0 * m_range / m_focusAltitude;
  m_focus.setLat(m_focus.lat() - sin(m_view.azim()) * dx
                               + cos(m_view.azim()) * dy);
  m_focus.setLon(m_focus.lon() - cos(m_view.azim()) * dx
                               - sin(m_view.azim()) * dy);
}

/// Adjust the range exponentially.
void tcObserver::adjustRange(double x)
{
  m_range *= exp(x);
}

/// Adjust the azimuth in radians.
void tcObserver::adjustAzimuth(double daz)
{
  m_view.setAzim(m_view.azim() + daz);
}

/// Adjust the elevation in radians.
void tcObserver::adjustElevation(double del)
{
  m_view.setElev(m_view.elev() + del);
}

/// Change the viewing angle.
void tcObserver::setView(const tcGeo& view)
{
  m_view = view;
}

/*
 * Accessors
 */

/// Get the current focus position.
tcGeo tcObserver::focus() const
{
  return m_focus;
}

/// Get the current altitude at the focus.
double tcObserver::focusAltitude() const
{
  return m_focusAltitude;
}

/// Get the current range from the focus.
double tcObserver::range() const
{
  return m_range;
}

/// Get the position of the actual observer.
maths::Vector<3,double> tcObserver::position() const
{
  // transform m_view*m_range+(0,0,m_focusAltitude) into object space
  maths::Vector<3,double> focusOrientedObs = m_view;
  focusOrientedObs *= m_range;
  focusOrientedObs[2] += m_focusAltitude;
  return focusOrientedObs * m_focus.operator maths::Matrix<3,double>();
}

/// Get a ray into the scene from a point on the screen.
maths::Vector<3,double> tcObserver::ray(const maths::Vector<2,double>& pos, double aspect)
{
  // Vector to near plane
  maths::Vector<3,double> toNear(SIZE*aspect*(-1.0 + 2.0*pos[0]),
                                 SIZE*(-1.0 + 2.0*pos[1]),
                                 -NEAR_DISTANCE);
  // Normalize
  toNear.normalize();
  // Rotate into modelview
  return (toNear * m_view.operator maths::Matrix<3,double>()) * m_focus.operator maths::Matrix<3,double>();
}
