/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcGeoImageData.cpp
 * @brief Terrain image data.
 */

#include "tcGeoImageData.h"
#include "tcChannelManager.h"
#include "tcChannel.h"
#include <glVector.h>

#include <ogr_spatialref.h>

#include <QObject>

/*
 * Constructors + destructor
 */

/// Default constructor.
tcGeoImageData::tcGeoImageData()
: tcGeoData(tcGeoData::ImageData)
, m_channelManager(new tcChannelManager())
, m_texToGeo()
, m_geoToTex()
, m_effectiveTexToGeo()
, m_geoToEffectiveTex()
, m_sensor(0)
, m_name(QObject::tr("Unamed geographical image data"))
, m_sunDirection(0.0, 1.0, 0.0)
, m_channelSetup()
, m_effectiveNormals()
{
  for (int i = 0; i < 3; ++i)
  {
    m_channelSetup[i] = false;
  }
}

/// Destructor.
tcGeoImageData::~tcGeoImageData()
{
  delete m_channelManager;
}

/*
 * Accessors
 */

/// Get a name for this image data.
const QString& tcGeoImageData::name() const
{
  return m_name;
}

/// Get the channel manager.
tcChannelManager* tcGeoImageData::channelManager() const
{
  return m_channelManager;
}

/// Get the sun direction.
maths::Vector<3,float> tcGeoImageData::sunDirection(const tcGeo& coord) const
{
  return (maths::Vector<3,float>)(coord.operator maths::Matrix<3,double>() * m_sunDirection);
}

/*
 * Main interface
 */
    
/// Get the affine transformation from texture to geo.
const tcAffineTransform2<double> tcGeoImageData::texToGeo() const
{
  return m_texToGeo;
}

/// Get the affine transformation from geo to texture.
const tcAffineTransform2<double> tcGeoImageData::geoToTex() const
{
  return m_geoToTex;
}

/// Get the affine transformation from effective texture to geo.
const tcAffineTransform2<double> tcGeoImageData::effectiveTexToGeo() const
{
  return m_effectiveTexToGeo;
}

/// Get the affine transformation from geo to effective texture.
const tcAffineTransform2<double> tcGeoImageData::geoToEffectiveTex() const
{
  return m_geoToEffectiveTex;
}

/*
 * Rendering
 */

void tcGeoImageData::setupThumbnailRendering(int numChannels, int* channels)
{
  Q_ASSERT(numChannels >= 3);

  float colours[3][4] = {{ 1.0f, 0.0f, 0.0f, 1.0f },
                         { 0.0f, 1.0f, 0.0f, 1.0f },
                         { 0.0f, 0.0f, 1.0f, 1.0f }};
  for (int i = 0; i < 3; ++i)
  {
    if (channels[i] >= 0 && channels[i] < channelManager()->numChannels())
    {
      m_channelSetup[i] = true;
      glActiveTexture(GL_TEXTURE0 + i);
      glEnable(GL_TEXTURE_2D);
      glBindTexture(GL_TEXTURE_2D, channelManager()->channel(channels[i])->thumbnailTexture());
      /// @todo Do this in a non NV specific way
      // result = previous*1 + texture*colour
      glTexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, colours[i]);

      glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE4_NV);
      glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_ADD);
      if (i == 0)
      {
        glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_ZERO);
        glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND0_RGB, GL_SRC_COLOR);
        glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_ZERO);
        glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND1_RGB, GL_SRC_COLOR);
      }
      else
      {
        glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PREVIOUS);
        glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND0_RGB, GL_SRC_COLOR);
        glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_ZERO);
        glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND1_RGB, GL_ONE_MINUS_SRC_COLOR);
      }
      glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE2_RGB, GL_TEXTURE);
      glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND2_RGB, GL_SRC_COLOR);
      glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE3_RGB_NV, GL_CONSTANT);
      glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND3_RGB_NV, GL_SRC_COLOR);
    }
    else
    {
      m_channelSetup[i] = false;
    }
  }

  setEffectiveTexToGeo(texToGeo());
}

void tcGeoImageData::setupDetailedRendering(int numChannels, int* channels,
                                           const tcGeo& swCorner, const tcGeo& neCorner)
{
  Q_ASSERT(numChannels >= 3);

  float colours[3][4] = {{ 1.0f, 0.0f, 0.0f, 1.0f },
                         { 0.0f, 1.0f, 0.0f, 1.0f },
                         { 0.0f, 0.0f, 1.0f, 1.0f }};

  tcGeo seCorner(neCorner.lon(), swCorner.lat());
  tcGeo nwCorner(swCorner.lon(), neCorner.lat());
  // Translate coordinates, we don't care about innacuracies along the edges
  maths::Vector<2,double> swTexCoord = geoToTex() * swCorner;
  maths::Vector<2,double> neTexCoord = geoToTex() * neCorner;
  double r1x = swTexCoord[0];
  double r1y = neTexCoord[1];
  double r2x = neTexCoord[0];
  double r2y = swTexCoord[1];

  // Load any additionals
  for (int i = 3; i < numChannels; ++i)
  {
    if (channels[i] >= 0 && channels[i] < channelManager()->numChannels())
    {
      Reference<tcAbstractPixelData> data = channelManager()->channel(channels[i])->portion(&r1x, &r1y, &r2x, &r2y);
      m_effectiveNormals[i-3] = dynamicCast<tcTypedPixelData<maths::Vector<3,float> >*>(data);
    }
  }
  for (int i = 0; i < 3; ++i)
  {
    if (channels[i] >= 0 && channels[i] < channelManager()->numChannels())
    {
      m_channelSetup[i] = true;
      // Only bother if we're overlapping the interesting bit
      if (r1x < 1.0 && r1y < 1.0 && r2x > 0.0 && r2y > 0.0)
      {
        GLuint texture = channelManager()->channel(channels[i])->portionTexture(&r1x, &r1y, &r2x, &r2y);
        if (0 != texture)
        {
          glActiveTexture(GL_TEXTURE0 + i);
          glEnable(GL_TEXTURE_2D);
          glBindTexture(GL_TEXTURE_2D, texture);
          /// @todo Do this in a non NV specific way
          // result = previous*1 + texture*colour
          glTexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, colours[i]);

          glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE4_NV);
          glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_ADD);
          if (i == 0)
          {
            glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_ZERO);
            glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND0_RGB, GL_SRC_COLOR);
            glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_ZERO);
            glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND1_RGB, GL_SRC_COLOR);
          }
          else
          {
            glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PREVIOUS);
            glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND0_RGB, GL_SRC_COLOR);
            glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_ZERO);
            glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND1_RGB, GL_ONE_MINUS_SRC_COLOR);
          }
          glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE2_RGB, GL_TEXTURE);
          glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND2_RGB, GL_SRC_COLOR);
          glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE3_RGB_NV, GL_CONSTANT);
          glTexEnvi(GL_TEXTURE_ENV,   GL_OPERAND3_RGB_NV, GL_SRC_COLOR);
        }
      }
    }
    else
    {
      m_channelSetup[i] = false;
    }
  }

  setEffectiveTexToGeo(tcAffineTransform2<double>(nwCorner, seCorner-nwCorner));
}

void tcGeoImageData::setupNormalRendering()
{
  for (int i = 0; i < 3; ++i)
  {
    glActiveTexture(GL_TEXTURE0 + i);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    m_channelSetup[i] = false;
  }
  glActiveTexture(GL_TEXTURE0);
}

void tcGeoImageData::texCoord(const tcGeo& coord)
{
  // Coordinates need offsetting and scaling
  maths::Vector<2,double> texCoord = geoToEffectiveTex() * coord;
  for (int i = 0; i < 3; ++i)
  {
    if (m_channelSetup[i])
    {
      glMultiTexCoord2(GL_TEXTURE0 + i, texCoord);
    }
  }
}

maths::Vector<3,float> tcGeoImageData::normalAt(int norm, const tcGeo& coord)
{
  Q_ASSERT(norm >= 0 && norm < 3);
  if (0 != m_effectiveNormals[norm])
  {
    maths::Vector<2,double> texCoord = geoToEffectiveTex() * coord;
    return m_effectiveNormals[norm]->interpolateLinear(texCoord[0], texCoord[1]);
  }
  else
  {
    return maths::Vector<3,float>(0.0f);
  }
}

void tcGeoImageData::finishRendering()
{
  for (int i = 0; i < 3; ++i)
  {
    m_effectiveNormals[i] = 0;
  }
}

/*
 * Protected mutators
 */

/// Set the texture to geo transformation.
void tcGeoImageData::setTexToGeo(const tcAffineTransform2<double>& texToGeo)
{
  m_texToGeo = texToGeo;
  m_geoToTex = texToGeo.inverse();
}

/// Set the effective texture to geo transformation.
void tcGeoImageData::setEffectiveTexToGeo(const tcAffineTransform2<double>& effectiveTexToGeo)
{
  m_effectiveTexToGeo = effectiveTexToGeo;
  m_geoToEffectiveTex = effectiveTexToGeo.inverse();
}

/// Set the name.
void tcGeoImageData::setName(const QString& name)
{
  m_name = name;
}

/// Set the sun direction.
void tcGeoImageData::setSunDirection(const tcGeo& sunDirection, const tcGeo& sunReference)
{
  m_sunDirection = sunReference * (maths::Vector<3,double>)sunDirection;
}
