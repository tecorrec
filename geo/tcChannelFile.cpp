/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcChannelFile.cpp
 * @brief An image channel from an image file.
 */

#include "tcChannelFile.h"
#include "tcPixelData.h"

#include <gdal_priv.h>
#include <ogr_spatialref.h>

#include <QtDebug>

#include <GL/glu.h>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcChannelFile::tcChannelFile(const QString& filename, const QString& name, const QString& description, float radianceOffset, float radianceBias)
: tcChannel(name, description)
, m_width(0)
, m_height(0)
, m_dataset(0)
, m_thumbnail(0)
, m_projToGeo(0)
, m_geoToProj(0)
, m_pixelsToProj()
, m_projToPixels()
, m_texToGeo()
, m_geoToTex()
{
  setRadianceTransform(radianceOffset, radianceBias);

  // Open the file
  /// @todo this probably isn't the best place to set it up
  GDALAllRegister();

  m_dataset = (GDALDataset*)GDALOpen((const char*)filename.toAscii(), GA_ReadOnly);
  if (0 != m_dataset)
  {
    GDALRasterBand* band = m_dataset->GetRasterBand(1);
    Q_ASSERT(0 != band);
    m_width = band->GetXSize();
    m_height = band->GetYSize();

    OGRCoordinateTransformation* result = 0;
    if (0 != m_dataset->GetProjectionRef())
    {
      OGRSpatialReference srs(m_dataset->GetProjectionRef());
      OGRSpatialReference* geog = srs.CloneGeogCS();
      m_projToGeo = OGRCreateCoordinateTransformation(&srs, geog);
      m_geoToProj = OGRCreateCoordinateTransformation(geog, &srs);
      delete geog;
      m_dataset->GetGeoTransform(m_pixelsToProj);
      GDALInvGeoTransform(m_pixelsToProj, m_projToPixels);

      // Transform each corner
      maths::Vector<2,double> corners[4] = {
        maths::Vector<2,double>( 0.0,       0.0        ),
        maths::Vector<2,double>( 0.0,       m_height-1 ),
        maths::Vector<2,double>( m_width-1, 0.0        ),
        maths::Vector<2,double>( m_width-1, m_height-1 )
      };
      maths::Vector<2,double> min, max;
      for (int i = 0; i < 4; ++i)
      {
        maths::Vector<2,double> lonlat = m_pixelsToProj * corners[i];
        m_projToGeo->Transform(1, &lonlat[0], &lonlat[1]);
        // Keep track of extents in geographical space
        if (i > 0)
        {
          for (int j = 0; j < 2; ++j)
          {
            if (lonlat[j] < min[j])
            {
              min[i] = lonlat[j];
            }
            else if (lonlat[j] > max[j])
            {
              max[j] = lonlat[j];
            }
          }
        }
        else
        {
          for (int j = 0; j < 2; ++j)
          {
            min[j] = max[j] = lonlat[j];
          }
        }
      }
      // Convert extents into radians
      min *= M_PI/180;
      max *= M_PI/180;
      // Texture is mirrored in y
      qSwap(min[1], max[1]);
      // Get affine transformation matrices
      m_texToGeo = tcAffineTransform2<double>(min, max-min);
      m_geoToTex = m_texToGeo.inverse();
    }
  }
}

/// Destructor.
tcChannelFile::~tcChannelFile()
{
  delete m_dataset;
  if (0 != m_thumbnail)
  {
    glDeleteTextures(1, &m_thumbnail);
  }

  delete m_projToGeo;
  delete m_geoToProj;
}

/*
 * Accessors
 */

/// Load the transformation from projection to texture.
const tcAffineTransform2<double> tcChannelFile::geoToTex() const
{
  return m_geoToTex;
}

/// Load the transformation from projection to texture.
const tcAffineTransform2<double> tcChannelFile::texToGeo() const
{
  return m_texToGeo;
}

/*
 * Main image interface
 */

// Reimplemented
GLuint tcChannelFile::thumbnailTexture()
{
  if (0 == m_thumbnail && 0 != m_dataset)
  {
    GDALRasterBand* band = m_dataset->GetRasterBand(1);
    Q_ASSERT(0 != band);

    // Get an overview 
    GDALRasterBand* thumbnailBand = 0;
    int numOverviews = band->GetOverviewCount();
    if (0 == numOverviews)
    {
      
      // we want to reduce to around 1000000 pixels
      /// @todo Do we need to be careful of overflow here?
      int pixels = m_width*m_height;
      int overview = (int)sqrt(pixels >> 22);
      if (overview > 1)
      {
        if (CE_None != m_dataset->BuildOverviews("NEAREST", 1, &overview, 0, 0,
              GDALDummyProgress, 0))
        {
          thumbnailBand = band->GetOverview(0);
        }
      }
    }
    else
    {
      /// @todo Pick an appropriate overview if more than one
      thumbnailBand = band->GetOverview(0);
    }

    if (thumbnailBand)
    {
      // Turn into a GL texture
      int width  = thumbnailBand->GetXSize();
      int height = thumbnailBand->GetYSize();
      int tnWidth = 2048;
      int tnHeight = 2048;
      tcPixelData<GLubyte> data(tnWidth, tnHeight);
      tcPixelData<GLubyte> geoData(tnWidth, tnHeight);
      if (CE_None == thumbnailBand->RasterIO(GF_Read, 0, 0, width, height,
                                             data.buffer(), tnWidth, tnHeight, GDT_Byte, 0, 0))
      {
        int index = 0;
        for (int j = 0; j < tnHeight; ++j)
        {
          for (int i = 0; i < tnWidth; ++i)
          {
            // Geo coordinates
            maths::Vector<2,double> tex((double)i/(tnWidth-1), (double)j/(tnHeight-1));
            maths::Vector<2,double> geo = (m_texToGeo * tex) * 180.0f/M_PI;
            // Local coordinates
            m_geoToProj->Transform(1, &geo[0], &geo[1]);
            // To pixel coordinates
            maths::Vector<2,double> pix = m_projToPixels*(maths::Vector<2,double>)geo;
            geoData.buffer()[index] = (GLubyte)data.tcPixelData<GLubyte>::sampleFloat(pix[0]/(m_width-1), pix[1]/(m_height-1));
            ++index;
          }
        }
        glGenTextures(1, &m_thumbnail);
        glBindTexture(GL_TEXTURE_2D, m_thumbnail);
        if (0 == gluBuild2DMipmaps(GL_TEXTURE_2D, 1, tnWidth, tnHeight, GL_LUMINANCE, GL_UNSIGNED_BYTE, geoData.buffer()))
        {
          glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
          glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
          glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
          glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
          glBindTexture(GL_TEXTURE_2D, 0);
        }
        else
        {
          glBindTexture(GL_TEXTURE_2D, 0);
          glDeleteTextures(1, &m_thumbnail);
          m_thumbnail = 0;
        }
      }
    }
  }
  return m_thumbnail;
}

/*
 * Interface for derived class to implement
 */

void tcChannelFile::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  // We're resampling the texture anyway so no need to adjust range
}

tcAbstractPixelData* tcChannelFile::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  tcPixelData<GLubyte>* buffer = 0;
  if (0 != m_dataset)
  {
    GDALRasterBand* band = m_dataset->GetRasterBand(1);
    Q_ASSERT(0 != band);

    // Transform each corner
    tcAffineTransform2<double> portionToTex(maths::Vector<2,double>(x1,y1), maths::Vector<2,double>(x2-x1, y2-y1));
    tcAffineTransform2<double> portionToGeo = m_texToGeo * portionToTex;
    maths::Vector<2,double> corners[4] = {
      maths::Vector<2,double>( x1, y1 ),
      maths::Vector<2,double>( x1, y2 ),
      maths::Vector<2,double>( x2, y1 ),
      maths::Vector<2,double>( x2, y2 )
    };
    maths::Vector<2,double> min, max;
    for (int i = 0; i < 4; ++i)
    {
      maths::Vector<2,double> lonlat = m_texToGeo * corners[i];
      lonlat *= 180.0f/M_PI;
      m_geoToProj->Transform(1, &lonlat[0], &lonlat[1]);
      maths::Vector<2,double> pix = m_projToPixels * lonlat;
      // Keep track of extents in pixel space
      if (i > 0)
      {
        for (int j = 0; j < 2; ++j)
        {
          if (pix[j] < min[j])
          {
            min[j] = pix[j];
          }
          else if (pix[j] > max[j])
          {
            max[j] = pix[j];
          }
        }
      }
      else
      {
        min = max = pix;
      }
    }
    // Round min down and max up
    min[0] = floor(min[0]);
    min[1] = floor(min[1]);
    max[0] = ceil(max[0]);
    max[1] = ceil(max[1]);
    // Keep within limits of original texture
    if (min[0] < 0.0)        { min[0] = 0.0;        }
    if (min[1] < 0.0)        { min[1] = 0.0;        }
    if (max[0] > m_width-1)  { max[0] = m_width-1;  }
    if (max[1] > m_height-1) { max[1] = m_height-1; }
    // Get affine transformation matrices
    tcAffineTransform2<double> outOfWindow = tcAffineTransform2<double>(min, max-min);
    tcAffineTransform2<double> inToWindow = outOfWindow.inverse();

    int s1 = (int)min[0];
    int t1 = (int)min[1];
    int partWidth  = (int)max[0] - s1;
    int partHeight = (int)max[1] - t1;

    int resw = partWidth;
    int resh = partHeight;
    // ensure resw and resh are powers of 2
    for (int i = 1; i < 20; ++i)
    {
      if (0 == (resw & ~((1<<i)-1)))
      {
        resw = 1<<i;
        break;
      }
    }
    for (int i = 1; i < 20; ++i)
    {
      if (0 == (resh & ~((1<<i)-1)))
      {
        resh = 1<<i;
        break;
      }
    }
    qDebug() << "Extract resolution from" << partWidth << "x" << partHeight << "to" << resw << "x" << resh;
    tcPixelData<GLubyte> data(resw, resh);
    if (CE_None == band->RasterIO(GF_Read, s1, t1, partWidth, partHeight,
          data.buffer(), resw, resh, GDT_Byte, 0, 0))
    {

      buffer = new tcPixelData<GLubyte>(resw, resh);
      int index = 0;
      for (int j = 0; j < resh; ++j)
      {
        for (int i = 0; i < resw; ++i)
        {
          // Geo coordinates
          maths::Vector<2,double> portion((double)i/(resw-1), (double)j/(resh-1));
          maths::Vector<2,double> tex = portionToTex * portion;
          maths::Vector<2,double> geo = (m_texToGeo * tex) * 180.0f/M_PI;
          // Local coordinates
          m_geoToProj->Transform(1, &geo[0], &geo[1]);
          // To pixel coordinates
          maths::Vector<2,double> pix = m_projToPixels * geo;
          maths::Vector<2,double> window = inToWindow * pix;
          buffer->buffer()[index] = (GLubyte)data.tcPixelData<GLubyte>::sampleFloat(window[0], window[1]);
          //buffer->buffer()[index] = 255.0f*portion[0];//data.buffer()[index];
          ++index;
        }
      }
    }
  }
  return buffer;
}
