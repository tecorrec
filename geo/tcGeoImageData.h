/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcGeoImageData_h_
#define _tcGeoImageData_h_

/**
 * @file tcGeoImageData.h
 * @brief Terrain image data.
 */

#include "tcGeoData.h"
#include "tcAffineTransform.h"
#include "tcPixelData.h"
#include "CountedReference.h"

#include <QString>
#include <QStringList>

class tcSensor;
class tcChannelManager;
class OGRCoordinateTransformation;

/// Terrain image data.
class tcGeoImageData : public tcGeoData
{
  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcGeoImageData();

    /// Destructor.
    virtual ~tcGeoImageData();

    /*
     * Accessors
     */

    /// Get a name for this image data.
    const QString& name() const;

    /// Get the channel manager.
    tcChannelManager* channelManager() const;

    /// Get the sun direction.
    maths::Vector<3,float> sunDirection(const tcGeo& coord) const;

    /*
     * Main interface
     */
    
    /// Get the affine transformation from texture to geo.
    const tcAffineTransform2<double> texToGeo() const;

    /// Get the affine transformation from geo to texture.
    const tcAffineTransform2<double> geoToTex() const;

    /// Get the affine transformation from effective texture to geo.
    const tcAffineTransform2<double> effectiveTexToGeo() const;

    /// Get the affine transformation from geo to effective texture.
    const tcAffineTransform2<double> geoToEffectiveTex() const;

    /*
     * Rendering
     */

    /// Set up rendering using thumbnails.
    virtual void setupThumbnailRendering(int numChannels, int* channels);

    /// Set up rendering using a custom generated high resolution image.
    virtual void setupDetailedRendering(int numChannels, int* channels,
                                        const tcGeo& swCorner, const tcGeo& neCorner);

    /// Set up rendering for normals.
    virtual void setupNormalRendering();

    /// Provide a geographical texture coordinate.
    virtual void texCoord(const tcGeo& coord);

    /// Finds the normal at a geographical coordinate during rendering.
    virtual maths::Vector<3,float> normalAt(int norm, const tcGeo& coord);

    /// Unbind any textures etc.
    virtual void finishRendering();

  protected:

    /*
     * Protected mutators
     */

    /// Set the texture to geo transformation.
    void setTexToGeo(const tcAffineTransform2<double>& texToGeo);

    /// Set the effective texture to geo transformation.
    void setEffectiveTexToGeo(const tcAffineTransform2<double>& effectiveTexToGeo);

    /// Set the name.
    void setName(const QString& name);

    /// Set the sun direction.
    void setSunDirection(const tcGeo& sunDirection, const tcGeo& sunReference);

  private:

    /*
     * Variables
     */

    /// Name of the image data.
    QString m_name;

    /// Channel manager object.
    tcChannelManager* m_channelManager;

    /// Transformation from full texture to geo.
    tcAffineTransform2<double> m_texToGeo;

    /// Transformation from geo to full texture.
    tcAffineTransform2<double> m_geoToTex;

    /// Transformation from effective texture to geo.
    tcAffineTransform2<double> m_effectiveTexToGeo;

    /// Transformation from geo to effective texture.
    tcAffineTransform2<double> m_geoToEffectiveTex;

    /// Sensors which collected the data.
    tcSensor* m_sensor;

    /// Direction of the sun relative to earth.
    maths::Vector<3,double> m_sunDirection;

    /// Whether each channel is set up for rendering.
    bool m_channelSetup[3];

    /// The current normal data.
    Reference<tcTypedPixelData<maths::Vector<3,float> > > m_effectiveNormals[3];
};

#endif

