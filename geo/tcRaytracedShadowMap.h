/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcRaytracedShadowMap_h_
#define _tcRaytracedShadowMap_h_

/**
 * @file tcRaytracedShadowMap.h
 * @brief Shadow map using ray tracing of DEM.
 */

#include "tcChannelDem.h"
#include "tcAffineTransform.h"

/// Shadow map using ray tracing of DEM.
class tcRaytracedShadowMap : public tcChannelDem
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcRaytracedShadowMap(tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery);

    /// Primary constructor.
    tcRaytracedShadowMap(const tcGeo& resolution, tcSrtmModel* dem, tcGeoImageData* imagery);

    /// Destructor.
    virtual ~tcRaytracedShadowMap();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  private:

    /*
     * Variables
     */

    /// We'll use the same resolution as this reference channel.
    tcChannel* m_referenceChannel;

    /// Resolution if no reference channel is provided.
    tcGeo m_resolution;

    /// Texture to geographical coordinate transform.
    tcAffineTransform2<double> m_texToGeo;
};

#endif
