/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcAffineTransform.cpp
 * @brief Affine transformation between 2d linear coordinate spaces.
 */

#include "tcAffineTransform.h"

#include <gdal_priv.h>

/// Find the inverse.
template <>
tcAffineTransform2<double> tcAffineTransform2<double>::inverse() const
{
  /// @todo Implement independently of GDAL
  /*
   * [a,b,c;d,e,f].[1;x;y] = [n;t]
   * n = a + b*x + c*y (1)
   * t = d + e*x + f*y (2)
   * (rearrange to make x subject of (2))
   * x = (t - d - f*y)/e (3)
   * (substitude (3) into (1))
   * n = a + b*(t - d - f*y)/e + c*y (4)
   * (rearrange to make y subject of (4))
   * n = a + b*(t - d)/e - b*f*y/e + c*y
   * n = a + b*(t - d)/e + y*(c - b*f/e)
   * y = (n - a + b*(d - t)/e ) / (c - b*f/e)
   * y = (n + (b*d/e - a) - t*b/e ) / (c - b*f/e)
   * (rearrange to make y subject of ..)
   * ..
   * [1;x;y] = [g,h,i;j,k,m].[n;t]
   * j = (b*d/e - a) / (c - b*f/e)
   * k = 1 / (c - b*f/e)
   * m = - b/(e*c - b*f)
   */
  tcAffineTransform2 result;
  GDALInvGeoTransform((double*)&m_transform[0][0], result);
  return result;
}
