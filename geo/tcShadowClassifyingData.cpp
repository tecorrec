/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcShadowClassifyingData.cpp
 * @brief Image data set which provides a shadow classification channel.
 */

#include "tcShadowClassifyingData.h"
#include "tcChannel.h"

/*
 * Constructors + destructor
 */

/// Default constructor.
tcShadowClassifyingData::tcShadowClassifyingData()
: tcGeoImageData()
, m_shadowClassification(0)
, m_shading(0)
{
}

/// Destructor.
tcShadowClassifyingData::~tcShadowClassifyingData()
{
}

/*
 * Accessors
 */

/// Get the shadow classification channel.
tcChannel* tcShadowClassifyingData::shadowClassification()
{
  return m_shadowClassification;
}

/// Get shading channel.
tcChannel* tcShadowClassifyingData::shading()
{
  return m_shading;
}

/*
 * Derived class interface
 */

/// Set shadow classification channel.
void tcShadowClassifyingData::setShadowClassification(tcChannel* shadowClassification)
{
  Q_ASSERT(0 == m_shadowClassification);
  m_shadowClassification = shadowClassification;
}

/// Set shading channel.
void tcShadowClassifyingData::setShading(tcChannel* shading)
{
  Q_ASSERT(0 == m_shading);
  m_shading = shading;
}
