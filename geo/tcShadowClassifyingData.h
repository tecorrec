/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcShadowClassifyingData_h_
#define _tcShadowClassifyingData_h_

/**
 * @file tcShadowClassifyingData.h
 * @brief Image data set which provides a shadow classification channel.
 */

#include "tcGeoImageData.h"

class tcChannel;

/// Image data set which provides a shadow classification channel.
class tcShadowClassifyingData : public tcGeoImageData
{
  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcShadowClassifyingData();

    /// Destructor.
    virtual ~tcShadowClassifyingData();

    /*
     * Accessors
     */

    /// Get the shadow classification channel.
    tcChannel* shadowClassification();

    /// Get shading channel.
    tcChannel* shading();

  protected:

    /*
     * Derived class interface
     */

    /// Set shadow classification channel.
    void setShadowClassification(tcChannel* shadowClassification);

    /// Set shading channel.
    void setShading(tcChannel* shading);

  private:

    /*
     * Variables
     */

    /// Shadow classification channel.
    tcChannel* m_shadowClassification;

    /// Shading channel.
    tcChannel* m_shading;
};

#endif

