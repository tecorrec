/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcPixelOp_h_
#define _tcPixelOp_h_

/**
 * @file tcPixelOp.h
 * @brief Performs a pixel operation on a set of input channels.
 */

#include "tcChannel.h"

#include <QList>

/// Performs a pixel operation on a set of input channels.
class tcPixelOp : public tcChannel
{
  public:

    /*
     * Types
     */

    /// Operations
    enum Op
    {
      Add,
      Subtract,
      Diff,
      NormalizedDiff
    };

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcPixelOp(Op op, const QList<tcChannel*>& inputChannels, float scale = 1.0f);

    /// Primary constructor.
    tcPixelOp(Op op, const QList<tcChannel*>& inputChannels, bool radiance);

    /// Destructor.
    virtual ~tcPixelOp();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  private:

    /*
     * Variables
     */

    /// Input channels.
    QList<tcChannel*> m_inputChannels;

    /// Operation.
    Op m_op;

    /// Automatically scale to [,1].
    bool m_autoScale;

    /// Whether to use radiance.
    bool m_radiance;

    /// Overall scale.
    float m_scale;
};

#endif
