/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcChannelManager_h_
#define _tcChannelManager_h_

/**
 * @file tcChannelManager.h
 * @brief Manages a set of channels, any of which can be mapped to RGB.
 */

#include <QObject>
#include <QList>

class tcChannel;

/// Manages a set of channels, any of which can be mapped to RGB.
class tcChannelManager : public QObject
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcChannelManager();

    /// Destructor.
    virtual ~tcChannelManager();

    /*
     * Accessors
     */

    /// Get the number of channel.
    int numChannels() const;

    /// Get a specific channel.
    tcChannel* channel(int index) const;

    /*
     * Channel operations
     */

    /// Add and take ownership of a channel.
    int addChannel(tcChannel* channel);

    /// Add and take ownership of a set of channels.
    void addChannels(const QList<tcChannel*>& channels);

  signals:

    /*
     * Signals
     */

    /// Emitted to indicate progress of processing.
    void progressing(const QString& message);

  private:

    /*
     * Variables
     */

    /// List of channels.
    QList<tcChannel*> m_channels;
};

#endif
