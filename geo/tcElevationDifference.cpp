/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcElevationDifference.cpp
 * @brief Elevation map including void highlighting.
 */

#include "tcElevationDifference.h"
#include "tcSrtmModel.h"
#include "tcGeoImageData.h"
#include "tcChannelConfigWidget.h"

#include <QVBoxLayout>
#include <QLabel>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcElevationDifference::tcElevationDifference(const tcGeoImageData* refImagery, tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery)
: tcChannelDem(dem, imagery,
               tr("Elevation difference"),
               tr("Difference in elevation between primary and secondary DEM."))
, m_referenceChannel(reference)
, m_refTransform(imagery->texToGeo() * refImagery->geoToTex())
, m_configWidget(0)
, m_stdDeviation(-1.0f)
{
}

/// Destructor.
tcElevationDifference::~tcElevationDifference()
{
  delete m_configWidget;
}

/*
 * Main image interface
 */

tcChannelConfigWidget* tcElevationDifference::configWidget()
{
  if (0 == m_configWidget)
  {
    m_configWidget = new tcChannelConfigWidget();
    m_configWidget->setWindowTitle(tr("%1 Configuration").arg(name()));

    QVBoxLayout* layout = new QVBoxLayout(m_configWidget);

    QLabel* standardDeviation = new QLabel(tr("Standard deviation: %1 m").arg(m_stdDeviation), m_configWidget);
      layout->addWidget(standardDeviation);
      connect(this, SIGNAL(stdDeviationChanged(const QString&)), standardDeviation, SLOT(setText(const QString&)));
  }
  return m_configWidget;
}

/*
 * Interface for derived class to implement
 */

void tcElevationDifference::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
}

tcAbstractPixelData* tcElevationDifference::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  maths::Vector<2,double> xy1 = m_refTransform * maths::Vector<2,double>(x1,y1);
  maths::Vector<2,double> xy2 = m_refTransform * maths::Vector<2,double>(x2,y2);
  Reference<tcAbstractPixelData> channelData = m_referenceChannel->loadPortion(xy1[0], xy1[1], xy2[0], xy2[1], changed);
  int width = channelData->width();
  int height = channelData->height();

  // Create a new pixel buffer
  tcPixelData<float>* data = new tcPixelData<float>(width, height);
  startProcessing("Diffing elevation data");
  int index = 0;
  double sum = 0.0;
  for (int j = 0; j < height; ++j)
  {
    progress((float)j/(height-1));
    for (int i = 0; i < width; ++i)
    {
      // Transform coordinates
      maths::Vector<2,float> coord(x1 + (x2-x1)*i / (width  - 1),
                                   y1 + (y2-y1)*j / (height - 1));
      tcGeo geoCoord = geoAt(coord);

      // Get some elevation data
      bool accurate;
      float altitude1 = dem()[0].altitudeAt(geoCoord, true, &accurate);
      float altitude2 = dem()[1].altitudeAt(geoCoord, true, &accurate);
      float diff = altitude2 - altitude1;
      diff *= diff;
      sum += diff;
      data->buffer()[index] = diff/(500.0f*500.0f);
      ++index;
    }
  }
  m_stdDeviation = sqrt(sum/(width*height));
  emit stdDeviationChanged(tr("Standard deviation: %1 m").arg(m_stdDeviation));
  endProcessing();
  return data;
}
