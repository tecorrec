/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcAffineTransform_h_
#define _tcAffineTransform_h_

/**
 * @file tcAffineTransform.h
 * @brief Affine transformation between 2d linear coordinate spaces.
 */

#include "tcGeo.h"
#include <Vector.h>

#include <cmath>

/// Geographical coordinates.
template <typename T>
class tcAffineTransform2
{
  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcAffineTransform2()
    : m_transform()
    {
      m_transform[0][1] = m_transform[1][2] = 1.0f;
      m_transform[0][2] = m_transform[1][1]
        = m_transform[0][0] = m_transform[0][0] = 0.0f;
    }

    /// Primary constructor.
    tcAffineTransform2(const maths::Vector<2,T>& min, const maths::Vector<2,T>& diagonal)
    : m_transform()
    {
      for (int i = 0; i < 2; ++i)
      {
        m_transform[i][0] = min[i];
        m_transform[i][1+i] = diagonal[i];
        m_transform[i][2-i] = 0.0f;
      }
    }

    /// Primary constructor.
    tcAffineTransform2(const maths::Vector<2,T>& min, const maths::Vector<2,T>& x, const maths::Vector<2,T>& y)
    : m_transform()
    {
      for (int i = 0; i < 2; ++i)
      {
        m_transform[i][0] = min[i];
        m_transform[i][1] = x[i];
        m_transform[i][2] = y[i];
      }
    }

    /// Primary constructor.
    tcAffineTransform2(const T* m)
    : m_transform()
    {
      for (int i = 0; i < 2*3; ++i)
      {
        ((T*)m_transform)[i] = m[i];
      }
    }

    /// Primary constructor.
    tcAffineTransform2(T m11, T m12, T m13,  T m21, T m22, T m23)
    : m_transform()
    {
      m_transform[0][0] = m11;
      m_transform[0][1] = m12;
      m_transform[0][2] = m13;
      m_transform[1][0] = m21;
      m_transform[1][1] = m22;
      m_transform[1][2] = m23;
    }

    /*
     * Accessors + mutators
     */

    /// Origin of coordinate system.
    maths::Vector<2,T> origin() const
    {
      return maths::Vector<2,T>(m_transform[0][0], m_transform[1][0]);
    }
    maths::Vector<2,T> axis(int index) const
    {
      Q_ASSERT(index >= 0 && index < 2);
      return maths::Vector<2,T>(m_transform[0][1+index], m_transform[1][1+index]);
    }

    /// Find the inverse.
    tcAffineTransform2 inverse() const;

    /*
     * Operators
     */

    /// Transform a coordinate.
    maths::Vector<2,T> operator * (const maths::Vector<2,T>& rhs) const
    {
      return maths::Vector<2,T>(
        m_transform[0][0] + m_transform[0][1]*rhs[0] + m_transform[0][2]*rhs[1],
        m_transform[1][0] + m_transform[1][1]*rhs[0] + m_transform[1][2]*rhs[1]
      );
    }

    /// Transform a coordinate by inverse of this matrix.
    maths::Vector<2,T> operator / (const maths::Vector<2,T>& rhs) const
    {
      maths::Vector<2,T> rebased = rhs - origin();
      return maths::Vector<2,T>( rebased / axis(0), rebased / axis(1) );
    }

    /// Transform by another such matrix to get another which performs rhs then lhs on the result.
    tcAffineTransform2 operator * (const tcAffineTransform2& rhs) const
    {
      return tcAffineTransform2(*this * rhs.origin(), *this * rhs.axis(0), *this * rhs.axis(1));
    }

    /// Convert to a pointer to elements.
    operator T* ()
    {
      return &m_transform[0][0];
    }

    /// Convert to a pointer to elements.
    operator const T* () const
    {
      return &m_transform[0][0];
    }

  private:

    /*
     * Variables
     */

    /// Matrix, first column is offset, second is x vector, third is y vector.
    T m_transform[2][3];

};

#endif // _tcAffineTransform_h_

