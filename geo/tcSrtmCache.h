/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcSrtmCache_h_
#define _tcSrtmCache_h_

/**
 * @file tcSrtmCache.h
 * @brief Manages cache of SRTM data.
 */

#include <QObject>
#include <QString>
#include <QStringList>
#include <QDir>

class tcSrtmData;
class tcGeo;
class tcElevationData;

/** Manages cache of SRTM data.
 * Obtains SRTM elevation data blocks, downloading from nasa if necessary.
 *
 * FTP site: ftp://e0srp01u.ecs.nasa.gov/
 */
class tcSrtmCache : public QObject
{
    Q_OBJECT

  public:

    /*
     * Static functions
     */

    /// Get a list of datasets.
    static QStringList dataSets();

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcSrtmCache(const QString& dataSet);

    /// Destructor.
    virtual ~tcSrtmCache();

    /*
     * Main interface
     */

    /** Load raw SRTM height file.
     * Download the file if necessary.
     * @param lon Longitude in degrees.
     * @param lat Latitude in degrees.
     * @returns SRTM data for the chunk containing (@p lon, @p lat) or 0.
     */
    tcSrtmData* loadData(int lon, int lat);

    /** Load raw SRTM height file.
     * Download the file if necessary.
     * @param coord Geographical coordinate.
     * @returns SRTM data for the chunk containing (@p lon, @p lat) or 0.
     */
    tcSrtmData* loadData(const tcGeo& coord);

    /** Find some SRTM height data overlapping an area.
     * @param swCorner Geographical coordinate of south-west corner.
     * @param neCorner Geographical coordinate of north-east corner.
     * @returns An arbitrary SRTM data for a chunk in the lon lat range.
     */
    tcSrtmData* findData(const tcGeo& swCorner, const tcGeo& neCorner);

    /// Update any samples affected by the elevation field.
    void updateFromElevationData(const tcElevationData* elevation);

  signals:

    /*
     * Signals
     */

    /// Progress notification.
    void progressing(const QString& message);

  private:

    /*
     * Private functions
     */

    /// Convert a geographical coordinate into lon lat ints.
    void toIntLonLat(const tcGeo& coord, int* const outLon, int* const outLat);

    /// Cache index.
    tcSrtmData*& cache(int lon, int lat);

  private:

    /*
     * Static variables
     */

    /// Directory containing elevation data sets.
    static QDir s_dataDirectory;

    /*
     * Variables
     */

    /// Dataset name.
    QString m_dataSet;

    /// Cache of data at each grid point.
    tcSrtmData* m_cache[360][180];
};

#endif

