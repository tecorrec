/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcChannel.cpp
 * @brief A single abstract image channel.
 */

#include "tcChannel.h"

/*
 * Static variables
 */

/// Current processing step.
QString tcChannel::s_processingStep;

/// Last progress percent.
int tcChannel::s_processingLastPercent;

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcChannel::tcChannel(const QString& name, const QString& description)
: m_channelManager(0)
, m_name(name)
, m_description(description)
, m_radianceOffset(0.0f)
, m_radianceGain(1.0f)
, m_crossSectioned(false)
{
}

/// Destructor.
tcChannel::~tcChannel()
{
}

/*
 * Metadata
 */

/// Get the channel manager.
tcChannelManager* tcChannel::channelManager() const
{
  return m_channelManager;
}

/// Set the channel manager.
void tcChannel::setChannelManager(tcChannelManager* channelManager)
{
  m_channelManager = channelManager;
}

/// Get the channel name.
const QString& tcChannel::name() const
{
  return m_name;
}

/// Get the channel description;
const QString& tcChannel::description() const
{
  return m_description;
}

/// Set the channel name.
void tcChannel::setName(const QString& name)
{
  m_name = name;
}

/// Set the channel description.
void tcChannel::setDescription(const QString& description)
{
  m_description = description;
}

/// Set radiance offset and gain.
void tcChannel::setRadianceTransform(float offset, float gain)
{
  m_radianceOffset = offset;
  m_radianceGain = gain;
}

/// New cross section selected.
void tcChannel::newCrossSection(const tcGeo& p1, const tcGeo& p2)
{
  m_crossSectioned = true;
  m_crossSection[0] = p1;
  m_crossSection[1] = p2;
}

/*
 * Dependencies
 */

/// Add derivitive of this channel.
void tcChannel::addDerivitive(tcChannel* derivitive)
{
  m_derivitives += derivitive;
}

/// Remove a derivitive of this channel.
void tcChannel::removeDerivitive(tcChannel* derivitive)
{
  m_derivitives.removeOne(derivitive);
}

/*
 * Main image interface
 */

/// Get configuration widget.
tcChannelConfigWidget* tcChannel::configWidget()
{
  return 0;
}

/// Get GL texture ID of thumbnail of the channel.
GLuint tcChannel::thumbnailTexture()
{
  return 0;
}

/// Get a reference to the pixel data of the current portion of the image.
Reference<tcAbstractPixelData> tcChannel::portion() const
{
  return m_portion;
}

/// Get a reference to the pixel data of a portion of the image.
Reference<tcAbstractPixelData> tcChannel::portion(double* x1, double* y1, double* x2, double* y2)
{
  // If the portion is out of date, remove it
  roundPortion(x1,y1,x2,y2);
  bool changed = false;
  if (0 != m_portion)
  {
    if (m_portionPosition[0][0] != *x1 ||
        m_portionPosition[0][1] != *y1 ||
        m_portionPosition[1][0] != *x2 ||
        m_portionPosition[1][1] != *y2)
    {
      m_portion = 0;
      changed = true;
    }
  }
  m_portionPosition[0][0] = *x1;
  m_portionPosition[0][1] = *y1;
  m_portionPosition[1][0] = *x2;
  m_portionPosition[1][1] = *y2;
  // Create the new portion
  if (0 == m_portion)
  {
    m_portion = loadPortion(*x1,*y1,*x2,*y2, changed);
  }
  return m_portion;
}

/// Get a reference to the pixel data of a portion of the image.
Reference<tcAbstractPixelData> tcChannel::portion(double x1, double y1, double x2, double y2)
{
  return portion(&x1, &y1, &x2, &y2);
}

/// Get GL texture ID of portion of the channel.
GLuint tcChannel::portionTexture(double* x1, double* y1, double* x2, double* y2)
{
  Reference<tcAbstractPixelData> data = portion(x1,y1,x2,y2);
  if (0 != data)
  {
    return data->texture();
  }
  else
  {
    return 0;
  }
}

/// Get the last portion position.
const double* tcChannel::portionPosition() const
{
  return &m_portionPosition[0][0];
}

/*
 * Interface for derived classes
 */

/// Invalidate this channel.
void tcChannel::invalidate()
{
  m_portion = 0;
  foreach (tcChannel* derivitive, m_derivitives)
  {
    derivitive->invalidate();
  }
}

/*
 * Progress functions
 */

/// Start a processing step.
void tcChannel::startProcessing(const QString& step)
{
  if (step.isEmpty())
  {
    s_processingStep = step;
  }
  else
  {
    s_processingStep = step + ": ";
  }
  s_processingLastPercent = -1;
  emit progressing(tr("%1: %2").arg(name()).arg(s_processingStep));
}

/// Update progress.
void tcChannel::progress(float progress)
{
  int percent = progress*100.0f;
  if (percent != s_processingLastPercent)
  {
    s_processingLastPercent = percent;
    emit progressing(tr("%1: %2%3%").arg(name()).arg(s_processingStep).arg(percent));
  }
}

/// Update progress.
void tcChannel::progress(const QString& info)
{
  emit progressing(tr("%1: %2%3").arg(name()).arg(s_processingStep).arg(info));
}

/// End processing step.
void tcChannel::endProcessing()
{
  emit progressing(tr("%1: %2Complete").arg(name()).arg(s_processingStep));
}
