find_package(Qt4 REQUIRED)
find_package(GDAL REQUIRED)

set(QT_USE_QTOPENGL true)
include(${QT_USE_FILE})
include_directories(${QT_INCLUDES}
                    ${GDAL_INCLUDE_DIR}
)

set(tecorrec_geo_SRCS
    tcAffineTransform.cpp
    tcObserver.cpp
    tcGlobe.cpp
    tcGeoData.cpp
    tcSrtmModel.cpp
    tcSrtmCache.cpp
    tcSrtmData.cpp
    tcPixelData.cpp
    tcChannel.cpp
    tcChannelDem.cpp
    tcChannelGroup.cpp
    tcChannelManager.cpp
    tcChannelConfigWidget.cpp
    tcChannelFile.cpp
    tcChannelChromaticity.cpp
    tcIlluminantDirection.cpp
    tcShadowFreeChromaticities.cpp
    tcIlluminantDiscontinuity.cpp
    tcShadowClassification.cpp
    tcNegativeProduct.cpp
    tcPixelOp.cpp
    tcShadowDepth.cpp
    tcLambertianShading.cpp
    tcRaytracedShadowMap.cpp
    tcNormals.cpp
    tcElevation.cpp
    tcElevationDifference.cpp
    tcElevationOptimization.cpp
    tcElevationData.cpp
    tcGeoImageData.cpp
    tcSensor.cpp
    tcSpectrum.cpp
    tcRangeSpectrum.cpp
    tcShadowClassifyingData.cpp
    tcLandsatMetaData.cpp
    tcLandsatData.cpp
    tcCustomSunDirection.cpp
    tcProcessingData.cpp
    tcExportText.cpp
)

set(tecorrec_geo_HEADERS
    tcChannel.h
    tcChannelGroup.h
    tcChannelManager.h
    tcChannelConfigWidget.h
    tcIlluminantDirection.h
    tcNegativeProduct.h
    tcElevationDifference.h
    tcElevationOptimization.h
    tcSrtmModel.h
    tcSrtmCache.h
    tcSrtmData.h
    tcExportText.h
)

qt4_wrap_cpp(tecorrec_geo_MOCS ${tecorrec_geo_HEADERS})

add_library(tecorrec_geo ${tecorrec_geo_SRCS} ${tecorrec_geo_MOCS})

target_link_libraries(tecorrec_geo
                      ${QT_LIBRARIES}
                      ${GDAL_LIBRARY}
                      tecorrec_maths
)
