/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcPixelData.cpp
 * @brief A block of pixel data.
 */

#include "tcPixelData.h"

#include <GL/glu.h>

#include <inttypes.h>

/*
 * Abstract Pixel Data Constructors + destructor
 */

/// Primary constructor.
tcAbstractPixelData::tcAbstractPixelData(int width, int height)
: m_textureId(0)
{
  Q_ASSERT(width > 0);
  Q_ASSERT(height > 0);
  m_size[0] = width;
  m_size[1] = height;
}

/// Destructor.
tcAbstractPixelData::~tcAbstractPixelData()
{
  discardTexture();
}

/*
 * Accessors
 */

/// Get the width of the image.
int tcAbstractPixelData::width() const
{
  return m_size[0];
}

/// Get the height of the image.
int tcAbstractPixelData::height() const
{
  return m_size[1];
}

/// Get (creating if necessary) a GL texture for this image.
GLuint tcAbstractPixelData::texture()
{
  if (0 == m_textureId)
  {
    m_textureId = loadTexture();
  }
  return m_textureId;
}

/// Discard of any cached texture id for this image.
void tcAbstractPixelData::discardTexture()
{
  glDeleteTextures(1, &m_textureId);
}

/*
 * Virtual interface for derived classes to implement.
 */

/// Load the pixel data into a GL texture.
GLuint tcAbstractPixelData::loadTexture()
{
  return 0;
}

/*
 * Pixel data texture creation
 */

template <>
GLuint tcPixelData<GLubyte>::loadTexture()
{
  GLuint textureId = 0;
  if (0 != buffer())
  {
    glGenTextures(1, &textureId);
    GLint binding = 0;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &binding);
    glBindTexture(GL_TEXTURE_2D, textureId);
    if (0 == gluBuild2DMipmaps(GL_TEXTURE_2D, GL_LUMINANCE, m_size[0], m_size[1],
                               GL_LUMINANCE, GL_UNSIGNED_BYTE, buffer()))
    {
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    }
    else
    {
      glDeleteTextures(1, &textureId);
    }
    glBindTexture(GL_TEXTURE_2D, binding);
  }
  return textureId;
}

template <>
GLuint tcPixelData<GLfloat>::loadTexture()
{
  GLuint textureId = 0;
  if (0 != buffer())
  {
    glGenTextures(1, &textureId);
    GLint binding = 0;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &binding);
    glBindTexture(GL_TEXTURE_2D, textureId);
    if (0 == gluBuild2DMipmaps(GL_TEXTURE_2D, GL_LUMINANCE, m_size[0], m_size[1],
                               GL_LUMINANCE, GL_FLOAT, buffer()))
    {
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    }
    else
    {
      glDeleteTextures(1, &textureId);
    }
    glBindTexture(GL_TEXTURE_2D, binding);
  }
  return textureId;
}
