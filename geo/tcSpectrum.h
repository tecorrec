/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcSpectrum_h_
#define _tcSpectrum_h_

/**
 * @file tcSpectrum.h
 * @brief Frequency spectrum.
 */

/// Frequency spectrum.
class tcSpectrum
{
  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcSpectrum();

    /// Destructor.
    virtual ~tcSpectrum();

    /*
     * Virtual interface
     */

    /// Get the mean input value.
    virtual float mean() const = 0;

    /// Full integral (-inf, inf).
    virtual float integrate() const = 0;

    /// Partial integral (-inf, @p x).
    virtual float integrate(float x) const = 0;

    /// Range integral (@p x, @p y).
    virtual float integrate(float x, float y) const;
};

#endif

