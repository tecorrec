/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcGeo_h_
#define _tcGeo_h_

/**
 * @file tcGeo.h
 * @brief Geographical coordinates.
 */

#include <Vector.h>
#include <Matrix.h>

#include <QString>
#include <QObject>

#include <cmath>

/// Use doubles for angles.
typedef double tcGeoAngle;

class tcGeoAngleDMS
{
  public:
    tcGeoAngleDMS(tcGeoAngle angle, bool latitude)
    : latitude(latitude)
    {
      positive = (angle >= 0.0);
      if (!positive)
      {
        angle = -angle;
      }
      angle *= 180.0 / M_PI;
      degrees = (int)floor(angle);
      angle -= degrees;
      angle *= 60.0;
      arcmins = (int)floor(angle);
      angle -= arcmins;
      arcsecs = angle * 60.0;
    }

    operator QString () const
    {
      return QObject::tr("%1\xB0%2'%3\"%4", "degrees, arcminutes, arcseconds, N/S/E/W")
                .arg(degrees)
                .arg(arcmins, 2, 10, QLatin1Char('0'))
                .arg(arcsecs, 5, 'f', 2, QLatin1Char('0'))
                .arg(latitude
                     ? (positive ? QObject::tr("N", "north") : QObject::tr("S", "south"))
                     : (positive ? QObject::tr("E", "east")  : QObject::tr("W", "west"))
                     );
    }

    bool latitude;
    bool positive;
    int degrees;
    int arcmins;
    double arcsecs;
};

/// Geographical coordinates.
class tcGeo
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcGeo()
    : m_longitude(0.0)
    , m_latitude(0.0)
    {
    }

    /// Primary constructor.
    tcGeo(const tcGeoAngle lon, const tcGeoAngle lat)
    : m_longitude(lon)
    , m_latitude(lat)
    {
    }

    /// Primary constructor.
    explicit tcGeo(const maths::Vector<2,double> lonlat)
    : m_longitude(lonlat[0])
    , m_latitude(lonlat[1])
    {
    }

    /// Construct from a vector.
    template <typename T>
    tcGeo(maths::Vector<3,T> vec, bool normalized)
    {
      if (!normalized)
      {
        vec.normalize();
      }
      m_longitude = atan2(vec[0], -vec[1]);
      m_latitude = asin(vec[2]);
    }

    /*
     * Conversions
     */

    /// Convert to a 2d position vector with same values.
    operator maths::Vector<2,double> () const
    {
      return maths::Vector<2,double>(m_longitude, m_latitude);
    }

    /// Convert to a 3d direction vector.
    operator maths::Vector<3,float> () const
    {
      float z = sinf(m_latitude);
      float xy = cosf(m_latitude);
      return maths::Vector<3,float>(xy*sinf(m_longitude), -xy*cosf(m_longitude), z);
    }

    /// Convert to a 3d direction vector.
    operator maths::Vector<3,double> () const
    {
      //return this->operator maths::Matrix<3,double>() * maths::Vector<3,double>(0.0, 0.0, 1.0);
      double z = sin(m_latitude);
      double xy = cos(m_latitude);
      return maths::Vector<3,double>(xy*sin(m_longitude), -xy*cos(m_longitude), z);
    }

    /// Convert to a rotation matrix.
    operator maths::Matrix<3,double> () const
    {
      return maths::Matrix<3,double>(
        maths::RotateRadMatrix44('x', m_latitude-M_PI/2)
        *
        maths::RotateRadMatrix44('z', -m_longitude)
        );
      double sina = sin(m_longitude);
      double cosa = cos(m_longitude);
      double sine = sin(m_latitude-M_PI/2);
      double cose = cos(m_latitude-M_PI/2);
      /*
      [[cos(a),                sin(a),                0],
       [-sin(a) cos(-1/2 PI+e),cos(a) cos(-1/2 PI+e), sin(-1/2 PI+e)],
       [sin(a) sin(-1/2 PI+e), -cos(a) sin(-1/2 PI+e),cos(-1/2 PI+e)]]
      */
      return maths::Matrix<3,double>(
        // First column
        maths::Vector<3,double>(cosa, -sina*cose, sina*sine),
        // Second column
        maths::Vector<3,double>(sina, cosa*cose, -cosa*sine),
        // Third column
        maths::Vector<3,double>(0.0, sine, cose)
      );
    }

    /*
     * Accessors
     */

    /// Get the longitude.
    tcGeoAngle lon() const
    {
      return m_longitude;
    }
    /// Get the azimuth (longitude).
    tcGeoAngle azim() const
    {
      return m_longitude;
    }

    /// Get the latitude.
    tcGeoAngle lat() const
    {
      return m_latitude;
    }
    /// Get the elevation (latitude).
    tcGeoAngle elev() const
    {
      return m_latitude;
    }

    /// Get a textual description.
    QString describe() const
    {
      tcGeoAngleDMS dmsLon(m_longitude, false);
      tcGeoAngleDMS dmsLat(m_latitude,  true);
      return QString("%1 %2").arg(dmsLat).arg(dmsLon);
    }

    /*
     * Mutators
     */

    /// Set the longitude.
    void setLon(const tcGeoAngle lon)
    {
      m_longitude = lon;
    }
    /// Set the azimuth (longitude).
    void setAzim(const tcGeoAngle lon)
    {
      m_longitude = lon;
    }

    /// Set the latitude.
    void setLat(const tcGeoAngle lat)
    {
      m_latitude = lat;
    }
    /// Set the elevation (latitude).
    void setElev(const tcGeoAngle lat)
    {
      m_latitude = lat;
    }

    /// Set the longitude and latitude.
    void setLonLat(const tcGeoAngle lon, const tcGeoAngle lat)
    {
      m_longitude = lon;
      m_latitude = lat;
    }
    /// Set the azimuth (longitude) and elevation (latitude).
    void setAzimElev(const tcGeoAngle lon, const tcGeoAngle lat)
    {
      m_longitude = lon;
      m_latitude = lat;
    }

    /*
     * Operators
     */

    tcGeo operator + (const tcGeo& other) const
    {
      return tcGeo(m_longitude + other.m_longitude,
                   m_latitude  + other.m_latitude);
    }
    tcGeo operator - (const tcGeo& other) const
    {
      return tcGeo(m_longitude - other.m_longitude,
                   m_latitude  - other.m_latitude);
    }
    tcGeo operator * (const tcGeoAngle factor) const
    {
      return tcGeo(m_longitude * factor,
                   m_latitude  * factor);
    }
    tcGeo operator / (const tcGeoAngle factor) const
    {
      return tcGeo(m_longitude / factor,
                   m_latitude  / factor);
    }
    maths::Vector<2,double> operator / (const tcGeo& other) const
    {
      return maths::Vector<2,double>(m_longitude / other.m_longitude,
                                     m_latitude  / other.m_latitude);
    }
    tcGeo operator * (const maths::Vector<2,double>& other) const
    {
      return tcGeo(m_longitude * other[0],
                   m_latitude  * other[1]);
    }
    template <typename T>
    maths::Vector<3,T> operator * (const maths::Vector<3,T>& other) const
    {
      return (maths::Vector<3,T>)((maths::Vector<3,double>)other * operator maths::Matrix<3,double>());
      float sinLon = sinf(m_longitude);
      float cosLon = cosf(m_longitude);
      float sinLat = sinf(m_latitude);
      float cosLat = cosf(m_latitude);
      return maths::Vector<3,T>(other[0]*cosLon + other[2]*sinLon*cosLat,
                                other[0]*sinLon - other[2]*cosLon*cosLat,
                                other[1]*cosLat + other[2]*sinLat);
    }

    float angle() const
    {
      return sqrt(m_longitude*m_longitude + m_latitude*m_latitude);
    }

    static double angleBetween(const tcGeo& lhs, const tcGeo& rhs)
    {
      //return acos((maths::Vector<3,double>)lhs* (maths::Vector<3,double>)rhs);
      maths::Vector<3,double> x = cross((maths::Vector<3,double>)lhs, (maths::Vector<3,double>)rhs);
      return asin(x.mag());
    }

  private:

    /*
     * Variables
     */

    /** East-West geographical coordinate.
     * Units are radians.
     */
    tcGeoAngle m_longitude;

    /** North-South geographical coordinate.
     * Units are radians.
     */
    tcGeoAngle m_latitude;

};

#endif // _tcGeo_h_

