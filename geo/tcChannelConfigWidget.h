/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcChannelConfigWidget_h_
#define _tcChannelConfigWidget_h_

/**
 * @file tcChannelConfigWidget.h
 * @brief A channel configuration widget.
 */

#include <QWidget>

class tcGeo;

/// A channel configuration widget.
class tcChannelConfigWidget : public QWidget
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcChannelConfigWidget();

    /// Destructor.
    virtual ~tcChannelConfigWidget();

    /*
     * Main interface
     */

    /// Request a texture point.
    void requestTexturePoint(QObject* receiver, const char* member);

    /// Request redraw.
    void requestRedraw();

    /// Select a new slice.
    void requestSlice(const tcGeo& sw, const tcGeo& ne);

  signals:

    /*
     * Signals
     */

    /// Emitted when settings have been changed and the viewport may need redrawing.
    void updated();

    /// Select a new slice.
    void newSlice(const tcGeo& sw, const tcGeo& ne);

    /// Emitted to select a new texture point.
    void texturePointRequested(QObject* receiver, const char* member);
};

#endif
