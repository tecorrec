/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcGeoData.cpp
 * @brief Data for a section of the globe.
 */

#include "tcGeoData.h"
#include <glVector.h>

#include <GL/gl.h>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcGeoData::tcGeoData(DataType type)
: m_type(type)
{
}

/// Destructor.
tcGeoData::~tcGeoData()
{
}

/*
 * Rendering
 */

/// Render schematic drawing.
void tcGeoData::renderSchematic(double meanRadius, tcObserver* const observer)
{
  double boxHeight[2] = {1.0, 1.005};
  glColor3f(1.0f, 0.0f, 1.0f);
  for (int i = 0; i < 2; ++i)
  {
    glBegin(GL_LINE_LOOP);
    {
      glVertex3((meanRadius*boxHeight[i]) * (GLvec3d)m_coordinates[0][0]);
      glVertex3((meanRadius*boxHeight[i]) * (GLvec3d)m_coordinates[0][1]);
      glVertex3((meanRadius*boxHeight[i]) * (GLvec3d)m_coordinates[1][1]);
      glVertex3((meanRadius*boxHeight[i]) * (GLvec3d)m_coordinates[1][0]);
    }
    glEnd();
  }
  glBegin(GL_LINES);
  {
    for (int i = 0; i < 2; ++i)
    {
      for (int j = 0; j < 2; ++j)
      {
        glVertex3((meanRadius*boxHeight[0]) * (GLvec3d)m_coordinates[i][j]);
        glVertex3((meanRadius*boxHeight[1]) * (GLvec3d)m_coordinates[i][j]);
      }
    }
  }
  glEnd();
}
