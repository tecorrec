/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcChannelChromaticity.cpp
 * @brief An image processing channel of the chromaticity between two channels.
 */

#include "tcChannelChromaticity.h"

#include <QObject>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcChannelChromaticity::tcChannelChromaticity(tcChannel* numerator, tcChannel* denominator)
: tcChannel(tr("Chromaticity(%1,%2)").arg(numerator->name()).arg(denominator->name()),
            tr("Chromaticity (division) of the %1 and %2 channels").arg(numerator->name()).arg(denominator->name()))
, m_numerator(numerator)
, m_denominator(denominator)
{
  m_numerator->addDerivitive(this);
  m_denominator->addDerivitive(this);
}

/// Destructor.
tcChannelChromaticity::~tcChannelChromaticity()
{
  m_numerator->removeDerivitive(this);
  m_denominator->removeDerivitive(this);
}

/*
 * Interface for derived class to implement
 */

void tcChannelChromaticity::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  m_numerator->roundPortion(x1, y1, x2, y2);
}

tcAbstractPixelData* tcChannelChromaticity::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  Reference<tcPixelData<GLubyte> > numerator   = dynamic_cast<tcPixelData<GLubyte>*>(m_numerator->loadPortion(x1, y1, x2, y2, changed));
  Reference<tcPixelData<GLubyte> > denominator = dynamic_cast<tcPixelData<GLubyte>*>(m_denominator->loadPortion(x1, y1, x2, y2, changed));

  tcPixelData<float>* data = 0;
  if (0 != numerator && 0 != denominator)
  {
    int width = numerator->width();
    int height = numerator->height();
    data = new tcPixelData<float>(width, height);
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int index = j*width + i;
        GLubyte n = numerator->buffer()[index];
        GLubyte d = denominator->sampleUByte((float)i/(width-1), (float)j/(height-1));
        if (n != 0xff && d != 0xff && d != 0x00)
        {
          data->buffer()[index] = (float)n / d / 4;
        }
        else
        {
          data->buffer()[index] = 0.0f;
        }
      }
    }
  }
  return data;
}
