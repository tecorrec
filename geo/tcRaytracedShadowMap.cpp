/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcRaytracedShadowMap.cpp
 * @brief Shadow map using ray tracing of DEM.
 */

#include "tcRaytracedShadowMap.h"
#include "tcGeoImageData.h"

#include <QObject>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcRaytracedShadowMap::tcRaytracedShadowMap(tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery)
: tcChannelDem(dem, imagery,
               tr("Raytraced Shadow Map"),
               tr("Uses ray tracing and elevation data to highlight shadows."))
, m_referenceChannel(reference)
, m_resolution()
, m_texToGeo(imagery->texToGeo())
{
}

/// Primary constructor.
tcRaytracedShadowMap::tcRaytracedShadowMap(const tcGeo& resolution, tcSrtmModel* dem, tcGeoImageData* imagery)
: tcChannelDem(dem, imagery,
               tr("Raytraced Shadow Map"),
               tr("Uses ray tracing and elevation data to highlight shadows."))
, m_referenceChannel(0)
, m_resolution(resolution)
, m_texToGeo(imagery->texToGeo())
{
}

/// Destructor.
tcRaytracedShadowMap::~tcRaytracedShadowMap()
{
}

/*
 * Interface for derived class to implement
 */

void tcRaytracedShadowMap::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  //m_referenceChannel->roundPortion(x1,y1,x2,y2);
}

tcAbstractPixelData* tcRaytracedShadowMap::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  int width;
  int height;
  if (0 != m_referenceChannel)
  {
    Reference<tcAbstractPixelData> channelData = m_referenceChannel->loadPortion(x1, y1, x2, y2, changed);
    width = channelData->width();
    height = channelData->height();
  }
  else
  {
    tcGeo diag = (tcGeo)(m_texToGeo * maths::Vector<2,double>(x1, y1) - m_texToGeo * maths::Vector<2,double>(x2,y2));
    maths::Vector<2,double> res = diag / m_resolution;
    width = abs((int)res[0]);
    height = abs((int)res[1]);
    // ensure resw and resh are powers of 2
    for (int i = 1; i < 20; ++i)
    {
      if (0 == (width & ~((1<<i)-1)))
      {
        width = 1<<i;
        break;
      }
    }
    for (int i = 1; i < 20; ++i)
    {
      if (0 == (height & ~((1<<i)-1)))
      {
        height = 1<<i;
        break;
      }
    }
  }

  // Create a new pixel buffer
  tcPixelData<float>* data = new tcPixelData<float>(width, height);
  startProcessing(tr("Raytracing"));
  for (int j = 0; j < height; ++j)
  {
    progress((float)j/(height-1));
    for (int i = 0; i < width; ++i)
    {
      int index = j*width + i;
      // Transform coordinates
      maths::Vector<2,float> coord      (x1 + (x2-x1)*i / (width  - 1),
                                         y1 + (y2-y1)*j / (height - 1));
      tcGeo geoCoord = geoAt(coord);
      float lit = litAt(geoCoord, true);

      data->buffer()[index] = lit;
    }
  }
  endProcessing();
  return data;
}
