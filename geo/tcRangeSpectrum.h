/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcRangeSpectrum_h_
#define _tcRangeSpectrum_h_

/**
 * @file tcRangeSpectrum.h
 * @brief Frequency spectrum of a uniform value in a range.
 */

#include "tcSpectrum.h"

/// Frequency spectrum.
class tcRangeSpectrum : public tcSpectrum
{
  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcRangeSpectrum(float max, float x, float y);

    /// Destructor.
    virtual ~tcRangeSpectrum();

    /*
     * Virtual interface
     */

    // Reimplemented
    virtual float mean() const;

    // Reimplemented
    virtual float integrate() const;

    // Reimplemented
    virtual float integrate(float x) const;

  private:

    /*
     * Variables
     */

    /// Value for the range.
    float m_max;

    /// Start and end of the value.
    float m_x, m_y;
};

#endif

