/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcLambertianShading.cpp
 * @brief Shading channel using lambertian reflectance.
 */

#include "tcLambertianShading.h"

#include <QObject>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcLambertianShading::tcLambertianShading(tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery)
: tcChannelDem(dem, imagery,
               tr("Lambertian Shading"),
               tr("Uses lambertian shading and elevation data to shade pixels. "
                           "Cast shadows are not taken into account. "
                           "Self shadows are set to zero."))
, m_referenceChannel(reference)
, m_customDirectionEnabled(false)
{
}

/// Primary constructor.
tcLambertianShading::tcLambertianShading(tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery, const maths::Vector<3,float>& direction)
: tcChannelDem(dem, imagery,
               tr("Custom Shading"),
               tr("Uses lambertian shading and elevation data to shade pixels. "
                           "Cast shadows are not taken into account. "
                           "Self shadows are set to zero."))
, m_referenceChannel(reference)
, m_customDirectionEnabled(true)
, m_customDirection(direction)
{
}

/// Destructor.
tcLambertianShading::~tcLambertianShading()
{
}

/*
 * Interface for derived class to implement
 */

void tcLambertianShading::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  m_referenceChannel->roundPortion(x1,y1,x2,y2);
}

tcAbstractPixelData* tcLambertianShading::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  Reference<tcAbstractPixelData> channelData = m_referenceChannel->loadPortion(x1, y1, x2, y2, changed);
  int width = channelData->width();
  int height = channelData->height();

  // Create a new pixel buffer
  tcPixelData<float>* data = new tcPixelData<float>(width, height);
  startProcessing("Calculating lighting");
  for (int j = 0; j < height; ++j)
  {
    progress((float)j/(height-1));
    for (int i = 0; i < width; ++i)
    {
      int index = j*width + i;
      // Transform coordinates
      maths::Vector<2,float> coord      (x1 + (x2-x1)*i / (width  - 1),
                                         y1 + (y2-y1)*j / (height - 1));
      tcGeo                  geoCoord = geoAt(coord);

      // Get some elevation data
      maths::Vector<3,float> normal   = normalAt(geoCoord, true);
      maths::Vector<3,float> light;
      if (m_customDirectionEnabled)
      {
        light = m_customDirection;
      }
      else
      {
        light = lightDirectionAt(geoCoord);
      }
      // Find dot product between normal and light vectors and limit it to 0.0
      float                  lambert  = normal * light;
      data->buffer()[index]           = (lambert >= 0.0f ? lambert : 0.0f);
    }
  }
  endProcessing();
  return data;
}
