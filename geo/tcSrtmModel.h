/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcSrtmModel_h_
#define _tcSrtmModel_h_

/**
 * @file tcSrtmModel.h
 * @brief SRTM elevation model.
 */

#include "tcGeo.h"
#include <Vector.h>

#include <QObject>

class tcSrtmCache;
class tcElevationData;

class QString;

/// SRTM elevation model.
class tcSrtmModel : public QObject
{
    Q_OBJECT

  public:

    /*
     * Types
     */

    /// Render state.
    class RenderState
    {
      public:

        /// Number of longitudenal samples in the tile.
        int samplesLon;

        /// Number of latitudenal samples in the tile.
        int samplesLat;

        /// Is there more detail available longitudely.
        bool moreAvailableLon;

        /// Is there more detail available latitudely
        bool moreAvailableLat;

      private:
        friend class tcSrtmModel;

        /// South west corner of tile.
        tcGeo swCorner;

        /// North east corner of tile.
        tcGeo neCorner;

        /// Coordinate of most south westly sample inside tile.
        tcGeo swSample;

        /// Diagonal angles between samples.
        tcGeo sampleDelta;
    };

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcSrtmModel();

    /// Destructor.
    virtual ~tcSrtmModel();

    /*
     * Main interface
     */

    /** Get information to allow alignment with actual samples.
     * @param swCorner South West corner of tile.
     * @param neCorner North East corner of tile.
     * @param state[out] Gets various information put into it to allow optimized rendering.
     * @param maxSamples Maximum number of inner samples.
     */
    void sampleAlign(const tcGeo& swCorner, const tcGeo& neCorner, RenderState* state, int maxSamples = 0);

    /// Get the altitude at a sample in a render state.
    double altitudeAt(const RenderState& state, int x, int y, tcGeo* outCoord, bool corrected = false, bool* accurate = 0);

    /// Get the altitude at a coordinate.
    double altitudeAt(const tcGeo& coord, bool corrected = false, bool* accurate = 0);

    /// Get the normal at a coordinate.
    maths::Vector<3,float> normalAt(const tcGeo& coord, bool corrected = false, bool* accurate = 0);

    /// Use raytracing to find whether a point is lit.
    bool isLitAt(const tcGeo& coord, const maths::Vector<3,float>& light, bool corrected = false, bool* accurate = 0);

    /// Use raytracing to find how lit a point is.
    float litAt(const tcGeo& coord, const maths::Vector<3,float>& light, float angularRadius = 0.0f, bool corrected = false, bool* accurate = 0);

    /// Set the data set to use.
    void setDataSet(const QString& name);

    /// Update any samples affected by the elevation field.
    void updateFromElevationData(const tcElevationData* elevation);

  signals:

    /*
     * Signals
     */

    /// Emitted when the dataset is changed.
    void dataSetChanged();

    /// Progress notifier.
    void progressing(const QString& progress);

  private:

    /*
     * Variables
     */

    /// Elevation dataset.
    tcSrtmCache* m_cache;
};

#endif

