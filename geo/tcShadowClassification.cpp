/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcShadowClassification.cpp
 * @brief Shadow classification channel.
 */

#include "tcShadowClassification.h"
#include "tcIlluminantDiscontinuity.h"

#include <QObject>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcShadowClassification::tcShadowClassification(tcIlluminantDiscontinuity* discontinuity, tcChannel* midIr)
: tcChannel(tr("Shadow classification"),
            tr("Tries to classify shadows using illuminant discontinuities and a mid IR channel"))
, m_discontinuity()
, m_midIr(midIr)
{
  for (int i = 0; i < 2; ++i)
  {
    m_discontinuity[i] = discontinuity->channels()[i];
    m_discontinuity[i]->addDerivitive(this);
  }
  m_midIr->addDerivitive(this);
}

/// Destructor.
tcShadowClassification::~tcShadowClassification()
{
  for (int i = 0; i < 2; ++i)
  {
    m_discontinuity[i]->removeDerivitive(this);
  }
  m_midIr->removeDerivitive(this);
}

/*
 * Interface for derived class to implement
 */

void tcShadowClassification::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  m_midIr->roundPortion(x1, y1, x2, y2);
}

tcAbstractPixelData* tcShadowClassification::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  // Get portions from both the discontinuity images and the mid ir
  // multiply discontinuity by (1-midIr)

  Reference<tcPixelData<float> > disc[2] = {
    dynamic_cast<tcPixelData<float>*>(m_discontinuity[0]->loadPortion(x1, y1, x2, y2, changed)),
    dynamic_cast<tcPixelData<float>*>(m_discontinuity[1]->loadPortion(x1, y1, x2, y2, changed))
  };
  Reference<tcPixelData<GLubyte> > midIr = dynamic_cast<tcPixelData<GLubyte>*>(m_midIr->loadPortion(x1, y1, x2, y2, changed));

  tcPixelData<GLubyte>* data = 0;
  if (0 != disc[0] && 0 != disc[1] && 0 != midIr)
  {
    int width = midIr->width();
    int height = midIr->height();
    Q_ASSERT(width == disc[0]->width());
    Q_ASSERT(height == disc[0]->height());
    Q_ASSERT(width == disc[1]->width());
    Q_ASSERT(height == disc[1]->height());
    data = new tcPixelData<GLubyte>(width, height);
    for (int i = 0; i < width*height; ++i)
    {
      float disc1Val = disc[0]->buffer()[i];
      float disc2Val = disc[1]->buffer()[i];
      float midIrVal = (float)midIr->buffer()[i]/255.0f;
      float shadowiness = fabs(disc1Val + disc1Val) * 0.5f
                            * (1.0f - midIrVal)
                            * (1.0f - midIrVal);
      //shadowiness = (shadowiness > 0.25f ? shadowiness : 0.0f);
      data->buffer()[i] = shadowiness;
      /// @todo Have a parameter threshold
      //data->buffer()[i] = (shadowiness > 0.25f ? 255 : 0);
    }

    // Linearly fill in the gaps
    /// @todo fillSize should be a parameter
    const int fillSize = 2;
    for (int d = 0; d < 0; ++d)
    {
      int dir = d % 2;
      int jMax = (dir ? width : height);
      int iMax = (dir ? height : width);
      for (int j = 0; j < jMax; ++j)
      {
        int blankCount = 0;
        for (int i = 0; i < iMax; ++i)
        {
          int row = (dir ? i : j);
          int col = (dir ? j : i);
          int index = row*width + col;
          if (0 != data->buffer()[index])
          {
            if (blankCount > 0 && blankCount <= fillSize)
            {
              int min = i-blankCount;
              if (min > 0)
              {
                for (int k = min; k < i; ++k)
                {
                  int kIndex = (dir?k:row)*width + (dir?col:k);
                  data->buffer()[kIndex] = 255;
                }
              }
            }
            blankCount = 0;
          }
          else
          {
            ++blankCount;
          }
        }
      }
    }
  }
  return data;
}
