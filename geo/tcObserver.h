/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcObserver_h_
#define _tcObserver_h_

/**
 * @file tcObserver.h
 * @brief Viewing information for an observer.
 */

#include "tcGeo.h"

/// Viewing information for an observer.
class tcObserver
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcObserver();

    /// Destructor.
    virtual ~tcObserver();

    /*
     * Main interface
     */

    /// Set up the OpenGL projection matrix for this observer.
    void setupProjection(double aspect) const;

    /// Set up the OpenGL modelview matrix for this observer.
    void setupModelView() const;

    /*
     * Mutators
     */

    /// Adjust the focus directly.
    void setFocus(const tcGeo& focus, double altitude);

    /// Adjust the focus altitude.
    void setFocusAltitude(double altitude);

    /// Make a local transformation of the focus.
    void moveFocusRelative(double dx, double dy);

    /// Adjust the range exponentially.
    void adjustRange(double x);

    /// Adjust the azimuth in radians.
    void adjustAzimuth(double daz);

    /// Adjust the elevation in radians.
    void adjustElevation(double del);

    /// Change the viewing angle.
    void setView(const tcGeo& view);

    /*
     * Accessors
     */

    /// Get the current focus position.
    tcGeo focus() const;

    /// Get the current altitude at the focus.
    double focusAltitude() const;

    /// Get the current range from the focus.
    double range() const;

    /// Get the position of the actual observer.
    maths::Vector<3,double> position() const;

    /// Get a ray into the scene from a point on the screen.
    maths::Vector<3,double> ray(const maths::Vector<2,double>& pos, double aspect);

  private:

    /*
     * Variables
     */

    /// Geographical position of focus.
    tcGeo m_focus;

    /// Altitude of focus.
    double m_focusAltitude;

    /// Observation direction relative to focus.
    tcGeo m_view;

    /// Range of camera.
    double m_range;
};

#endif

