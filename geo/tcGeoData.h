/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcGeoData_h_
#define _tcGeoData_h_

/**
 * @file tcGeoData.h
 * @brief Data for a section of the globe.
 */

#include "tcGeo.h"
#include <Vector.h>

#include <QDateTime>

class tcObserver;

/// Data for a section of the globe.
class tcGeoData
{
  public:

    /*
     * Types
     */

    /// Types of data.
    enum DataType
    {
      ImageData
    };

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcGeoData(DataType type);

    /// Destructor.
    virtual ~tcGeoData();

    /*
     * Rendering
     */

    /// Render schematic drawing.
    virtual void renderSchematic(double meanRadius, tcObserver* const observer);

  protected:

    /*
     * Variables
     */

    /// Type of data.
    DataType m_type;

    /// Time of collection.
    QDateTime m_captureTime;

    /// Coordinates of corners of data.
    tcGeo m_coordinates[2][2];

};

#endif

