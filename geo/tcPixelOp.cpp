/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcPixelOp.cpp
 * @brief Performs a pixel operation on a set of input channels.
 */

#include "tcPixelOp.h"

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcPixelOp::tcPixelOp(Op op, const QList<tcChannel*>& inputChannels, float scale)
: tcChannel(tr("Pixel Operation"),
            tr("Some pixel operation"))
, m_inputChannels(inputChannels)
, m_op(op)
, m_autoScale(false)
, m_radiance(false)
, m_scale(scale)
{
  Q_ASSERT(m_inputChannels.size() > 0);
  for (int c = 0; c < m_inputChannels.size(); ++c)
  {
    m_inputChannels[c]->addDerivitive(this);
  }
}

/// Primary constructor.
tcPixelOp::tcPixelOp(Op op, const QList<tcChannel*>& inputChannels, bool radiance)
: tcChannel(tr("Pixel Operation"),
            tr("Some pixel operation"))
, m_inputChannels(inputChannels)
, m_op(op)
, m_autoScale(true)
, m_radiance(radiance)
, m_scale(1.0f)
{
  Q_ASSERT(m_inputChannels.size() > 0);
  for (int c = 0; c < m_inputChannels.size(); ++c)
  {
    m_inputChannels[c]->addDerivitive(this);
  }
}

/// Destructor.
tcPixelOp::~tcPixelOp()
{
  foreach (tcChannel* channel, m_inputChannels)
  {
    channel->removeDerivitive(this);
  }
}

/*
 * Interface for derived class to implement
 */

void tcPixelOp::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  m_inputChannels.first()->roundPortion(x1, y1, x2, y2);
}

tcAbstractPixelData* tcPixelOp::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  tcPixelData<float>* data = 0;
  bool first = true;
  int width = 0;
  int height = 0;
  switch (m_op)
  {
    case Add:
    case Subtract:
    case Diff:
    {
      for (int c = 0; c < m_inputChannels.size(); ++c)
      {
        tcChannel* channel = m_inputChannels[c];
        Reference<tcAbstractPixelData> channelData = channel->portion(x1, y1, x2, y2);
        if (first)
        {
          first = false;
          width  = channelData->width();
          height = channelData->height();
          data = new tcPixelData<float>(width, height);
          for (int j = 0; j < height; ++j)
          {
            for (int i = 0; i < width; ++i)
            {
              int index = j*width+i;
              float value = m_scale*channelData->sampleFloat((float)i/(width-1), (float)j/(height-1));
              if (m_radiance)
              {
                value = channel->valueToRadiance(value);
              }
              data->buffer()[index] = value;
            }
          }
        }
        else
        {
          if (m_op == Add)
          {
            for (int j = 0; j < height; ++j)
            {
              for (int i = 0; i < width; ++i)
              {
                int index = j*width+i;
                float value = m_scale*channelData->sampleFloat((float)i/(width-1), (float)j/(height-1));
                if (m_radiance)
                {
                  value = channel->valueToRadiance(value);
                }
                data->buffer()[index] += value;
              }
            }
          }
          else if (m_op == Subtract)
          {
            for (int j = 0; j < height; ++j)
            {
              for (int i = 0; i < width; ++i)
              {
                int index = j*width+i;
                float value = m_scale*channelData->sampleFloat((float)i/(width-1), (float)j/(height-1));
                if (m_radiance)
                {
                  value = channel->valueToRadiance(value);
                }
                data->buffer()[index] -= value;
              }
            }
          }
          else if (m_op == Diff)
          {
            for (int j = 0; j < height; ++j)
            {
              for (int i = 0; i < width; ++i)
              {
                int index = j*width+i;
                data->buffer()[index] += m_scale*(0.5f - channelData->sampleFloat((float)i/(width-1), (float)j/(height-1)));
              }
            }
            break;
          }
        }
      }
      break;
    }
    case NormalizedDiff:
    {
      // "Scene shadow effects on multispectral response"
      Reference<tcAbstractPixelData> channelData1 = m_inputChannels[0]->portion(x1, y1, x2, y2);
      Reference<tcAbstractPixelData> channelData2 = m_inputChannels[1]->portion(x1, y1, x2, y2);
      for (int j = 0; j < height; ++j)
      {
        for (int i = 0; i < width; ++i)
        {
          int index = j*width+i;
          float value1 = m_scale*channelData1->sampleFloat((float)i/(width-1), (float)j/(height-1));
          float value2 = m_scale*channelData2->sampleFloat((float)i/(width-1), (float)j/(height-1));
          if (m_radiance)
          {
            value1 = m_inputChannels[0]->valueToRadiance(value1);
            value2 = m_inputChannels[1]->valueToRadiance(value2);
          }
          data->buffer()[index] = (value1-value2) / (value1+value2);
        }
      }
      break;
    }
    default:
    {
      break;
    }
  }
  if (m_autoScale)
  {
    float max = 0.0f;
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int index = j*width+i;
        if (data->buffer()[index] > max)
        {
          max = data->buffer()[index];
        }
      }
    }
    if (max > 0.0f)
    {
      float min = max*0.25f;
      for (int j = 0; j < height; ++j)
      {
        for (int i = 0; i < width; ++i)
        {
          int index = j*width+i;
          data->buffer()[index] = qMax(0.0f, qMin(1.0f, (data->buffer()[index]-min)/(max-min)));
        }
      }
    }
  }
  return data;
}
