/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcElevationData.cpp
 * @brief A block of pixel data representing elevation.
 */

#include "tcElevationData.h"

/*
 * Constructors + destructor
 */

/// Default constructor.
tcElevationData::tcElevationData()
: Parent()
, m_geoToPixels()
{
}

/// Primary constructor.
tcElevationData::tcElevationData(int width, int height)
: Parent(width, height)
, m_geoToPixels()
{
}

/// Destructor.
tcElevationData::~tcElevationData()
{
}

/*
 * Elevation interface
 */

/// Sample elevation at a coordinate.
float tcElevationData::elevationAt(const tcGeo& coord) const
{
  maths::Vector<2,double> pt = m_geoToPixels * coord;
  return sampleFloat(pt[0]/(width()-1), pt[1]/(height()-1));
}

/// Get the max elevation over a range of pixels.
float tcElevationData::maxElevation(const tcGeo& swCorner, const tcGeo& neCorner, bool* inRange) const
{
  maths::Vector<2,double> corner1 = m_geoToPixels * swCorner;
  maths::Vector<2,double> corner2 = m_geoToPixels * neCorner;

  maths::Vector<2,double> minCorner(qMin(corner1[0], corner2[0]),
                                    qMin(corner1[1], corner2[1]));
  maths::Vector<2,double> maxCorner(qMax(corner1[0], corner2[0]),
                                    qMax(corner1[1], corner2[1]));

  // Go through pixels in this range.
  int count = 0;
  float max = 0.0f;
  int minj = qMax((int)ceil(minCorner[1]), 0);
  int maxj = qMin((int)ceil(maxCorner[1]), height());
  int mini = qMax((int)ceil(minCorner[0]), 0);
  int maxi = qMin((int)ceil(maxCorner[0]), width());
  for (int j = minj; j < maxj; ++j)
  {
    for (int i = mini; i < maxi; ++i)
    {
      int index = j*width() + i;
      ++count;
      max = qMax(max, buffer()[index]);
    }
  }

  if (0 != inRange)
  {
    *inRange = true;
  }

  if (count > 0)
  {
    // Just average the samples
    return max;
  }
  else
  {
    // No samples? resort to interpolation
    maths::Vector<2,float> mid(corner1 + corner2);
    mid /= 2;
    mid[0] /= width() -1;
    mid[1] /= height()-1;
    if (mid[0] >= 0.0f && mid[0] <= 1.0f &&
        mid[1] >= 0.0f && mid[1] <= 1.0f)
    {
      return Parent::sampleFloat(mid[0], mid[1]);
    }
    else
    {
      if (0 != inRange)
      {
        *inRange = false;
      }
      return 0.0f;
    }
  }
}

/// Get the mean elevation over a range of pixels.
float tcElevationData::meanElevation(const tcGeo& swCorner, const tcGeo& neCorner, bool* inRange) const
{
  maths::Vector<2,double> corner1 = m_geoToPixels * swCorner;
  maths::Vector<2,double> corner2 = m_geoToPixels * neCorner;

  maths::Vector<2,double> minCorner(qMin(corner1[0], corner2[0]),
                                    qMin(corner1[1], corner2[1]));
  maths::Vector<2,double> maxCorner(qMax(corner1[0], corner2[0]),
                                    qMax(corner1[1], corner2[1]));

  // Go through pixels in this range.
  int count = 0;
  float total = 0.0f;
  int minj = qMax((int)ceil(minCorner[1]), 0);
  int maxj = qMin((int)ceil(maxCorner[1]), height());
  int mini = qMax((int)ceil(minCorner[0]), 0);
  int maxi = qMin((int)ceil(maxCorner[0]), width());
  for (int j = minj; j < maxj; ++j)
  {
    for (int i = mini; i < maxi; ++i)
    {
      int index = j*width() + i;
      ++count;
      total += buffer()[index];
    }
  }

  if (0 != inRange)
  {
    *inRange = true;
  }

  if (count > 0)
  {
    // Just average the samples
    return total / count;
  }
  else
  {
    // No samples? resort to interpolation
    maths::Vector<2,float> mid(corner1 + corner2);
    mid /= 2;
    mid[0] /= width() -1;
    mid[1] /= height()-1;
    if (mid[0] >= 0.0f && mid[0] <= 1.0f &&
        mid[1] >= 0.0f && mid[1] <= 1.0f)
    {
      return Parent::sampleFloat(mid[0], mid[1]);
    }
    else
    {
      if (0 != inRange)
      {
        *inRange = false;
      }
      return 0.0f;
    }
  }
}

/// Set the geographical to pixels transformation.
void tcElevationData::setGeoToPixels(const tcAffineTransform2<double>& geoToPixels)
{
  m_geoToPixels = geoToPixels;
}

/*
 * Accessors
 */

/// Get the coordinate of the most south westerly corner.
tcGeo tcElevationData::swCorner() const
{
  tcAffineTransform2<double> pixelsToGeo = m_geoToPixels.inverse();
  return (tcGeo)(pixelsToGeo * maths::Vector<2,double>(0.0, height()-1));
}

/// Get the coordinate of the most north easterly corner.
tcGeo tcElevationData::neCorner() const
{
  tcAffineTransform2<double> pixelsToGeo = m_geoToPixels.inverse();
  return (tcGeo)(pixelsToGeo * maths::Vector<2,double>(width()-1, 0.0));
}
