/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcChannelDem.cpp
 * @brief An abstract image channel which needs access to the digital elevation model.
 */

#include "tcChannelDem.h"
#include "tcSrtmModel.h"
#include "tcGeoImageData.h"

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcChannelDem::tcChannelDem(tcSrtmModel* dem, tcGeoImageData* imagery, const QString& name, const QString& description, bool depend1, bool depend2)
: tcChannel(name, description)
, m_dem(dem)
, m_imagery(imagery)
{
  if (depend1)
  {
    connect(dem, SIGNAL(dataSetChanged()), this, SLOT(invalidate()));
  }
  if (depend2)
  {
    connect(dem+1, SIGNAL(dataSetChanged()), this, SLOT(invalidate()));
  }
}

/// Destructor.
tcChannelDem::~tcChannelDem()
{
}

/*
 * Interface for derived classes to access elevation model data
 */

// Main transformation

/// Get the geographical coordinate at a pixel.
tcGeo tcChannelDem::geoAt(const maths::Vector<2,float>& textureCoordinate, tcGeoImageData* imagery) const
{
  if (imagery == 0)
  {
    imagery = m_imagery;
  }
  return (tcGeo)(imagery->texToGeo() * (maths::Vector<2,double>)textureCoordinate);
}

/// Get the texture coordinate at a geographical coordinate.
maths::Vector<2,float> tcChannelDem::textureAt(const tcGeo& geoCoordinate, tcGeoImageData* imagery) const
{
  if (imagery == 0)
  {
    imagery = m_imagery;
  }
  return (maths::Vector<2,float>)(imagery->geoToTex() * geoCoordinate);
}

// Wrapper functions

/// Get the altitude at a geographical coordinate.
float tcChannelDem::altitudeAt(const tcGeo& coordinate, bool corrected, bool* accurate) const
{
  return m_dem->altitudeAt(coordinate, corrected, accurate);
}

/// Get the texture-space normal vector at a geographical coordinate.
maths::Vector<3,float> tcChannelDem::normalAt(const tcGeo& coordinate, bool corrected, bool* accurate) const
{
  return m_dem->normalAt(coordinate, corrected, accurate);
}

/// Get the lighting direction vector at a geographical coordinate.
maths::Vector<3,float> tcChannelDem::lightDirectionAt(const tcGeo& coordinate, tcGeoImageData* imagery) const
{
  if (imagery == 0)
  {
    imagery = m_imagery;
  }
  return imagery->sunDirection(coordinate);
}

/// Find whether a point is lit by the light.
bool tcChannelDem::isLitAt(const tcGeo& coordinate, bool corrected, bool* accurate, tcGeoImageData* imagery) const
{
  return m_dem->litAt(coordinate, lightDirectionAt(coordinate, imagery), 0.0f, corrected, accurate) > 0.5f;
}

/// Find how lit a point is by the light.
float tcChannelDem::litAt(const tcGeo& coordinate, bool corrected, bool* accurate, tcGeoImageData* imagery) const
{
  // Sun is half a degree in the sky
  static const float sunSize = 0.25f * M_PI / 180.0f;
  return m_dem->litAt(coordinate, lightDirectionAt(coordinate, imagery), sunSize, corrected, accurate);
}

// Lower level accessors

/// Get the digital elevation model.
tcSrtmModel* tcChannelDem::dem() const
{
  return m_dem;
}

/// Get the imagery.
tcGeoImageData* tcChannelDem::imagery() const
{
  return m_imagery;
}
