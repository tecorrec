/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcChannelConfigWidget.cpp
 * @brief A channel configuration widget.
 */

#include "tcChannelConfigWidget.h"

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcChannelConfigWidget::tcChannelConfigWidget()
: QWidget()
{
}

/// Destructor.
tcChannelConfigWidget::~tcChannelConfigWidget()
{
}

/*
 * Main interface
 */

/// Request a texture point.
void tcChannelConfigWidget::requestTexturePoint(QObject* receiver, const char* member)
{
  emit texturePointRequested(receiver, member);
}

/// Request redraw.
void tcChannelConfigWidget::requestRedraw()
{
  emit updated();
}

/// Select a new slice.
void tcChannelConfigWidget::requestSlice(const tcGeo& sw, const tcGeo& ne)
{
  emit newSlice(sw, ne);
}
