/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcIlluminantDirection_h_
#define _tcIlluminantDirection_h_

/**
 * @file tcIlluminantDirection.h
 * @brief Illuminant direction based calculation.
 */

#include "tcChannelGroup.h"
#include "tcPixelData.h"
#include <VarVector.h>

#include <QList>

class tcChannel;

/// Illuminant direction based calculation.
class tcIlluminantDirection : public tcChannelGroup
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcIlluminantDirection(const QList<tcChannel*>& chromaticities,
                          int outputs, const QString& name, const QString& description);

    /// Destructor.
    virtual ~tcIlluminantDirection();

    /*
     * Accessors
     */

    /// Get the number of chromaticity channels.
    int numChromaticities() const;

    /// Get the list of chromaticitiy channels.
    const QList<tcChannel*>& chromaticities() const;

    /// Get the illuminant direction.
    const maths::VarVector<float>& illuminantDirection() const;

    /*
     * Main image interface
     */

    // Reimplemented
    virtual tcChannelConfigWidget* configWidget();

  private slots:

    /*
     * Private slots
     */

    /// Reset the illuminant direction vector.
    void resetIlluminantDirection();

    /// Start choosing a shadow / non shadow pair.
    void startShadowNonShadow();

    /// New shadow point.
    void selectShadowPoint(const maths::Vector<2,float>& point);

    /// New non-shadow point.
    void selectNonShadowPoint(const maths::Vector<2,float>& point);

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual void loadPortions(double x1, double y1, double x2, double y2, bool changed);

    /// Load portions using nicely accessed chromaticity data.
    virtual void loadPortions(const QList< Reference< tcPixelData<float> > >& chromaticities, int width, int height) = 0;

  private:

    /*
     * Variables
     */

    /// Input chromaticities.
    QList<tcChannel*> m_chromaticities;

    /// Current log chromaticity illuminant direction.
    maths::VarVector<float> m_illuminantDirection;

    /// Number of samples used to approximate illuminant direction.
    int m_illuminantDirectionSamples;

    /// Configuration widget.
    tcChannelConfigWidget* m_configWidget;

    /// Current shadow point.
    maths::Vector<2,float> m_shadowPoint;
};

#endif
