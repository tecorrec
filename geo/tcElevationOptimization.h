/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcElevationOptimization_h_
#define _tcElevationOptimization_h_

/**
 * @file tcElevationOptimization.h
 * @brief Optimized elevation.
 */

#include "tcChannelDem.h"
#include "tcElevationData.h"
#include "tcAffineTransform.h"

#include <qwt_array.h>

#include <QList>
#include <QMutex>

class tcShadowClassifyingData;
class tcGeoImageData;
class QwtPlot;
class QwtPlotCurve;
class QSpinBox;
class QSlider;
class QThread;
class QPushButton;
class QLabel;

/// Optimized elevation.
class tcElevationOptimization : public tcChannelDem
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcElevationOptimization(const QList<tcShadowClassifyingData*>& datasets, tcSrtmModel* dem, tcGeoImageData* imagery);

    /// Destructor.
    virtual ~tcElevationOptimization();

    /*
     * Main image interface
     */

    // Reimplemented
    virtual tcChannelConfigWidget* configWidget();

  signals:

    /// Change statistics.
    void statistics(const QString& stats);

    /// Queued non blocking signal to replot.
    void replotStdDeviations();

    /// Queued blocking signal to invalidate and redraw.
    void invalidateAndRedraw();

  private slots:

    /*
     * Slots
     */

    /// Reset DEM.
    void resetDem();

    /// Load elevation data from DEM.
    void loadFromDem();

    /// Apply hard constraints to elevation data.
    void applyHardConstraints();

    /// Void-fill bilinear.
    void voidFillBilinear();

    /// Void-fill bicubic.
    void voidFillBicubic();

    /// Start/stop optimization process.
    void startStopOptimize();

    /// Optimize a number of times.
    void optimize();

    /// Clear standard deviations.
    void clearStdDeviations();

    /// Sample standard deviations.
    void sampleStdDeviations();

    /// Sample standard deviations.
    void saveStdDeviations();

    /// Save cross section data.
    void saveCrossSections();

    /// Invalidate and redraw.
    void invalidateAndRedrawSlot();

    /// Initialise snapshotting.
    void initSnapShotting();

    /// Load a sequence of snapshots.
    void loadSnapShots();

    /// Load a single snapshot.
    void loadSnapShot(int id);

    /// Update weight labels.
    void updateWeightLabels();

    /// Save weights.
    void saveWeights();

    /// Load weights.
    void loadWeights();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  private:

    /*
     * Private functions
     */

    /** Calculate the cost for a set of pixels.
     * @param x1 Starting horizontal pixel (left).
     * @param y1 Starting vertical pixel (top).
     * @param x2 Ending horizontal pixel (right).
     * @param y2 Ending horizontal pixel (bottom).
     * @return Cost of the chosen pixels.
     */
    float cost(int x1, int y1, int x2, int y2) const;

    /** Calculate the relative cost of changing a pixel's elevation.
     * @param x Horizontal coordinate of pixel to change.
     * @param y Vertical coordinate of pixel to change.
     * @param dh Relative change in elevation.
     * @return Relative cost of altering the elevation of the specified pixel.
     */
    float costChange(int x, int y, float dh, int range = 0);

    /// Optimize a whole range of elevations.
    void optimizeElevations(int x1, int y1, int x2, int y2, float dh, int range = 0);

    /// Get pointer to elevation at a pixel.
    float* pixelElevation(int x, int y) const;

    /// Adjust elevation at a pixel.
    void adjustElevation(int x, int y, float dh, int range = 0);

    /*
     * Types
     */

    /// Worker thread class.
    class WorkerThread;

    /// Shadow transition types.
    enum TransitionType {
      TransitionEntrance = 0,
      TransitionExit = 1
    };

    /// Per-dataset-pixel data to cache
    struct PerDatasetPixelCache
    {
      /// Coordinates of shadow entrance and exit.
      maths::Vector<2,int> shadowTransition[2];
      /// Light direction.
      maths::Vector<3,float> light;
      /// Length of light direction in x y.
      float lxy;
      /// Gradiant of light direction.
      float dlz_dlxy;
      /// Distance to shadow entrance and exit.
      float shadowTransitionDistance[2];
    };

    /// Per-pixel data to cache.
    struct PerPixelCache
    {
      /// Resolution.
      maths::Vector<2,float> resolution;
      /// Original elevation.
      float originalElevation;
      /// Reliability of original elevation.
      unsigned char reliability;
      /// Accurate elevation.
      float accurateElevation;
      /// Estimated normal.
      maths::Vector<3,float> normalEstimate;
      /// Temporary values.
      union {
        float f;
        int i;
      } temporary;
    };

    /*
     * Variables
     */

    /// Number of datasets.
    int m_numDatasets;

    /// Datasets.
    tcGeoImageData** m_datasets;

    /// Transform local tex coords to dataset tex coords for each dataset.
    tcAffineTransform2<double>* m_texToDatasetTex;

    /// Shadow channel.
    tcChannel** m_shadowChannels;

    /// Shading channel.
    tcChannel** m_shadingChannels;

    /// Current elevation data.
    tcElevationData*   m_elevationData;

    /// Per-dataset-pixel data.
    tcTypedPixelData<PerDatasetPixelCache>** m_data;

    /// Per-pixel data.
    tcTypedPixelData<PerPixelCache>* m_datum;

    /// Temporary shadow data.
    Reference<tcPixelData<float> >* m_shadowData;

    /// Temporary shading data.
    Reference<tcPixelData<float> >* m_shadingData;

    enum Weights
    {
      Variance,
      Roughness,
      Steepness,
      LitFacing,
      Shading,
      PhotometricStereo,
      BelowShadowLine,
      TransitionElevTop,
      TransitionElevBottom,
      TransitionTangential,
      TransitionCurvingDown,
      ElevationStep,
      Range,
      WeightsMax
    };

    /// Weights.
    float m_weights[WeightsMax];

    /// Weight sliders.
    QSlider* m_weightSliders[2][WeightsMax];

    /// Weight labels.
    QLabel* m_weightLabels[WeightsMax];

    /// Weight limits.
    float m_weightLimits[2][WeightsMax];

    /// Update voids only.
    bool m_voidsOnly;

    /// Currently loading from dem.
    bool m_loadingFromDem;

    /// Main processing thread.
    QThread* m_thread;

    /// Main processing mutex.
    QMutex m_processingMutex;

    /// Signal to stop processing.
    bool m_stopProcessing;

    /// Optimize toggle button.
    QPushButton* m_btnOptimize;

    /*
     * Configuration gui
     */

    /// Configuration widget.
    tcChannelConfigWidget* m_configWidget;

    /// Number of iterations to perform.
    QSpinBox* m_spinIterations;

    /// Plot of standard deviations.
    QwtPlot* m_plot;

    /// Plot x coordinate data.
    QwtArray<double> m_plotX;

    /// Standard deviation curve.
    QwtPlotCurve* m_stdDevCurve;

    /// Standard deviation data.
    QwtArray<double> m_stdDevData;

    /// Standard deviation curve for void pixels.
    QwtPlotCurve* m_stdDevVoidCurve;

    /// Standard deviation data.
    QwtArray<double> m_stdDevVoidData;

    /// Standard deviation curve for non-void pixels.
    QwtPlotCurve* m_stdDevNonVoidCurve;

    /// Standard deviation data.
    QwtArray<double> m_stdDevNonVoidData;

    /// Cross sectioning resolution.
    QSpinBox* m_crossResolution;

    /// Cross sectioning frequency.
    QSpinBox* m_crossFrequency;

    /// Snapshotting config file.
    QString m_snapshotFile;

    /// Snapshot read file.
    QString m_snapshotReadFile;

    /// Whether currently reading snapshots.
    bool m_snapshotReading;

    /// Snapshot time slider.
    QSlider* m_snapshotSlider;

    /// Counter of snapshot frames.
    QLabel* m_snapshotCounter;
};

#endif
