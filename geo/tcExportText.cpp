/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcExportText.cpp
 * @brief Exports a set of channels to a file.
 */

#include "tcExportText.h"
#include "tcChannel.h"
#include "tcChannelManager.h"
#include "tcGeoImageData.h"
#include "tcPixelData.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QListWidget>
#include <QComboBox>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcExportText::tcExportText(const QList<tcGeoImageData*>& imagery, QWidget* parent)
: QWidget(parent)
, m_imagery(imagery)
{
  tcChannelManager* channelManager = m_imagery[0]->channelManager();
  setWindowTitle(tr("Export as Text"));

  QVBoxLayout* layout = new QVBoxLayout(this);

  QLabel* labelClassificationChannel = new QLabel(tr("Classification channel"), this);
    layout->addWidget(labelClassificationChannel);
  m_comboClassificationChannel = new QComboBox(this);
    layout->addWidget(m_comboClassificationChannel);
    labelClassificationChannel->setBuddy(m_comboClassificationChannel);
    for (int i = 0; i < channelManager->numChannels(); ++i)
    {
      tcChannel* channel = channelManager->channel(i);
      m_comboClassificationChannel->addItem(channel->name());
    }
    connect(m_comboClassificationChannel, SIGNAL(currentIndexChanged(int)),
            this,    SLOT(setClassificationChannel(int)));

  QLabel* labelFilterChannel = new QLabel(tr("Filter channel"), this);
    layout->addWidget(labelFilterChannel);
  m_comboFilterChannel = new QComboBox(this);
    layout->addWidget(m_comboFilterChannel);
    labelFilterChannel->setBuddy(m_comboFilterChannel);
    m_comboFilterChannel->addItem(tr("(Export all pixels)"));
    for (int i = 0; i < channelManager->numChannels(); ++i)
    {
      tcChannel* channel = channelManager->channel(i);
      m_comboFilterChannel->addItem(channel->name());
    }
    connect(m_comboFilterChannel, SIGNAL(currentIndexChanged(int)),
            this,    SLOT(setFilterChannel(int)));
  m_comboFilterType = new QComboBox(this);
    layout->addWidget(m_comboFilterType);
    m_comboFilterType->addItem(tr("Export dark values"));
    m_comboFilterType->addItem(tr("Export light values"));
    m_comboFilterType->setEnabled(false);
    connect(m_comboFilterType, SIGNAL(currentIndexChanged(int)),
            this,    SLOT(setFilterType(int)));

  QLabel* labelChannels = new QLabel(tr("Channels to export"), this);
    layout->addWidget(labelChannels);
  m_listChannels = new QListWidget(this);
    layout->addWidget(m_listChannels);
    labelChannels->setBuddy(m_listChannels);
    for (int i = 0; i < channelManager->numChannels(); ++i)
    {
      tcChannel* channel = channelManager->channel(i);
      m_listChannels->addItem(channel->name());
      QListWidgetItem* item = m_listChannels->item(i);
      item->setCheckState(Qt::Unchecked);
    }

  QPushButton* btnSave = new QPushButton(tr("Export"), this);
    layout->addWidget(btnSave);
    connect(btnSave, SIGNAL(clicked(bool)), this, SLOT(exportToFile()));
}

/// Destructor.
tcExportText::~tcExportText()
{
}

/*
 * Public slots
 */

/// Export to a file.
void tcExportText::exportToFile()
{
  tcChannelManager* channelManager = m_imagery[0]->channelManager();

  // Get the data together.
  tcChannel* classification = channelManager->channel(m_comboClassificationChannel->currentIndex());
  tcChannel* filter = channelManager->channel(m_comboFilterChannel->currentIndex()-1);
  int filterType = m_comboFilterType->currentIndex();
  QList<tcChannel*> channels;
  for (int i = 0; i < m_listChannels->count(); ++i)
  {
    if (m_listChannels->item(i)->checkState() == Qt::Checked)
    {
      tcChannel* channel = channelManager->channel(i);
      if (0 != channel)
      {
        channels += channel;
      }
    }
  }
  Q_ASSERT(0 != classification);
  Q_ASSERT(filterType < 2);

  // Check there are enough channels.
  if (channels.count() == 0)
  {
    QMessageBox::warning(this,
                         tr("No Channels Ticked."),
                         tr("You haven't ticked any channels to export."));
    return;
  }

  // Get the portion of the image
  Q_ASSERT(0);
  /*
  tcGeoImageData::LocalCoord min = m_imagery[0]->minLocal();
  tcGeoImageData::LocalCoord range = m_imagery[0]->rangeLocal();
  tcGeoImageData::LocalCoord minEffective = m_imagery[0]->minEffectiveLocal();
  tcGeoImageData::LocalCoord rangeEffective = m_imagery[0]->rangeEffectiveLocal();
  double rx1 = (minEffective.x - min.x) / range.x;
  double ry1 = 1.0 - (minEffective.y + rangeEffective.y - min.y) / range.y;
  double rx2 = (minEffective.x + rangeEffective.x - min.x) / range.x;
  double ry2 = 1.0 - (minEffective.y - min.y) / range.y;
  */
  double rx1 = 0.0;
  double ry1 = 0.0;
  double rx2 = 1.0;
  double ry2 = 1.0;

  double x1,y1,x2,y2;

#define REWRITE_PORTION() \
    { x1 = rx1; y1 = ry1; x2 = rx2; y2 = ry2; }

  // Get all the pixel data objects
  REWRITE_PORTION();
  Reference<tcPixelData<float> > classificationData = dynamicCast<tcPixelData<float>*>(classification->portion(&x1,&y1,&x2,&y2));
  if (0 == classificationData)
  {
    QMessageBox::warning(this,
                         tr("No Classification Data."),
                         tr("No classification pixel data was found in this portion."));
    return;
  }
  Reference<tcAbstractPixelData> filterData;
  if (0 != filter)
  {
    REWRITE_PORTION();
    filterData = filter->portion(&x1,&y1,&x2,&y2);
    if (0 == filterData)
    {
      QMessageBox::warning(this,
          tr("No Filter Data."),
          tr("No filter pixel data was found in this portion."));
      return;
    }
  }
  QList<Reference<tcAbstractPixelData> > channelsData;
  foreach (tcChannel* channel, channels)
  {
    REWRITE_PORTION();
    channelsData += channel->portion(&x1,&y1,&x2,&y2);
    if (0 == channelsData[channelsData.count()-1])
    {
      QMessageBox::warning(this,
          tr("No %1 Data.").arg(channel->name()),
          tr("No %1 pixel data was found in this portion.").arg(channel->name()));
      return;
    }
  }

  QString filename = QFileDialog::getSaveFileName(this,
                        tr("Export channels"),
                        QString(),
                        tr("ASCII Data Files (*.adf)"));

  if (!filename.isEmpty())
  {
    // Open the file ready to write the data
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
      QMessageBox::warning(this,
          tr("Open failed."),
          tr("Could not open '%1' for writing.").arg(filename));
      return;
    }

    QTextStream out(&file);

    // Go through the classification data and sample the other channels
    int width = classificationData->width();
    int height = classificationData->height();
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int index = j*height + i;
        float x = (float)i/(width-1);
        float y = (float)j/(height-1);
        // Check filter
        if (0 != filterData)
        {
          float val = filterData->sampleFloat(x,y);
          if (filterType == 0)
          {
            if (val > 0.0f)
            {
              continue;
            }
          }
          else if (filterType == 1)
          {
            if (val <= 0.0f)
            {
              continue;
            }
          }
        }
        // Write vales
        out << classificationData->buffer()[index];
        for (int i = 0; i < channelsData.count(); ++i)
        {
          out << " " << channelsData[i]->sampleFloat(x, y);
        }
        out << "\n";
      }
    }
  }
}

/*
 * Private slots
 */

/// Set the channel to classify the pixels.
void tcExportText::setClassificationChannel(int channel)
{
}

/// Set the channel to filter which pixels to export.
void tcExportText::setFilterChannel(int channel)
{
  m_comboFilterType->setEnabled(channel != 0);
}

/// Set the type of filtering.
void tcExportText::setFilterType(int channel)
{
}
