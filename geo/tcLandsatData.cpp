/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcLandsatData.cpp
 * @brief Landsat image data.
 */

#include "tcLandsatData.h"
#include "tcLandsatMetaData.h"
#include "tcSrtmModel.h"
#include "tcChannelManager.h"
#include "tcChannelFile.h"
#include "tcChannelChromaticity.h"
#include "tcShadowFreeChromaticities.h"
#include "tcIlluminantDiscontinuity.h"
#include "tcShadowClassification.h"
#include "tcNegativeProduct.h"
#include "tcLambertianShading.h"
#include "tcRaytracedShadowMap.h"
#include "tcShadowDepth.h"
#include "tcPixelOp.h"

#include <QDir>
#include <QFile>
#include <QColor>

/*
 * Constructors + destructor
 */

#include <QtDebug>
/// Load landsat data given the path.
tcLandsatData::tcLandsatData(const QString& path, tcSrtmModel* dem)
: tcShadowClassifyingData()
, m_meta(0)
{
  // Find name of txt file automatically
  QDir dir(path);
  QStringList textFiles = dir.entryList(QStringList() << "*_MTL.txt" << "*.met");
  foreach (QString textFile, textFiles)
  {
    QFile metaFile(path + textFile);
    if (metaFile.open(QIODevice::ReadOnly))
    {
      m_meta = new tcLandsatMetaData(metaFile);
      tcLandsatMetaData::Reference meta = m_meta;

      // Landsat 7 data looks like this
      tcLandsatMetaData::Reference l1_metadata_file = meta("L1_METADATA_FILE");
      if (l1_metadata_file)
      {
        tcLandsatMetaData::Reference productMetadata = l1_metadata_file("PRODUCT_METADATA");
        tcLandsatMetaData::Reference minMaxRadiance = l1_metadata_file("MIN_MAX_RADIANCE");
        tcLandsatMetaData::Reference minMaxPixelValue = l1_metadata_file("MIN_MAX_PIXEL_VALUE");

        setName(QObject::tr("%1 %2 (%3)").arg(productMetadata["SPACECRAFT_ID"].toString())
                                         .arg(productMetadata["SENSOR_ID"].toString())
                                         .arg(productMetadata["ACQUISITION_DATE"].toString()));

        // Load coordinates
        m_coordinates[0][0] = tcGeo(productMetadata["PRODUCT_UL_CORNER_LON"].toDouble() * M_PI/180.0,
                                    productMetadata["PRODUCT_UL_CORNER_LAT"].toDouble() * M_PI/180.0);
        m_coordinates[1][0] = tcGeo(productMetadata["PRODUCT_UR_CORNER_LON"].toDouble() * M_PI/180.0,
                                    productMetadata["PRODUCT_UR_CORNER_LAT"].toDouble() * M_PI/180.0);
        m_coordinates[0][1] = tcGeo(productMetadata["PRODUCT_LL_CORNER_LON"].toDouble() * M_PI/180.0,
                                    productMetadata["PRODUCT_LL_CORNER_LAT"].toDouble() * M_PI/180.0);
        m_coordinates[1][1] = tcGeo(productMetadata["PRODUCT_LR_CORNER_LON"].toDouble() * M_PI/180.0,
                                    productMetadata["PRODUCT_LR_CORNER_LAT"].toDouble() * M_PI/180.0);

        tcLandsatMetaData::Reference productParameters = l1_metadata_file("PRODUCT_PARAMETERS");
        setSunDirection(tcGeo(M_PI - M_PI/180.0 * productParameters["SUN_AZIMUTH"].toDouble(),
                              M_PI/180.0 * productParameters["SUN_ELEVATION"].toDouble()),
                        m_coordinates[1][0]);

        // Names and descriptions of channels
        QStringList channelNames;
        channelNames << QObject::tr("Blue Green");
        channelNames << QObject::tr("Green");
        channelNames << QObject::tr("Red");
        channelNames << QObject::tr("Near IR");
        channelNames << QObject::tr("Mid IR 1");
        channelNames << QObject::tr("Thermal IR Low");
        channelNames << QObject::tr("Thermal IR High");
        channelNames << QObject::tr("Mid IR 2");
        channelNames << QObject::tr("Panchromatic");

        // Get the filenames of the bands
        QString bandCombo = productMetadata["BAND_COMBINATION"].toString();
        QChar last = 0;
        int sameCount = 0;
        for (int band = 0; band < bandCombo.size(); ++band)
        {
          QChar code = bandCombo.at(band);
          QChar next = (band+1 < bandCombo.size() ? bandCombo.at(band+1) : 0);
          QString bandNumber;
          QString bandName;
          QString description;
          if (code == next || last == code)
          {
            ++sameCount;
            bandNumber = QString("%1%2").arg(code).arg(sameCount);
            description = QObject::tr("Band %1.%2").arg(code).arg(sameCount);
          }
          else
          {
            sameCount = 0;
            bandNumber = QString("%1").arg(code);
            description = QObject::tr("Band %1").arg(code);
          }
          bandName = QString("BAND%1_FILE_NAME").arg(bandNumber);
          float lMin = minMaxRadiance[QString("LMIN_BAND%1").arg(bandNumber)].toDouble();
          float lMax = minMaxRadiance[QString("LMAX_BAND%1").arg(bandNumber)].toDouble();
          float qCalMin = minMaxPixelValue[QString("QCALMIN_BAND%1").arg(bandNumber)].toDouble();
          float qCalMax = minMaxPixelValue[QString("QCALMAX_BAND%1").arg(bandNumber)].toDouble();
          float offset = lMin;
          float gain = (lMax - lMin) / (qCalMax - qCalMin);
          tcChannelFile* channel = new tcChannelFile(path + productMetadata[bandName].toString(),
                                                     channelNames[band], description,
                                                     offset, gain);
          channelManager()->addChannel(channel);
          // Obtain the transformation from the first of the files.
          if (0 == band)
          {
            setTexToGeo(channel->texToGeo());
          }
          last = code;
        }
      }
      // Landsat 5 data is a little different
      else
      {
        tcLandsatMetaData::Reference inventoryMetaData = meta("INVENTORYMETADATA");
        tcLandsatMetaData::Reference sensorCharacteristics = inventoryMetaData("SENSORCHARACTERISTIC")("SENSORCHARACTERISTICCONTAINER",0);
        tcLandsatMetaData::Reference singleDateTime = inventoryMetaData("SINGLEDATETIME");

        setName(QObject::tr("%1 %2 (%3)").arg(sensorCharacteristics("PLATFORMSHORTNAME",0)["VALUE"].toString())
                                         .arg(sensorCharacteristics("SENSORSHORTNAME",0)["VALUE"].toString())
                                         .arg(singleDateTime("CALENDARDATE",0)["VALUE"].toString()));

        // Load additional attributes into a nice hash
        tcLandsatMetaData::Reference additionalAttributes = inventoryMetaData("ADDITIONALATTRIBUTES");
        QHash<QString, QVariant> attributes;
        int numAttributes = additionalAttributes.hasObject("ADDITIONALATTRIBUTESCONTAINER");
        for (int i = 0; i < numAttributes; ++i)
        {
          tcLandsatMetaData::Reference container = additionalAttributes("ADDITIONALATTRIBUTESCONTAINER", i);
          QString name = container("ADDITIONALATTRIBUTENAME",0)["VALUE"].toString();
          attributes[name] = container("INFORMATIONCONTENT")("PARAMETERVALUE", 0)["VALUE"];
        }

        // Lon/Lat coordinates
        tcLandsatMetaData::Reference gRingPoint = inventoryMetaData("SPATIALDOMAINCONTAINER")
                                                                   ("HORIZONTALSPATIALDOMAINCONTAINER")
                                                                   ("GPOLYGON")
                                                                   ("GPOLYGONCONTAINER",0)
                                                                   ("GRINGPOINT");
        const QList<QVariant>& lons = gRingPoint("GRINGPOINTLONGITUDE",0)["VALUE"].toList();
        const QList<QVariant>& lats = gRingPoint("GRINGPOINTLATITUDE",0)["VALUE"].toList();

        m_coordinates[0][0] = tcGeo(lons[0].toDouble() * M_PI/180.0,
                                    lats[0].toDouble() * M_PI/180.0);
        m_coordinates[1][0] = tcGeo(lons[1].toDouble() * M_PI/180.0,
                                    lats[1].toDouble() * M_PI/180.0);
        m_coordinates[0][1] = tcGeo(lons[3].toDouble() * M_PI/180.0,
                                    lats[3].toDouble() * M_PI/180.0);
        m_coordinates[1][1] = tcGeo(lons[2].toDouble() * M_PI/180.0,
                                    lats[2].toDouble() * M_PI/180.0);

        // Sun direction
        setSunDirection(tcGeo(M_PI - M_PI/180.0 * attributes["SolarAzimuth"].toDouble(),
                                     M_PI/180.0 * attributes["SolarElevation"].toDouble()),
                        m_coordinates[1][0]);

        // Names and descriptions of channels
        QStringList channelNames;
        channelNames << QObject::tr("Blue Green");
        channelNames << QObject::tr("Green");
        channelNames << QObject::tr("Red");
        channelNames << QObject::tr("Near IR");
        channelNames << QObject::tr("Mid IR 1");
        channelNames << QObject::tr("Thermal IR");
        channelNames << QObject::tr("Mid IR 2");

        // Get input files and filter tif files
        int band = 0;
        foreach (QVariant file, inventoryMetaData("INPUTGRANULE")("INPUTPOINTER",0)["VALUE"].toList())
        {
          static QRegExp tifExt("^.*\\.tif$");
          if (tifExt.exactMatch(file.toString()))
          {
            float bias = attributes[QString("Band%1GainSetting").arg(band+1)].toDouble();
            float gain = attributes[QString("Band%1BiasSetting").arg(band+1)].toDouble();
            if (gain == 0.0f)
            {
              gain = 1.0f;
            }

            tcChannelFile* channel = new tcChannelFile(path + file.toString(),
                                                       channelNames[band], QString("Band %1").arg(band),
                                                       bias, gain);
            channelManager()->addChannel(channel);
            // Obtain the transformation from the first of the files.
            if (0 == band)
            {
              setTexToGeo(channel->texToGeo());
            }
            ++band;
          }
        }
      }



      QList<tcChannel*> mainColourChannels;
      for (int i = 0; i < channelManager()->numChannels(); ++i)
      {
        mainColourChannels += channelManager()->channel(i);
      }
      tcNegativeProduct* shadowClassification = new tcNegativeProduct(mainColourChannels);
      channelManager()->addChannel(shadowClassification);
      setShadowClassification(shadowClassification);

      //tcPixelOp* normalizedDiff = new tcPixelOp(tcPixelOp::NormalizedDiff, QList<tcChannel*>() << mainColourChannels[3] << mainColourChannels[2]);
      //channelManager()->addChannel(normalizedDiff);

      tcShadowDepth* shadowDepth1 = new tcShadowDepth(shadowClassification, dem, this, false);
      channelManager()->addChannel(shadowDepth1);
      tcShadowDepth* shadowDepth2 = new tcShadowDepth(shadowClassification, dem, this, true);
      channelManager()->addChannel(shadowDepth2);

      tcLambertianShading* diffuse = new tcLambertianShading(channelManager()->channel(0), dem, this);
      channelManager()->addChannel(diffuse);

      tcRaytracedShadowMap* raytrace = new tcRaytracedShadowMap(channelManager()->channel(0), dem, this);
      channelManager()->addChannel(raytrace);

      tcChannel* shading = new tcPixelOp(tcPixelOp::Add, QList<tcChannel*>()
        << channelManager()->channel(0) // b
        << channelManager()->channel(1) // g
        << channelManager()->channel(2) // r
        << channelManager()->channel(3) // nir
        //<< channelManager()->channel(4) // mir
        //<< channelManager()->channel(5) // tir
        //<< channelManager()->channel(6) // tir
        //<< channelManager()->channel(channelManager()->numChannels()-2) // mir
        //<< channelManager()->channel(channelManager()->numChannels()-1) // pan
        //, 1.0f / 800
        , false
        );
      channelManager()->addChannel(shading);
      setShading(shading);

      /*
      QList<tcChannel*> chromaticities;
      for (int i = 0; i <= 8; ++i)
      {
        chromaticities += new tcChannelChromaticity(channelManager()->channel(0), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(1), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(2), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(3), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(4), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(5), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(6), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(7), channelManager()->channel(i));
        chromaticities += new tcChannelChromaticity(channelManager()->channel(8), channelManager()->channel(i));
      }
      channelManager()->addChannels(chromaticities);
      */

      /*
      tcShadowFreeChromaticities* sfc = new tcShadowFreeChromaticities(chromaticities);
      channelManager()->addChannels(sfc->channels());
      tcIlluminantDiscontinuity* idm = new tcIlluminantDiscontinuity(chromaticities);
      channelManager()->addChannels(idm->channels());
      tcShadowClassification* sc = new tcShadowClassification(idm, channelManager()->channel(4));
      channelManager()->addChannel(sc);
      */

      shadowClassification->loadParams(path + "negativeproduct.npp");

      // Only load one metafile
      break;
    }
  }
}

/// Destructor.
tcLandsatData::~tcLandsatData()
{
}
