/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcChannelManager.cpp
 * @brief Manages a set of channels, any of which can be mapped to RGB.
 */

#include "tcChannelManager.h"
#include "tcChannel.h"

/*
 * Constructors + destructor
 */

/// Default constructor.
tcChannelManager::tcChannelManager()
: m_channels()
{
}

/// Destructor.
tcChannelManager::~tcChannelManager()
{
}

/*
 * Accessors
 */

/// Get the number of channel.
int tcChannelManager::numChannels() const
{
  return m_channels.size();
}

/// Get a specific channel.
tcChannel* tcChannelManager::channel(int index) const
{
  if (index >= 0 && index < m_channels.count())
  {
    return m_channels[index];
  }
  else
  {
    return 0;
  }
}

/*
 * Channel operations
 */

/// Add and take ownership of a channel.
int tcChannelManager::addChannel(tcChannel* channel)
{
  m_channels += channel;
  channel->setChannelManager(this);
  connect(channel, SIGNAL(progressing(const QString&)), this, SIGNAL(progressing(const QString&)));
  return m_channels.size()-1;
}

/// Add and take ownership of a set of channels.
void tcChannelManager::addChannels(const QList<tcChannel*>& channels)
{
  foreach (tcChannel* channel, channels)
  {
    addChannel(channel);
  }
}
