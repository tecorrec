/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcLambertianShading_h_
#define _tcLambertianShading_h_

/**
 * @file tcLambertianShading.h
 * @brief Shading channel using lambertian reflectance.
 */

#include "tcChannelDem.h"

/// Shading channel using lambertian reflectance.
class tcLambertianShading : public tcChannelDem
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcLambertianShading(tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery);

    /// Primary constructor.
    tcLambertianShading(tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery, const maths::Vector<3,float>& direction);

    /// Destructor.
    virtual ~tcLambertianShading();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  private:

    /*
     * Variables
     */

    /// We'll use the same resolution as this reference channel.
    tcChannel* m_referenceChannel;

    /// Whether to use a custom direction instead of the light direction.
    bool m_customDirectionEnabled;

    /// What custom direction to use instead of the light direction.
    maths::Vector<3,float> m_customDirection;
};

#endif
