/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcCustomSunDirection.cpp
 * @brief Dataset with raytraced shadow classification using a custom sun direction.
 */

#include "tcCustomSunDirection.h"
#include "tcSrtmModel.h"
#include "tcChannelManager.h"
#include "tcLambertianShading.h"
#include "tcRaytracedShadowMap.h"

#include <QObject>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcCustomSunDirection::tcCustomSunDirection(tcSrtmModel* dem, const tcGeo& sunDirection, const tcGeo& sunReference, bool shading)
: tcShadowClassifyingData()
{
  setName(QObject::tr("Custom sun direction"));
  //setTexToGeo(landsatDatasets[0]->texToGeo());
  setSunDirection(sunDirection, sunReference);

/*
  tcGeoImageData* refImagery = landsatDatasets[0];
  tcChannel* reference = refImagery->channelManager()->channel(0);

  tcLambertianShading* upshading = new tcLambertianShading(reference, dem, this, maths::Vector<3,float>(0.0f, 0.0f, 1.0f));
  channelManager()->addChannel(upshading);
  */

  double pixelRes = 30.0/6378.137e3;
  tcRaytracedShadowMap* raytrace = new tcRaytracedShadowMap(tcGeo(pixelRes, pixelRes), dem, this);
  channelManager()->addChannel(raytrace);
  setShadowClassification(raytrace);

  tcLambertianShading* diffuse = new tcLambertianShading(channelManager()->channel(0), dem, this);
  channelManager()->addChannel(diffuse);
  setShading(diffuse);
}

/// Destructor.
tcCustomSunDirection::~tcCustomSunDirection()
{
}
