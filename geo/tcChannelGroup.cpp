/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcChannelGroup.cpp
 * @brief A group of linked channels.
 */

#include "tcChannelGroup.h"
#include "tcChannel.h"

/// Our custom channel group member class.
class tcChannelGroup::Channel : public tcChannel
{
  public:
    
    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    Channel(tcChannelGroup* group, int index)
    : tcChannel(QString(), QString())
    , m_group(group)
    , m_index(index)
    {
    }

    /// Destructor
    virtual ~Channel()
    {
    }

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual tcChannelConfigWidget* configWidget()
    {
      return m_group->configWidget();
    }

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2)
    {
      m_group->roundPortion(x1, y1, x2, y2);
    }

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed)
    {
      return m_group->portion(m_index, x1, y1, x2, y2, changed);
    }

  private:

    /*
     * Variables
     */

    /// Group of channels.
    tcChannelGroup* m_group;

    /// Index of this channel in m_group.
    int m_index;
};

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcChannelGroup::tcChannelGroup(int channels, const QString& name, const QString& description)
: m_name(name)
, m_description(description)
{
  for (int i = 0; i < channels; ++i)
  {
    Channel* channel = new Channel(this, i);
    m_channels << channel;
  }
}

/// Destructor.
tcChannelGroup::~tcChannelGroup()
{
  foreach (tcChannel* channel, m_channels)
  {
    delete channel;
  }
}

/*
 * Metadata
 */

/// Get the channel name.
const QString& tcChannelGroup::name() const
{
  return m_name;
}

/// Get the channel description;
const QString& tcChannelGroup::description() const
{
  return m_description;
}

/// Set the channel name.
void tcChannelGroup::setName(const QString& name)
{
  m_name = name;
}

/// Set the channel description.
void tcChannelGroup::setDescription(const QString& description)
{
  m_description = description;
}

/*
 * Main image interface
 */

/// Get the list of output channels.
const QList<tcChannel*>& tcChannelGroup::channels() const
{
  return m_channels;
}

/// Get configuration widget.
tcChannelConfigWidget* tcChannelGroup::configWidget()
{
  return 0;
}

/// Get a reference to the pixel data of a portion of one of the output channels.
Reference<tcAbstractPixelData> tcChannelGroup::portion(int channel, double x1, double y1, double x2, double y2, bool changed)
{
  // If the portion is out of date, remove it
  if (0 != m_portions.size())
  {
    if (m_portionPosition[0][0] != x1 ||
        m_portionPosition[0][1] != y1 ||
        m_portionPosition[1][0] != x2 ||
        m_portionPosition[1][1] != y2)
    {
      m_portions.clear();
    }
  }
  m_portionPosition[0][0] = x1;
  m_portionPosition[0][1] = y1;
  m_portionPosition[1][0] = x2;
  m_portionPosition[1][1] = y2;
  // Create the new portions
  if (0 == m_portions.size())
  {
    loadPortions(x1,y1,x2,y2, changed);
  }
  // Return just the requested channel
  if (channel >= 0 && channel < m_portions.size())
  {
    return m_portions[channel];
  }
  else
  {
    return 0;
  }
}

/*
 * Interface for derived class to implement
 */

/// Round coordinates to sensible values.
void tcChannelGroup::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
}

/// Load portions of pixel data for each output channel.
void tcChannelGroup::loadPortions(double x1, double y1, double x2, double y2, bool changed)
{
}

/*
 * Interface for derived classes
 */

/// Invalidate this channel.
void tcChannelGroup::invalidate()
{
  m_portions.clear();
  foreach (tcChannel* channel, m_channels)
  {
    channel->invalidate();
  }
}
