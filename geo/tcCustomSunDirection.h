/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcCustomSunDirection_h_
#define _tcCustomSunDirection_h_

/**
 * @file tcCustomSunDirection.h
 * @brief Dataset with raytraced shadow classification using a custom sun direction.
 */

#include "tcShadowClassifyingData.h"

class tcSrtmModel;

/// Dataset with raytraced shadow classification using a custom sun direction.
class tcCustomSunDirection : public tcShadowClassifyingData
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcCustomSunDirection(tcSrtmModel* dem, const tcGeo& sunDirection, const tcGeo& sunReference, bool shading = false);

    /// Destructor.
    virtual ~tcCustomSunDirection();
};

#endif

