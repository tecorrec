/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcPixelData_h_
#define _tcPixelData_h_

/**
 * @file tcPixelData.h
 * @brief A block of pixel data.
 */

#include "CountedReference.h"

#include <QtGlobal>

#include <GL/gl.h>

#include <cstring>

/// Abstract pixel data, independent of type.
class tcAbstractPixelData : public ReferenceCounted
{
  public:

    /*
     * Types
     */

    /// Delete when no more references.
    typedef Referencer<tcAbstractPixelData> DefaultReferencer;

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcAbstractPixelData(int width, int height);

    /// Destructor.
    virtual ~tcAbstractPixelData();

    /*
     * Accessors
     */

    /// Get the width of the image.
    int width() const;

    /// Get the height of the image.
    int height() const;

    /// Get (creating if necessary) a GL texture for this image.
    GLuint texture();

    /// Discard of any cached texture id for this image.
    void discardTexture();

    /// Sample a byte value from the image.
    virtual GLubyte sampleUByte(float x, float y) const
    {
      return 0;
    }

    /// Sample a floating point value from the image.
    virtual float sampleFloat(float x, float y) const
    {
      return 0.0f;
    }

  protected:

    /*
     * Virtual interface for derived classes to implement.
     */

    /// Load the pixel data into a GL texture.
    virtual GLuint loadTexture();

  protected:

    /*
     * Variables
     */

    /// Size of pixel data.
    int m_size[2];

    /// Also keep reference to an OpenGL texture id for this texture.
    GLuint m_textureId;
};

/// A block of fairly abstract but typed pixel data.
template <typename T>
class tcTypedPixelData : public tcAbstractPixelData
{
  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcTypedPixelData()
    : tcAbstractPixelData(0, 0)
    , m_data(0)
    {
    }

    /// Primary constructor.
    tcTypedPixelData(int width, int height)
    : tcAbstractPixelData(width, height)
    , m_data(new T[width*height])
    {
    }

    /// Destructor.
    virtual ~tcTypedPixelData()
    {
      delete [] m_data;
    }

    /*
     * Accessors
     */

    /// Get pointer to the buffer.
    T* buffer() const
    {
      return m_data;
    }

    T interpolateLinear(float x, float y) const
    {
      if (x < 0.0) x = 0.0;
      if (x > 1.0) x = 1.0;
      if (y < 0.0) y = 0.0;
      if (y > 1.0) y = 1.0;

      float dx = x*(width()-1);
      float dy = y*(height()-1);
      int xi = (int)dx;
      int yi = (int)dy;
      dx -= xi;
      dy -= yi;
      if (xi >= width()-1)
      {
        --xi;
        dx = 1.0f;
      }
      if (yi >= height()-1)
      {
        --yi;
        dy = 1.0f;
      }

      return buffer()[yi*width() + xi] * (1.0f - dx) * (1.0f - dy) +
             buffer()[yi*width() + xi+1] * dx * (1.0f - dy) +
             buffer()[(yi+1)*width() + xi] * (1.0f - dx) * dy +
             buffer()[(yi+1)*width() + xi+1] * dx * dy;
    }

  private:

    /*
     * Variables
     */

    /// Parent pixel data into which we are looking.
    T* m_data;
};

/// A block of pixel data.
template <typename T>
class tcPixelData : public tcTypedPixelData<T>
{
  private:

    /*
     * Types
     */

    // Parent type.
    typedef tcTypedPixelData<T> Parent;

  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcPixelData()
    : Parent()
    {
    }

    /// Primary constructor.
    tcPixelData(int width, int height)
    : Parent(width, height)
    {
    }

    /// Destructor.
    virtual ~tcPixelData()
    {
    }

    /*
     * Accessors
     */

    // Reimplemented
    virtual GLubyte sampleUByte(float x, float y) const
    {
      return (GLubyte)tcPixelData::sampleFloat(x,y);
    }

    // Reimplemented
    virtual float sampleFloat(float x, float y) const
    {
      if (x < 0.0) x = 0.0;
      if (x > 1.0) x = 1.0;
      if (y < 0.0) y = 0.0;
      if (y > 1.0) y = 1.0;

      float dx = x*(Parent::width()-1);
      float dy = y*(Parent::height()-1);
      int xi = (int)dx;
      int yi = (int)dy;
      dx -= xi;
      dy -= yi;
      if (xi >= Parent::width()-1)
      {
        --xi;
        dx = 1.0f;
      }
      if (yi >= Parent::height()-1)
      {
        --yi;
        dy = 1.0f;
      }

      return (float)Parent::buffer()[yi*Parent::width() + xi] * (1.0f - dx) * (1.0f - dy) +
             (float)Parent::buffer()[yi*Parent::width() + xi+1] * dx * (1.0f - dy) +
             (float)Parent::buffer()[(yi+1)*Parent::width() + xi] * (1.0f - dx) * dy +
             (float)Parent::buffer()[(yi+1)*Parent::width() + xi+1] * dx * dy;
    }

  protected:

    /*
     * Virtual interface for derived classes to implement.
     */

    // Reimplemented
    virtual GLuint loadTexture();
};

#endif
