/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcGlobe.cpp
 * @brief Manages data for a globe.
 */

#include "tcGlobe.h"
#include "tcGeoImageData.h"
#include "tcLandsatData.h"
#include "tcProcessingData.h"
#include "tcCustomSunDirection.h"
#include "tcSrtmModel.h"
#include <glMatrix.h>
#include <glVector.h>

#include <GL/gl.h>

#include <cmath>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcGlobe::tcGlobe(double meanRadius)
: m_meanRadius(meanRadius)
, m_elevation(new tcSrtmModel[2])
, m_imagery()
, m_elevationMode()
, m_elevationInterpolation(1.0f)
, m_colourCoding(NoColourCoding)
, m_colourMapping()
{
  for (int i = 0; i < 2; ++i)
  {
    m_elevationMode[i] = CorrectedElevation;
  }
  for (int i = 0; i < 3; ++i)
  {
    m_colourMapping[i][0] = 0;
    m_colourMapping[i][1] = 2-i;
  }
  for (int i = 3; i < 6; ++i)
  {
    m_colourMapping[i][0] = -1;
    m_colourMapping[i][1] = -1;
  }
  QList<tcShadowClassifyingData*> datasets;
  // this one is a bit rubbish, the sun is too high
  //datasets << new tcLandsatData("/home/james/cs/pro/data/LE71950281999206EDC01/", m_elevation);

  // high azimuth, low elevation
  datasets << new tcLandsatData("/home/james/cs/pro/data/LE71950282001307EDC00/", m_elevation);
  datasets << new tcLandsatData("/home/james/cs/pro/data/LE71950282002006EDC00/", m_elevation);
  //datasets << new tcLandsatData("/home/james/cs/pro/data/LE71950282002310EDC00/", m_elevation);

  datasets << new tcLandsatData("/home/james/cs/pro/data/LE71950282000081EDC00/", m_elevation);
  //datasets << new tcLandsatData("/home/james/cs/pro/data/etp195r28_5t19900910/",  m_elevation);

// Relative to 0N, 0E
#define NEW_DATASET(AZ, EL, TILT) \
  datasets << new tcCustomSunDirection(m_elevation+1, tcGeo((180.0-AZ)*M_PI/180, (EL)*M_PI/180), \
                                                      tcGeo(7.0*M_PI/180, (TILT)*M_PI/180), true)
// Relative to our region of interest
#define NEW_DATASET_REL(AZ, EL) \
  datasets << new tcCustomSunDirection(m_elevation+1, tcGeo((180.0-AZ)*M_PI/180, (EL)*M_PI/180), \
                                                      tcGeo(7.0*M_PI/180,   46.0*M_PI/180))

  // Mustn't be co-planar
  for (int i = -10; i <= 10; i += 20)
  {
    for (int j = 30; j <= 150; j += 10)
    {
      //NEW_DATASET((double)90, (double)j, (double)i);
    }
  }

  foreach (tcShadowClassifyingData* dataset, datasets)
  {
    addImagery(dataset);
  }
  addImagery(new tcProcessingData(datasets, m_elevation));
}

/// Destructor.
tcGlobe::~tcGlobe()
{
  foreach (tcGeoImageData* data, m_imagery)
  {
    delete data;
  }
  delete [] m_elevation;
}

/*
 * Data sets
 */

/// Add some imagery data.
void tcGlobe::addImagery(tcGeoImageData* data)
{
  m_imagery.push_back(data);
}

/// Get the imagery data.
const QList<tcGeoImageData*>& tcGlobe::imagery() const
{
  return m_imagery;
}

/// Get the elevation model.
tcSrtmModel* tcGlobe::dem() const
{
  return m_elevation;
}

/*
 * Rendering
 */

/// Draw a line of latitude.
void tcGlobe::drawLineOfLatitude(double latitude) const
{
  double z  = sin(latitude) * m_meanRadius;
  double xy = cos(latitude) * m_meanRadius;
  glBegin(GL_LINE_LOOP);
  {
    for (int lon = 0; lon < 360; ++lon)
    {
      glVertex3d(xy*sin(M_PI/180*lon), xy*cos(M_PI/180*lon), z);
    }
  }
  glEnd();
}

/// Render a cell.
void tcGlobe::renderCell(tcObserver* const observer, const tcGeo& swCorner, const tcGeo& neCorner, int samples, bool normals,
                         bool northEdge, bool eastEdge, bool southEdge, bool westEdge) const
{
  // Sample at a sensible level
  tcSrtmModel::RenderState state;
  m_elevation->sampleAlign(swCorner, neCorner, &state, samples);

  if (state.moreAvailableLon || state.moreAvailableLat)
  {
    // Find the square distances to each corner
    GLmat4d modelview;
    glGetv(GL_MODELVIEW_MATRIX, &modelview);
    tcGeo geoCorners[4] = {
      tcGeo(swCorner.lon(), swCorner.lat()),
      tcGeo(swCorner.lon(), neCorner.lat()),
      tcGeo(neCorner.lon(), neCorner.lat()),
      tcGeo(neCorner.lon(), swCorner.lat())
    };
    GLvec3d cartCorners[4];
    float toCorners[4];
    double altitudeMean = m_meanRadius + altitudeAt((geoCorners[0] + geoCorners[1] + geoCorners[2] + geoCorners[3])/4);
    for (int i = 0; i < 4; ++i)
    {
      cartCorners[i] = (GLvec3d)geoCorners[i] * altitudeMean;
      toCorners[i] = (modelview*(cartCorners[i], 1.0)).slice<0,3>().sqr();
      // Cull faces which are roughly backfacing 
// actually lets not
#if 0
      if ((modelview*(cartCorners[i], 0.0))[2] <= 0.0)
      {
        return;
      }
#endif
    }

    // Decide whether to subdivide
    float diagonal = ( (cartCorners[0]-cartCorners[2]).sqr()
                     + (cartCorners[3]-cartCorners[1]).sqr())/2*4;
    // If it is disproportionately tall, only subdivide horizontally
    bool tall = (cartCorners[1] - cartCorners[0]).sqr() > (cartCorners[3] - cartCorners[0]).sqr()*4.0
             || (cartCorners[2] - cartCorners[3]).sqr() > (cartCorners[2] - cartCorners[1]).sqr()*4.0;
    // If it is disproportionately wide, only subdivide vertically
    bool wide = (cartCorners[3] - cartCorners[0]).sqr() > (cartCorners[1] - cartCorners[0]).sqr()*4.0
             || (cartCorners[2] - cartCorners[1]).sqr() > (cartCorners[2] - cartCorners[3]).sqr()*4.0;
    bool subdivide = true;
    for (int i = 0; i < 4; ++i)
    {
      if (toCorners[i] > diagonal)
      {
        subdivide = false;
        break;
      }
    }

    if (subdivide)
    {
      if (tall && !wide)
      {
        // bottom
        renderCell(observer,  geoCorners[0],                        (geoCorners[3] + geoCorners[2]) * 0.5, samples, normals,
                   false, eastEdge, southEdge, westEdge);
        // top
        renderCell(observer, (geoCorners[0] + geoCorners[1]) * 0.5,  geoCorners[2], samples, normals,
                   northEdge, eastEdge, false, westEdge);
      }
      else if (wide && !tall)
      {
        // left
        renderCell(observer,  geoCorners[0],                        (geoCorners[1] + geoCorners[2]) * 0.5, samples, normals,
                   northEdge, false, southEdge, westEdge);
        // right
        renderCell(observer, (geoCorners[0] + geoCorners[3]) * 0.5,  geoCorners[2], samples, normals,
                   northEdge, eastEdge, southEdge, false);
      }
      else
      {
        // bottom left
        renderCell(observer,  geoCorners[0],                        (geoCorners[0] + geoCorners[2]) * 0.5, samples, normals,
                   false, false, southEdge, westEdge);
        // bottom right
        renderCell(observer, (geoCorners[0] + geoCorners[3]) * 0.5, (geoCorners[3] + geoCorners[2]) * 0.5, samples, normals,
                   false, eastEdge, southEdge, false);
        // top left
        renderCell(observer, (geoCorners[0] + geoCorners[1]) * 0.5, (geoCorners[1] + geoCorners[2]) * 0.5, samples, normals,
                   northEdge, false, false, westEdge);
        // top right
        renderCell(observer, (geoCorners[0] + geoCorners[2]) * 0.5,  geoCorners[2], samples, normals,
                   northEdge, eastEdge, false, false);
      }
      return;
    }
  }

#if 0
  if (subdivide)
  {
    glColor3f(0.0f, 0.0f, 1.0f);
  }
  else
  {
    glColor3f(1.0f, 0.5f, 0.0f);
  }

  glBegin(GL_LINE_LOOP);
  for (int i = 0; i < 4; ++i)
  {
    glVertex3(cartCorners[i]);
  }
  glEnd();
#endif

  #define EDGE_KERNEL_1 \
          glColor4f(0.5f, 0.3f, 0.2f, 1.0f); \
          glVertex3(dir * (m_meanRadius+alt)); \
          glColor4f(0.5f, 0.5f, 0.5f, 1.0f); \
          glVertex3(dir * m_meanRadius);
  #define EDGE_KERNEL_2 \
          glColor4f(0.5f, 0.5f, 1.0f, 1.0f); \
          glVertex3(dir * m_meanRadius); \
          glColor4f(0.5f, 0.5f, 1.0f, 1.0f); \
          glVertex3(dir * (m_meanRadius-(accurate ? 100.0 : 1000.0)));
  #define EDGE_KERNEL_3 \
          glColor4f(0.5f, 0.5f, 0.5f, 1.0f); \
          glVertex3(dir * (m_meanRadius-(accurate ? 100.0 : 1000.0))); \
          glColor4f(0.5f, 0.5f, 0.5f, 1.0f); \
          glVertex3(dir * (m_meanRadius-5000.0));
  #define EDGE_KERNEL_4 \
          glColor4f(0.5f, 0.5f, 0.5f, 1.0f); \
          glVertex3(dir * (m_meanRadius-5000.0)); \
          glColor4f(1.0f, 0.0f, 0.0f, 0.5f); \
          glVertex3(dir * (m_meanRadius-8000.0));
  #define EDGE_KERNEL(STAGE, EDGE, SAMPLES, LON, LAT) \
      if (EDGE##Edge) \
      { \
        glBegin(GL_TRIANGLE_STRIP); \
        for (int i = 0; i < SAMPLES; ++i) \
        { \
          tcGeo coord; \
          bool accurate = true; \
          double alt = altitudeAt(state, (LON), (LAT), &coord, &accurate); \
          GLvec3d dir = coord; \
          EDGE_KERNEL_##STAGE \
        } \
        glEnd(); \
      }
  #define EDGE(STAGE) \
      EDGE_KERNEL(STAGE, north, state.samplesLon, i,                  state.samplesLat-1); \
      EDGE_KERNEL(STAGE, east,  state.samplesLat, state.samplesLon-1, i); \
      EDGE_KERNEL(STAGE, south, state.samplesLon, i,                  0); \
      EDGE_KERNEL(STAGE, west,  state.samplesLat, 0,                  i);

  if (normals)
  {
    // Draw normals
    float normColours[3][2][3] = {
      { { 1.0f, 1.0f, 1.0f }, { 1.0f, 0.0f, 0.0f } },
      { { 1.0f, 1.0f, 1.0f }, { 0.0f, 1.0f, 0.0f } },
      { { 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f, 1.0f } }
    };
    glBegin(GL_LINES);
    for (int i = 0; i < state.samplesLon; ++i)
    {
      for (int j = 0; j < state.samplesLat; ++j)
      {
        tcGeo coord;
        bool accurate = true;
        double alt = altitudeAt(state, i, j, &coord, &accurate);
        for (int n = 0; n < 3; ++n)
        {
          GLvec3f norm = normalAt(n, coord);
          if (!norm.zero())
          {
            norm = coord * norm;
            GLvec3d dir = coord;
            double rad = m_meanRadius + alt;
            glColor3fv(normColours[n][0]);
            glVertex3(dir * rad);
            glColor3fv(normColours[n][1]);
            glVertex3(dir * rad + norm*50.0f);
          }
        }
      }
    }
    glEnd();
  }
  else
  {
#if 0
    // Render the solid rock walls
    glDisable(GL_CULL_FACE);
    EDGE(1)
    EDGE(2)
    EDGE(3)
    EDGE(4)
    glEnable(GL_CULL_FACE);
#endif

    // Render this cell
    /// @todo cache one edge of strip to save time on other
    for (int i = 0; i < state.samplesLon-1; ++i)
    {
      glBegin(GL_TRIANGLE_STRIP);
      {
        for (int j = 0; j < state.samplesLat; ++j)
        {
          for (int k = 0; k < 2; ++k)
          {
            tcGeo coord;
            bool accurate = true;
            double alt = altitudeAt(state, i+k, j, &coord, &accurate);

            // Get colour
            foreach (tcGeoImageData* imagery, m_imagery)
            {
              imagery->texCoord(coord);
            }

            if (!accurate)
            {
              glColor4f(0.5f, 0.5f, 1.0f, 1.0f);
            }
            else
            {
              glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
              //glColor4f(1.0f, 1.0f-(float)alt/3278.0f, 0.0f, 1.0f);
            }
            // Colour code if applicable
            if (m_colourCoding == ElevationSampleAlignment)
            {
              if (state.moreAvailableLon && state.moreAvailableLat)
              {
                glColor3f(1.0f, 0.0f, 1.0f);
              }
              else if (state.moreAvailableLon)
              {
                glColor3f(1.0f, 0.0f, 0.0f);
              }
              else if (state.moreAvailableLat)
              {
                glColor3f(0.0f, 0.0f, 1.0f);
              }
              else
              {
                glColor3f(0.0f, 1.0f, 0.0f);
              }
            }
            GLvec3d dir = coord;
            double rad = m_meanRadius + alt;
            glVertex3(dir * rad);
          }
        }
      }
      glEnd();
    }
  }
}

/// Render from the POV of an observer.
void tcGlobe::render(tcObserver* const observer, bool adaptive, const tcGeo* extent)
{
  /// @todo use a really simple fragment shader to cull backfacing lines
#if 1
  // Lines of latitude 
  glColor3f(0.0f, 1.0f, 0.0f);
  for (int lat = -75; lat <= 75; lat += 15)
  {
    if (lat != 0)
    {
      drawLineOfLatitude(M_PI/180*lat);
    }
  }

  double tropic = (23.0 + 26.0/60 + 22.0/3600) * M_PI/180;
  // Equator
  glColor3f(1.0f, 0.0f, 0.0f);
  drawLineOfLatitude(0.0);
  // Tropics (Capricorn and Cancer)
  glColor3f(1.0f, 0.0f, 1.0f);
  drawLineOfLatitude(-tropic);
  glColor3f(1.0f, 1.0f, 0.0f);
  drawLineOfLatitude(+tropic);
  // Arctic and Antarctic Circles
  glColor3f(1.0f, 1.0f, 1.0f);
  drawLineOfLatitude(+M_PI/2 - tropic);
  drawLineOfLatitude(-M_PI/2 + tropic);

  // Lines of longitude
  for (int lon = 0; lon < 360; lon += 15)
  {
    double x = sin(M_PI/180*lon) * m_meanRadius;
    double y = -cos(M_PI/180*lon) * m_meanRadius;
    int minLat = 15;
    int maxLat = 165;
    if (lon % 180 == 0)
    {
      glLineWidth(2.0f);
      glColor3f(1.0f, lon/180, 0.0f);
    }
    if (lon % 90 == 0)
    {
      minLat = 0;
      maxLat = 180;
    }
    glBegin(GL_LINE_STRIP);
    {
      for (int lat = minLat; lat <= maxLat; ++lat)
      {
        double z = cos(M_PI/180*lat) * m_meanRadius;
        double xy = sin(M_PI/180*lat);
        glVertex3d(xy*x, xy*y, z);
      }
    }
    glEnd();
    if (lon % 180 == 0)
    {
      glLineWidth(1.0f);
      glColor3f(0.0f, 1.0f, 0.0f);
    }
  }
#endif

  // Draw data diagramatically
  foreach (tcGeoImageData* imagery, m_imagery)
  {
    imagery->renderSchematic(m_meanRadius, observer);
  }

  if (0 == extent)
  {
    int colourMapping[6];
    for (int band = 0; band < m_imagery.size(); ++band)
    {
      for (int i = 0; i < 6; ++i)
      {
        colourMapping[i] = (m_colourMapping[i][0] == band ? m_colourMapping[i][1] : -1);
      }
      tcGeoImageData* imagery = m_imagery[band];
      imagery->setupThumbnailRendering(6, colourMapping);
    }
    // Go through cells
    const int dlon = 30;
    const int dlat = 30;
    for (int lon = -180; lon < 180; lon += dlon)
    {
      for (int lat = -90; lat < 90; lat += dlat)
      {
        tcGeo sw(M_PI/180 * (lon), M_PI/180 * (lat));
        tcGeo ne(M_PI/180 * (lon+dlon), M_PI/180 * (lat+dlat));
        renderCell(observer, sw, ne, adaptive ? 5 : 0, false);
      }
    }
    foreach (tcGeoImageData* imagery, m_imagery)
    {
      imagery->setupNormalRendering();
    }
    for (int lon = -180; lon < 180; lon += dlon)
    {
      for (int lat = -90; lat < 90; lat += dlat)
      {
        tcGeo sw(M_PI/180 * (lon), M_PI/180 * (lat));
        tcGeo ne(M_PI/180 * (lon+dlon), M_PI/180 * (lat+dlat));
        renderCell(observer, sw, ne, adaptive ? 5 : 0, true);
      }
    }
  }
  else
  {
    tcGeo sw = extent[0];
    tcGeo ne = extent[1];
    if (sw.lon() > ne.lon())
    {
      sw.setLon(ne.lon());
      ne.setLon(extent[0].lon());
    }
    if (sw.lat() > ne.lat())
    {
      sw.setLat(ne.lat());
      ne.setLat(extent[0].lat());
    }
    int colourMapping[6];
    for (int band = 0; band < m_imagery.size(); ++band)
    {
      for (int i = 0; i < 6; ++i)
      {
        colourMapping[i] = (m_colourMapping[i][0] == band ? m_colourMapping[i][1] : -1);
      }
      tcGeoImageData* imagery = m_imagery[band];
      imagery->setupDetailedRendering(6, colourMapping, sw, ne);
    }
    /// @todo If it is really big, split it
    renderCell(observer, sw, ne, adaptive ? 16 : 0, false, true, true, true, true);
    foreach (tcGeoImageData* imagery, m_imagery)
    {
      imagery->setupNormalRendering();
    }
    renderCell(observer, sw, ne, adaptive ? 16 : 0, true, true, true, true, true);
  }
  foreach (tcGeoImageData* imagery, m_imagery)
  {
    imagery->finishRendering();
  }
}

/// Set the elevation mode to render in.
void tcGlobe::setElevationMode(int dem, ElevationMode mode)
{
  Q_ASSERT(dem >= 0 && dem < 2);
  m_elevationMode[dem] = mode;
}

/// Set the level of interpolation.
void tcGlobe::setElevationInterpolation(float interpolation)
{
  m_elevationInterpolation = interpolation;
}

/// Set the elevation data set name.
void tcGlobe::setElevationDataSet(int dem, const QString& name)
{
  Q_ASSERT(dem >= 0 && dem < 2);
  m_elevation[dem].setDataSet(name);
}

/// Set colour coding method.
void tcGlobe::setColourCoding(ColourCoding colourCoding)
{
  m_colourCoding = colourCoding;
}

/// Adjust the mapping between bands and colour channels.
void tcGlobe::setColourMapping(int outputChannel, int inputBand, int inputGroup)
{
  if (outputChannel < 6)
  {
    m_colourMapping[outputChannel][0] = inputGroup;
    m_colourMapping[outputChannel][1] = inputBand;
  }
}

/*
 * Accessors
 */

/// Get the mean radius.
double tcGlobe::meanRadius() const
{
  return m_meanRadius;
}

/// Get the altitude above sea level at a sample in a render state.
double tcGlobe::altitudeAt(const tcSrtmModel::RenderState& state, int x, int y, tcGeo* outCoord, bool* isAccurate) const
{
  bool accurate[2] = {
    true,
    true
  };
  double alt[2] = {
    0.0,
    0.0
  };
  bool needElev[2] = {
    m_elevationInterpolation < 1.0f,
    m_elevationInterpolation > 0.0f
  };
  for (int i = 0; i < 2; ++i)
  {
    switch (m_elevationMode[i])
    {
      case NoElevation:
        break;
      case RawElevation:
        {
          alt[i] = m_elevation[i].altitudeAt(state, x, y, outCoord, false, &accurate[i]);
          if (!accurate[i])
          {
            alt[i] = 0.0;
          }
        }
        break;
      case CorrectedElevation:
        {
          // get accurate set
          alt[i] = m_elevation[i].altitudeAt(state, x, y, outCoord, true, &accurate[i]);
        }
        break;
      default:
        Q_ASSERT(false);
        break;
    }
  }
  double result = alt[0]*(1.0-m_elevationInterpolation) + alt[1]*m_elevationInterpolation;
  if (0 != isAccurate)
  {
    *isAccurate = accurate[0] && accurate[1];
  }
  return result;
}

/// Get the altitude above sea level at a coordinate.
double tcGlobe::altitudeAt(const tcGeo& coord, bool* isAccurate) const
{
  bool accurate[2] = {
    true,
    true
  };
  double alt[2] = {
    0.0,
    0.0
  };
  bool needElev[2] = {
    m_elevationInterpolation < 1.0f,
    m_elevationInterpolation > 0.0f
  };
  for (int i = 0; i < 2; ++i)
  {
    switch (m_elevationMode[i])
    {
      case NoElevation:
        break;
      case RawElevation:
        {
          alt[i] = m_elevation[i].altitudeAt(coord, false, &accurate[i]);
          if (!accurate[i])
          {
            alt[i] = 0.0;
          }
        }
        break;
      case CorrectedElevation:
        {
          // get accurate set
          alt[i] = m_elevation[i].altitudeAt(coord, true, &accurate[i]);
        }
        break;
      default:
        Q_ASSERT(false);
        break;
    }
  }
  double result = alt[0]*(1.0-m_elevationInterpolation) + alt[1]*m_elevationInterpolation;
  if (0 != isAccurate)
  {
    *isAccurate = accurate[0] && accurate[1];
  }
  return result;
}

/// Get the radius at a coordinate.
double tcGlobe::radiusAt(const tcGeo& coord) const
{
  return m_meanRadius + altitudeAt(coord);
}

/// Get the texture coordinate of the effective texture at a geographical coordinate.
maths::Vector<2,double> tcGlobe::textureCoordOfGeo(const tcGeo& coord) const
{
  /// @todo Reimplement tcGlobe::textureCoordOfGeo with multiple imagery
  Q_ASSERT(0 && "Reimplement tcGlobe::TextureCoordOfGeo with multiple imagery");
  return m_imagery[0]->geoToEffectiveTex() * coord;
}

/// Get the current normal at a coordinate.
maths::Vector<3,float> tcGlobe::normalAt(int norm, const tcGeo& coord) const
{
  Q_ASSERT(norm >= 0 && norm < 3);
  if (m_colourMapping[3+norm][0] >= 0)
  {
    return m_imagery[m_colourMapping[3+norm][0]]->normalAt(norm, coord);
  }
  else
  {
    return maths::Vector<3,float>(0.0f);
  }
}
