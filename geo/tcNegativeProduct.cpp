/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcNegativeProduct.cpp
 * @brief Highlights the parts of an image which are dark in all channels.
 */

#include "tcNegativeProduct.h"
#include "tcChannelConfigWidget.h"
#include "tcChannelManager.h"

#include <QHBoxLayout>
#include <QCheckBox>
#include <QSlider>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QSpinBox>
#include <QSignalMapper>
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>

#include <cmath>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcNegativeProduct::tcNegativeProduct(const QList<tcChannel*>& inputChannels)
: tcChannel(tr("Negative Product"),
            tr("Highlights pixels which are dark in all channels"))
, m_inputChannels(inputChannels)
, m_thresholdEnabled(true)
, m_threshold(0.01f)
, m_powers(new float[inputChannels.size()])
, m_configWidget(0)
, m_powerSliders()
, m_powerLabels()
{
  Q_ASSERT(m_inputChannels.size() > 0);
  for (int c = 0; c < m_inputChannels.size(); ++c)
  {
    m_inputChannels[c]->addDerivitive(this);
    m_powers[c] = 1.0f;
  }
}

/// Destructor.
tcNegativeProduct::~tcNegativeProduct()
{
  foreach (tcChannel* channel, m_inputChannels)
  {
    channel->removeDerivitive(this);
  }
  delete m_configWidget;
  delete [] m_powers;
}

/*
 * Main image interface
 */

tcChannelConfigWidget* tcNegativeProduct::configWidget()
{
  if (0 == m_configWidget)
  {
    m_configWidget = new tcChannelConfigWidget();
    m_configWidget->setWindowTitle(tr("%1 Configuration").arg(name()));

    QVBoxLayout* layout = new QVBoxLayout(m_configWidget);

    // Create a checkbox to enable/disable threshold.
    QCheckBox* chkThreshold = new QCheckBox(tr("Threshold to classify shadow"), m_configWidget);
      layout->addWidget(chkThreshold);
      chkThreshold->setCheckState(m_thresholdEnabled ? Qt::Checked : Qt::Unchecked);
      connect(chkThreshold, SIGNAL(clicked(bool)), this, SLOT(setThresholdEnabled(bool)));

    // Create a slider to set threshold.
    m_sliderThreshold = new QSlider(Qt::Horizontal, m_configWidget);
      layout->addWidget(m_sliderThreshold);
      m_sliderThreshold->setRange(-4000, 0);
      m_sliderThreshold->setValue(m_sliderThreshold->minimum());
      m_sliderThreshold->setTickInterval(100);
      m_sliderThreshold->setTickPosition(QSlider::TicksBelow);
      m_sliderThreshold->setTracking(false);
      connect(m_sliderThreshold, SIGNAL(valueChanged(int)), this, SLOT(setThreshold(int)));
      connect(m_sliderThreshold, SIGNAL(sliderMoved(int)), this, SLOT(moveThreshold(int)));
      connect(chkThreshold, SIGNAL(clicked(bool)), m_sliderThreshold, SLOT(setEnabled(bool)));

    // Label to tell us what the threshold is.
    QLabel* labelThreshold = new QLabel(m_configWidget);
      layout->addWidget(labelThreshold);
      connect(this, SIGNAL(thresholdChanged(const QString&)), labelThreshold, SLOT(setText(const QString&)));
      connect(chkThreshold, SIGNAL(clicked(bool)), labelThreshold, SLOT(setEnabled(bool)));

    // Have sliders to adjust the powers of negatives of channels.
    QSignalMapper* powerMapper = new QSignalMapper(m_configWidget);
      connect(powerMapper, SIGNAL(mapped(int)), this, SLOT(changePower(int)));
    QSignalMapper* powerMapperMove = new QSignalMapper(m_configWidget);
      connect(powerMapperMove, SIGNAL(mapped(int)), this, SLOT(movePower(int)));

    QWidget* widgetPowers = new QWidget(m_configWidget);
      layout->addWidget(widgetPowers);
      QGridLayout* layPowers = new QGridLayout(widgetPowers);
      for (int i = 0; i < m_inputChannels.size(); ++i)
      {
        QLabel* labelPower = new QLabel(m_inputChannels[i]->name(), m_configWidget);
          layPowers->addWidget(labelPower, i, 0);
        QSlider* sliderPower = new QSlider(Qt::Horizontal, m_configWidget);
          layPowers->addWidget(sliderPower, i, 1);
          labelPower->setBuddy(sliderPower);
          sliderPower->setRange(0, 200);
          sliderPower->setValue(m_powers[i]*10);
          sliderPower->setTickInterval(10);
          sliderPower->setTickPosition(QSlider::TicksBelow);
          sliderPower->setTracking(false);
          powerMapper->setMapping(sliderPower, i);
          powerMapperMove->setMapping(sliderPower, i);
          connect(sliderPower, SIGNAL(valueChanged(int)), powerMapper, SLOT(map()));
          connect(sliderPower, SIGNAL(sliderMoved(int)), powerMapperMove, SLOT(map()));
          m_powerSliders += sliderPower;
        QLabel* labelPowerValue = new QLabel(QString("%1").arg(m_powers[i]), m_configWidget);
          layPowers->addWidget(labelPowerValue, i, 2);
          m_powerLabels += labelPowerValue;
      }

    QWidget* widgetOptimize = new QWidget(m_configWidget);
      layout->addWidget(widgetOptimize);
      QHBoxLayout* layoutOptimize = new QHBoxLayout(widgetOptimize);
    m_comboClassifier = new QComboBox(widgetOptimize);
      layoutOptimize->addWidget(m_comboClassifier);
      for (int i = 0; i < channelManager()->numChannels(); ++i)
      {
        tcChannel* channel = channelManager()->channel(i);
        m_comboClassifier->addItem(channel->name());
      }
    QPushButton* btnCalculateError = new QPushButton(tr("Calculate error"), widgetOptimize);
      layoutOptimize->addWidget(btnCalculateError);
      connect(btnCalculateError, SIGNAL(clicked(bool)), this, SLOT(calculateError()));
    QPushButton* btnOptimizeThreshold = new QPushButton(tr("Optimize Threshold"), widgetOptimize);
      layoutOptimize->addWidget(btnOptimizeThreshold);
      connect(btnOptimizeThreshold, SIGNAL(clicked(bool)), this, SLOT(optimizeThreshold()));
    m_subsample = new QSpinBox(widgetOptimize);
      layoutOptimize->addWidget(m_subsample);
      m_subsample->setRange(1,20);
      m_subsample->setValue(1);
    QPushButton* btnSaveThresholdErrors = new QPushButton(tr("Save Threshold Errors"), widgetOptimize);
      layoutOptimize->addWidget(btnSaveThresholdErrors);
      connect(btnSaveThresholdErrors, SIGNAL(clicked(bool)), this, SLOT(saveThresholdErrors()));

    QWidget* widgetOptimizeParameters = new QWidget(m_configWidget);
      layout->addWidget(widgetOptimizeParameters);
      QHBoxLayout* layoutOptimizeParameters = new QHBoxLayout(widgetOptimizeParameters);
    m_sliderLearningRate = new QSlider(Qt::Horizontal, widgetOptimizeParameters);
      layoutOptimizeParameters->addWidget(m_sliderLearningRate);
      m_sliderLearningRate->setRange(0, 5);
      m_sliderLearningRate->setValue(5);
    QPushButton* btnOptimizeParams = new QPushButton(tr("Optimize Parameters"), widgetOptimizeParameters);
      layoutOptimizeParameters->addWidget(btnOptimizeParams);
      connect(btnOptimizeParams, SIGNAL(clicked(bool)), this, SLOT(optimizeParameters()));

    m_labelError = new QLabel(tr("Error: Unknown"), widgetOptimize);
      layout->addWidget(m_labelError);

    QWidget* widgetSaveLoad = new QWidget(m_configWidget);
      layout->addWidget(widgetSaveLoad);
      QHBoxLayout* layoutSaveLoad = new QHBoxLayout(widgetSaveLoad);
    QPushButton* btnSaveParams = new QPushButton(tr("Save Parameters"), widgetSaveLoad);
      layoutSaveLoad->addWidget(btnSaveParams);
      connect(btnSaveParams, SIGNAL(clicked(bool)), this, SLOT(saveParams()));
    QPushButton* btnLoadParams = new QPushButton(tr("Load Parameters"), widgetSaveLoad);
      layoutSaveLoad->addWidget(btnLoadParams);
      connect(btnLoadParams, SIGNAL(clicked(bool)), this, SLOT(loadParams()));

    moveThreshold(m_sliderThreshold->value());
    setThreshold(m_sliderThreshold->value());
  }
  return m_configWidget;
}

/*
 * Parameters
 */

/// Load parameters.
void tcNegativeProduct::loadParams(const QString& filename)
{
  configWidget();
  m_saveParameters = filename;
  
  QFile file(m_saveParameters);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    QMessageBox::warning(m_configWidget,
        tr("Open failed."),
        tr("Could not open '%1' for reading.").arg(m_saveParameters));
    return;
  }

  QTextStream in(&file);
  in >> m_threshold;
  m_sliderThreshold->setValue(log(m_threshold)*100);
  for (int i = 0; i < m_powerSliders.size(); ++i)
  {
    in >> m_powers[i];
    m_powerSliders[i]->setValue(m_powers[i]*10);
  }

  invalidate();
  m_configWidget->requestRedraw();
}

/*
 * Interface for derived class to implement
 */

void tcNegativeProduct::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  m_inputChannels.first()->roundPortion(x1, y1, x2, y2);
}

tcAbstractPixelData* tcNegativeProduct::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  tcPixelData<float>* data = 0;
  bool isFirst = true;
  int width = 0;
  int height = 0;
  //startProcessing(tr("Calculating"));
  for (int c = 0; c < m_inputChannels.size(); ++c)
  {
    tcChannel* channel = m_inputChannels[c];
    //progress(channel->name());
    Reference<tcPixelData<GLubyte> > channelData = dynamicCast<tcPixelData<GLubyte>*>(channel->portion(x1, y1, x2, y2));
    if (isFirst)
    {
      isFirst = false;
      width  = channelData->width();
      height = channelData->height();
      // Create a new pixel buffer at full intensity
      data = new tcPixelData<float>(width, height);
      for (int i = 0; i < width*height; ++i)
      {
        data->buffer()[i] = 1.0f;
      }
    }
    // Go through channels clearing saturated pixels
    float avg = 0.5f;
    avg *= avg;
    avg *= avg;
    avg *= avg;
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        int index = j*width + i;
        // Multiply by 1-value
        float neg = 1.0f - (float)channelData->sampleUByte((float)i/(width-1), (float)j/(height-1)) / 255.0f;
        data->buffer()[index] *= powf(neg, m_powers[c]); //avg;
        // Have a slider for each channel to adjust power
      }
    }
  }

  // Threshold
  if (0 != data && m_thresholdEnabled)
  {
    //startProcessing(tr("Thresholding"));
    for (int i = 0; i < width*height; ++i)
    {
      if (data->buffer()[i] > m_threshold)
      {
        data->buffer()[i] = 0.0f;
      }
      else
      {
        data->buffer()[i] = 1.0f;
      }
    }
  }
  //endProcessing();

  return data;
}

/*
 * Private slots
 */

/// Set whether the theshold is enabled.
void tcNegativeProduct::setThresholdEnabled(bool thresholdEnabled)
{
  m_thresholdEnabled = thresholdEnabled;
  invalidate();
  m_configWidget->requestRedraw();
}

/// Move the theshold value slider.
void tcNegativeProduct::moveThreshold(int threshold)
{
  m_threshold = exp((float)threshold/100);
  emit thresholdChanged(tr("Threshold: %1").arg(m_threshold));
}

/// Set the theshold value.
void tcNegativeProduct::setThreshold(int threshold)
{
  moveThreshold(threshold);
  m_threshold = exp((float)threshold/100);
  invalidate();
  m_configWidget->requestRedraw();
}

/// Indicates that the power slider of a channel has moved.
void tcNegativeProduct::movePower(int channel)
{
  Q_ASSERT(channel >= 0 && channel < m_inputChannels.size());
  Q_ASSERT(0 != m_configWidget);
  float power = 0.1f * m_powerSliders[channel]->sliderPosition();
  m_powerLabels[channel]->setText(QString("%1").arg(power));
}

/// Indicates that the power of a channel has changed.
void tcNegativeProduct::changePower(int channel)
{
  movePower(channel);
  float power = 0.1f * m_powerSliders[channel]->value();
  m_powers[channel] = power;
  invalidate();
  m_configWidget->requestRedraw();
}

/// Optimize the threshold for the parameters using a classification channel.
void tcNegativeProduct::optimizeThreshold()
{
  tcChannel* classifier = channelManager()->channel(m_comboClassifier->currentIndex());
  if (0 != classifier)
  {
    // Get the unthresholded portion
    // Find the optimal threshold and calculate the error
    // Create bins for each threshold slider value with the change in error as the slider crosses
    Reference<tcAbstractPixelData> abstractData = portion();
    Reference<tcAbstractPixelData> classification = classifier->portion();
    if (0 != abstractData && 0 != classification)
    {
      Reference<tcPixelData<float> > data = dynamicCast<tcPixelData<float>*>(abstractData);
      Q_ASSERT(0 != data);

      int min = m_sliderThreshold->minimum();
      int max = m_sliderThreshold->maximum();
      int numBins = max-min;
      int* bins = new int[numBins];
      for (int i = 0; i < numBins; ++i)
      {
        bins[i] = 0;
      }

      // Find change in error across threshold bins
      int width = data->width();
      int height = data->height();
      int pixels = width*height;
      int baseError = 0;
      int subsample = m_subsample->value();
      for (int j = 0; j < height; j += subsample)
      {
        for (int i = 0; i < width; i += subsample)
        {
          int index = j*width + i;
          float val = data->buffer()[index];
          bool classified = classification->sampleFloat((float)i/(width-1), (float)j/(height-1)) > 0.5f;
          bool tooSmall = true;
          if (val > 0.0f)
          {
            int bin = 0.5f + logf(val)*100.0f;
            bin -= min;
            if (bin >= 0)
            {
              tooSmall = false;
              if (bin < numBins)
              {
                if (classified)
                {
                  // As threshold reaches this value, error will decrease
                  --bins[bin];
                }
                else 
                {
                  // As threshold reaches this value, error will increase
                  ++bins[bin];
                }
              }
            }
          }
          if ((tooSmall && !classified) ||
              (!tooSmall && classified))
          {
            ++baseError;
          }
        }
      }

      // Save cumulative values in bins
      if (!m_saveThresholdError.isEmpty())
      {
        QFile file(m_saveThresholdError);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
          QMessageBox::warning(m_configWidget,
              tr("Open failed."),
              tr("Could not open '%1' for writing.").arg(m_saveThresholdError));
          return;
        }

        QTextStream out(&file);
        int curError = baseError;
        out << 0.0f << " " << curError << "\n";
        for (int i = 0; i < numBins; ++i)
        {
          curError += bins[i];
          float curThreshold = expf((float)(min+i)/100);
          out << curThreshold << " " << curError << "\n";
        }
      }


      // Find the minimum bin and use it!
      int minBin = 0;
      int minError = baseError;
      int curError = minError;
      for (int i = 0; i < numBins; ++i)
      {
        curError += bins[i];
        if (curError < minError)
        {
          minBin = i;
          minError = curError;
        }
      }
      delete [] bins;

      m_optimalThresholdError = minError;
      calculateError();
      m_sliderThreshold->setValue(min + minBin);
      setThreshold(min + minBin);
    }
  }
}

/// Save threshold errors.
void tcNegativeProduct::saveThresholdErrors()
{
  m_saveThresholdError = QFileDialog::getSaveFileName(m_configWidget,
                          tr("Export channels"),
                          QString(),
                          tr("ASCII Data Files (*.adf)"));
  if (m_saveThresholdError.isEmpty())
  {
    return;
  }

  bool previousThresholding = m_thresholdEnabled;
  if (previousThresholding)
  {
    setThresholdEnabled(false);
  }

  optimizeThreshold();
  m_saveThresholdError = QString();

  if (previousThresholding = m_thresholdEnabled)
  {
    setThresholdEnabled(previousThresholding);
  }
}

/// Optimize the parameters using a classification channel.
void tcNegativeProduct::optimizeParameters()
{
  tcChannel* classifier = channelManager()->channel(m_comboClassifier->currentIndex());
  if (0 != classifier)
  {
    startProcessing(tr("Optimising parameters"));

    bool previousThresholding = m_thresholdEnabled;
    if (previousThresholding)
    {
      setThresholdEnabled(false);
    }

    int learningRate = 1 << m_sliderLearningRate->value();
    int perLRate = 2;
    while (learningRate > 0)
    {
      for (int i = 0; i < perLRate; ++i)
      {
        bool improved = false;
        for (int c = 0; c < m_inputChannels.size(); ++c)
        {
          // Vary the sliders a little bit to see which direction is best
          optimizeThreshold();
          progress(tr("learning rate %1, step: %2, channel: %3, %4").arg(learningRate).arg(i).arg(m_inputChannels[c]->name()).arg(m_labelError->text()));
          int baseError = m_optimalThresholdError;
          int power = m_powerSliders[c]->value();

          m_powerSliders[c]->setValue(power-learningRate);
          changePower(c);
          optimizeThreshold();
          int reduceError = m_optimalThresholdError;

          m_powerSliders[c]->setValue(power+learningRate);
          changePower(c);
          optimizeThreshold();
          int increaseError = m_optimalThresholdError;

          if (baseError < increaseError && baseError < reduceError)
          {
            m_powerSliders[c]->setValue(power);
            changePower(c);
          }
          else if (reduceError < increaseError && reduceError < baseError)
          {
            m_powerSliders[c]->setValue(power-learningRate);
            changePower(c);
            improved = true;
          }
          else
          {
            improved = true;
          }
          m_configWidget->repaint();
        }
        if (!improved)
        {
          break;
        }
      }
      learningRate /= 2;
      perLRate *= 2;
    }
    optimizeThreshold();
    if (previousThresholding = m_thresholdEnabled)
    {
      setThresholdEnabled(previousThresholding);
    }
    endProcessing();
  }
}

/// Calculate errors.
void tcNegativeProduct::calculateError()
{
  m_labelError->setText(tr("Error: Unknown"));
  tcChannel* classifier = channelManager()->channel(m_comboClassifier->currentIndex());
  if (0 != classifier)
  {
    Reference<tcAbstractPixelData> abstractData = portion();
    Reference<tcAbstractPixelData> classification = classifier->portion();
    if (0 != abstractData && 0 != classification)
    {
      Reference<tcPixelData<float> > data = dynamicCast<tcPixelData<float>*>(abstractData);
      Q_ASSERT(0 != data);

      // indexed first by reference shadow map litness, then by negative product litness
      int counts[2][2] = { {0, 0}, {0, 0} };

      int width = data->width();
      int height = data->height();
      for (int j = 0; j < height; ++j)
      {
        for (int i = 0; i < width; ++i)
        {
          int index = j*width + i;
          int val = (data->buffer()[index] > 0.5f ? 1 : 0);
          int classified = (classification->sampleFloat((float)i/(width-1), (float)j/(height-1)) > 0.5f ? 1 : 0);
          ++counts[classified][val];
        }
      }

      int total = counts[0][0]+counts[0][1]+counts[1][0]+counts[1][1];
      int correct = counts[0][0]+counts[1][1];
      int incorrect = counts[0][1]+counts[1][0];
      m_labelError->setText(tr("Error: %1% (%2/%3 pixels) [%4,%5,%6,%7]")
        .arg(100.0f*incorrect/total)
        .arg(incorrect)
        .arg(total)
        .arg(counts[0][0])
        .arg(counts[0][1])
        .arg(counts[1][0])
        .arg(counts[1][1]));
    }
  }

}

/// Save parameters to a file.
void tcNegativeProduct::saveParams()
{
  QString saveParameters = QFileDialog::getSaveFileName(m_configWidget,
                                                  tr("Save Parameters"),
                                                  m_saveParameters,
                                                  tr("Negative Product Parameter Files (*.npp)"));
  if (saveParameters.isEmpty())
  {
    return;
  }
  m_saveParameters = saveParameters;

  QFile file(m_saveParameters);
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    QMessageBox::warning(m_configWidget,
        tr("Open failed."),
        tr("Could not open '%1' for writing.").arg(m_saveParameters));
    return;
  }

  QTextStream out(&file);
  out << m_threshold;
  for (int i = 0; i < m_powerSliders.size(); ++i)
  {
    out << " " << m_powers[i];
  }
}

#include <QtDebug>
/// Load parameters from a file.
void tcNegativeProduct::loadParams()
{
  QString saveParameters = QFileDialog::getOpenFileName(m_configWidget,
                                                  tr("Load Parameters"),
                                                  m_saveParameters,
                                                  tr("Negative Product Parameter Files (*.npp)"));
  if (saveParameters.isEmpty())
  {
    return;
  }
  loadParams(saveParameters);
}

/*
 * Private functions
 */

/// Calculate an error using some channel as a classifier.
float tcNegativeProduct::calculateError(tcChannel* classifier)
{
}
