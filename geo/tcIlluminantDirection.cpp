/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcIlluminantDirection.cpp
 * @brief Illuminant direction based calculation.
 */

#include "tcIlluminantDirection.h"
#include "tcChannelConfigWidget.h"
#include "tcChannel.h"

#include <QObject>
#include <QVBoxLayout>
#include <QPushButton>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcIlluminantDirection::tcIlluminantDirection(const QList<tcChannel*>& chromaticities,
                                             int outputs, const QString& name, const QString& description)
: tcChannelGroup(outputs, name, description)
, m_chromaticities(chromaticities)
, m_illuminantDirection(chromaticities.size(), 1.0f)
, m_illuminantDirectionSamples(0)
, m_configWidget(0)
{
  m_illuminantDirection.normalize();
}

/// Destructor.
tcIlluminantDirection::~tcIlluminantDirection()
{
  delete m_configWidget;
}

/*
 * Accessors
 */

/// Get the number of chromaticity channels.
int tcIlluminantDirection::numChromaticities() const
{
  return m_chromaticities.size();
}

/// Get the list of chromaticitiy channels.
const QList<tcChannel*>& tcIlluminantDirection::chromaticities() const
{
  return m_chromaticities;
}

/// Get the illuminant direction.
const maths::VarVector<float>& tcIlluminantDirection::illuminantDirection() const
{
  return m_illuminantDirection;
}

/*
 * Main image interface
 */

tcChannelConfigWidget* tcIlluminantDirection::configWidget()
{
  if (0 == m_configWidget)
  {
    m_configWidget = new tcChannelConfigWidget();
    m_configWidget->setWindowTitle(tr("%1 Configuration").arg(name()));

    QVBoxLayout* layout = new QVBoxLayout(m_configWidget);

    // Create a button which triggers an illuminant direction reset.
    QPushButton* btnReset = new QPushButton(tr("Reset illuminant direction"), m_configWidget);
      layout->addWidget(btnReset);
      connect(btnReset, SIGNAL(clicked(bool)), this, SLOT(resetIlluminantDirection()));

    // Create a button which triggers texture point request.
    QPushButton* btnNewShadowNonShadow = new QPushButton(tr("Choose Shadow/Non-Shadow transition"), m_configWidget);
      layout->addWidget(btnNewShadowNonShadow);
      connect(btnNewShadowNonShadow, SIGNAL(clicked(bool)), this, SLOT(startShadowNonShadow()));
  }
  return m_configWidget;
}

/*
 * Private slots
 */

/// Reset the illuminant direction vector.
void tcIlluminantDirection::resetIlluminantDirection()
{
  for (int i = 0; i < m_illuminantDirection.length(); ++i)
  {
    m_illuminantDirection[i] = 1.0f;
  }
  m_illuminantDirectionSamples = 0;
  invalidate();
  m_configWidget->requestRedraw();
}

/// Start choosing a shadow / non shadow pair.
void tcIlluminantDirection::startShadowNonShadow()
{
  Q_ASSERT(0 != m_configWidget);
  m_configWidget->requestTexturePoint(this, SLOT(selectShadowPoint(const maths::Vector<2,float>&)));
}

/// New shadow point.
void tcIlluminantDirection::selectShadowPoint(const maths::Vector<2,float>& point)
{
  Q_ASSERT(0 != m_configWidget);
  m_shadowPoint = point;
  m_configWidget->requestTexturePoint(this, SLOT(selectNonShadowPoint(const maths::Vector<2,float>&)));
}

/// New non-shadow point.
void tcIlluminantDirection::selectNonShadowPoint(const maths::Vector<2,float>& point)
{
  Q_ASSERT(0 != m_configWidget);
  // Find the direction between the log chromaticities of the two points
  for (int i = 0; i < m_chromaticities.size(); ++i)
  {
    tcChannel* channel = m_chromaticities[i];
    Reference<tcAbstractPixelData> pixelData = channel->portion();
    if (0 != pixelData)
    {
      float delta = pixelData->sampleFloat(point[0],         1.0f - point[1])
                  - pixelData->sampleFloat(m_shadowPoint[0], 1.0f - m_shadowPoint[1]);
      m_illuminantDirection[i] *= m_illuminantDirectionSamples;
      m_illuminantDirection[i] += delta;
      m_illuminantDirection[i] /= m_illuminantDirectionSamples+1;
    }
  }
  m_illuminantDirection.normalize();
  ++m_illuminantDirectionSamples;
  invalidate();
  m_configWidget->requestRedraw();
}

/*
 * Interface for derived class to implement
 */

void tcIlluminantDirection::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  if (!m_chromaticities.empty())
  {
    m_chromaticities[0]->roundPortion(x1,y1,x2,y2);
  }
}

void tcIlluminantDirection::loadPortions(double x1, double y1, double x2, double y2, bool changed)
{
  QList< Reference< tcPixelData<float> > > chromaticityData;
  int width = 0;
  int height = 0;
  foreach (tcChannel* channel, m_chromaticities)
  {
    /// @todo Make this neater
    double x1a = x1;
    double y1a = y1;
    double x2a = x2;
    double y2a = y2;
    Reference< tcPixelData<float> > pixelData = dynamicCast< tcPixelData<GLfloat>* >(channel->portion(&x1a,&y1a,&x2a,&y2a));
    chromaticityData += pixelData;
    Q_ASSERT(0 != pixelData);
    // Ensure each channel is the same resolution
    if (0 == width)
    {
      width = pixelData->width();
    }
    else
    {
      Q_ASSERT(width == pixelData->width());
    }
    if (0 == height)
    {
      height = pixelData->height();
    }
    else
    {
      Q_ASSERT(height == pixelData->height());
    }
  }
  loadPortions(chromaticityData, width, height);
}
