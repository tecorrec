/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcShadowDepth_h_
#define _tcShadowDepth_h_

/**
 * @file tcShadowDepth.h
 * @brief 2D depth into shadow along light direction.
 */

#include "tcChannelDem.h"

/// 2D depth into shadow along light direction.
class tcShadowDepth : public tcChannelDem
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcShadowDepth(tcChannel* lit, tcSrtmModel* dem, tcGeoImageData* imagery, bool exitDepth);

    /// Destructor.
    virtual ~tcShadowDepth();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2);

    // Reimplemented
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed);

  private:

    /*
     * Private functions
     */

    /// Structure for temporary data during the calculation process.
    struct tempData
    {
      double x1, y1, x2, y2;
      int width, height;
      Reference<tcAbstractPixelData> lit;
      tcPixelData<float>* shadowDepths;
    };

    /// Calculate the shadow depth of a pixel, recursively as necessary.
    float pixelShadowDepth(const tempData* data, int i, int j);

    /*
     * Variables
     */

    /// We'll use the same resolution as this reference channel.
    tcChannel* m_litChannel;

    /// Whether to measure the exit depth.
    bool m_exitDepth;
};

#endif
