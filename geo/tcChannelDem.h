/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcChannelDem_h_
#define _tcChannelDem_h_

/**
 * @file tcChannelDem.h
 * @brief An abstract image channel which needs access to the digital elevation model.
 */

#include "tcChannel.h"
#include "tcGeo.h"
#include <Vector.h>

class tcSrtmModel;
class tcGeoImageData;

/// An abstract image channel which needs access to the digital elevation model.
class tcChannelDem : public tcChannel
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcChannelDem(tcSrtmModel* dem, tcGeoImageData* imagery, const QString& name, const QString& description, bool depend1 = true, bool depend2 = false);

    /// Destructor.
    virtual ~tcChannelDem();

  protected:

    /*
     * Interface for derived classes to access elevation model data
     */

    // Main transformation

    /// Get the geographical coordinate at a pixel.
    tcGeo geoAt(const maths::Vector<2,float>& textureCoordinate, tcGeoImageData* imagery = 0) const;

    /// Get the texture coordinate at a geographical coordinate.
    maths::Vector<2,float> textureAt(const tcGeo& geoCoordinate, tcGeoImageData* imagery = 0) const;

    // Wrapper functions

    /// Get the altitude at a geographical coordinate.
    float altitudeAt(const tcGeo& coordinate, bool corrected = false, bool* accurate = 0) const;

    /// Get the texture-space normal vector at a geographical coordinate.
    maths::Vector<3,float> normalAt(const tcGeo& coordinate, bool corrected = false, bool* accurate = 0) const;

    /// Get the lighting direction vector at a geographical coordinate.
    maths::Vector<3,float> lightDirectionAt(const tcGeo& coordinate, tcGeoImageData* imagery = 0) const;

    /// Find whether a point is lit by the light.
    bool isLitAt(const tcGeo& coordinate, bool corrected = false, bool* accurate = 0, tcGeoImageData* imagery = 0) const;

    /// Find how lit a point is by the light.
    float litAt(const tcGeo& coordinate, bool corrected = false, bool* accurate = 0, tcGeoImageData* imagery = 0) const;

    // Lower level accessors

    /// Get the digital elevation model.
    tcSrtmModel* dem() const;

    /// Get the imagery.
    tcGeoImageData* imagery() const;

  private:

    /*
     * Variables
     */

    /// Digital elevation model.
    tcSrtmModel* m_dem;

    /// Imagery object.
    tcGeoImageData* m_imagery;
};

#endif
