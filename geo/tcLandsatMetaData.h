/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcLandsatMetaData_h_
#define _tcLandsatMetaData_h_

/**
 * @file tcLandsatMetaData.h
 * @brief Landsat metadata file.
 */

#include <QHash>
#include <QString>
#include <QStringList>
#include <QVariant>

class QFile;

/** Landsat metadata file.
 * This aims to be generic enough to read both Landsat 5 and Landsat 7 metadata files.
 */
class tcLandsatMetaData
{
  public:

    /*
     * Types
     */

    /// Nice wrapper class.
    class Reference;

    /*
     * Constructors + destructor
     */

    /// Load metadata from a file.
    tcLandsatMetaData(QFile& file);

    /// Destructor.
    virtual ~tcLandsatMetaData();

    /*
     * Accessors
     */

    /// Get the list of group names.
    QStringList groupNames() const;

    /// Get the list of object names.
    QStringList objectNames() const;

    /// Get the number of objects with a particular name.
    int numObjects(const QString& name) const;

    /// Get the list of value names.
    QStringList valueNames() const;

    /// Find whether a certain group exists.
    bool hasGroup(const QString& name) const;

    /// Find whether a certain object exists.
    bool hasObject(const QString& name) const;

    /// Find whether a certain value exists.
    bool hasValue(const QString& name) const;

    /// Access a group.
    tcLandsatMetaData* group(const QString& name) const;

    /// Access an object.
    tcLandsatMetaData* object(const QString& name, int index) const;

    /// Access a value.
    QVariant value(const QString& name) const;

  private:

    /*
     * Variables
     */

    /// Hash of groups.
    QHash<QString, tcLandsatMetaData*> m_groups;

    /// Hash of objects.
    QHash<QString, QList<tcLandsatMetaData*> > m_objects;

    /// Hash of values.
    QHash<QString, QVariant> m_values;
};

/// Nice wrapper class.
class tcLandsatMetaData::Reference
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    Reference(tcLandsatMetaData* metaData = 0)
    : m_metaData(metaData)
    {
    }

    /*
     * Accessors
     */

    /// Return whether the reference is valid
    bool isValid() const
    {
      return 0 != m_metaData;
    }

    /// Return whether the reference is valid
    operator bool() const
    {
      return isValid();
    }

    /// Find if a group exists.
    bool hasGroup(const QString& name) const
    {
      if (isValid())
      {
        return m_metaData->hasGroup(name);
      }
      return false;
    }

    /// Find if an object exists.
    int hasObject(const QString& name) const
    {
      if (isValid())
      {
        return m_metaData->numObjects(name);
      }
      return false;
    }

    /// Find if a value exists.
    bool hasValue(const QString& name) const
    {
      if (isValid())
      {
        return m_metaData->hasValue(name);
      }
      return false;
    }

    /// Get a reference to a group.
    Reference operator () (const QString& name) const
    {
      if (isValid())
      {
        return m_metaData->group(name);
      }
      return 0;
    }

    /// Get a reference to an object.
    Reference operator () (const QString& name, int index) const
    {
      if (isValid())
      {
        return m_metaData->object(name, index);
      }
      return 0;
    }

    /** Get a value.
     * We get an error when using string literals without this extra
     * definition. oh well.
     */
    QVariant operator [] (const char* name) const
    {
      if (isValid())
      {
        return m_metaData->value(name);
      }
      return QVariant();
    }

    /// Get a value.
    QVariant operator [] (const QString& name) const
    {
      if (isValid())
      {
        return m_metaData->value(name);
      }
      return QVariant();
    }

  private:

    /*
     * Variables
     */

    /// Meta data object.
    tcLandsatMetaData* m_metaData;
};

#endif

