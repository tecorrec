/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcElevationData_h_
#define _tcElevationData_h_

/**
 * @file tcElevationData.h
 * @brief A block of pixel data representing elevation.
 */

#include "tcPixelData.h"
#include "tcAffineTransform.h"

class tcGeo;

/// A block of pixel data representing elevation.
class tcElevationData : public tcPixelData<float>
{
  private:

    /*
     * Types
     */

    // Parent type.
    typedef tcPixelData<float> Parent;

  public:

    /*
     * Constructors + destructor
     */

    /// Default constructor.
    tcElevationData();

    /// Primary constructor.
    tcElevationData(int width, int height);

    /// Destructor.
    virtual ~tcElevationData();

    /*
     * Elevation interface
     */

    /// Sample elevation at a coordinate.
    float elevationAt(const tcGeo& coord) const;

    /// Get the max elevation over a range of pixels.
    float maxElevation(const tcGeo& swCorner, const tcGeo& neCorner, bool* inRange) const;

    /** Get the mean elevation over a range of pixels.
     * This is so elevation models can update their elevations from an elevation field.
     */
    float meanElevation(const tcGeo& swCorner, const tcGeo& neCorner, bool* inRange) const;

    /// Set the geographical to pixels transformation.
    void setGeoToPixels(const tcAffineTransform2<double>& geoToPixels);

    /*
     * Accessors
     */

    /// Get the coordinate of the most south westerly corner.
    tcGeo swCorner() const;

    /// Get the coordinate of the most north easterly corner.
    tcGeo neCorner() const;

  private:

    /*
     * Variables
     */

    /// Transformation from geographical to pixels.
    tcAffineTransform2<double> m_geoToPixels;
};

#endif
