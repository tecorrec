/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcChannelGroup_h_
#define _tcChannelGroup_h_

/**
 * @file tcChannelGroup.h
 * @brief A group of linked channels.
 */

#include "CountedReference.h"
#include "tcPixelData.h"

#include <QObject>
#include <QString>
#include <QList>

class tcChannel;
class tcChannelConfigWidget;

/** A single abstract image channel.
 * This sort of acts as a data source for a set of related output channels.
 */
class tcChannelGroup : public QObject
{
    Q_OBJECT

  public:

    /*
     * Types
     */

    /// Our custom channel group member class.
    class Channel;

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcChannelGroup(int channels, const QString& name, const QString& description);

    /// Destructor.
    virtual ~tcChannelGroup();

    /*
     * Metadata
     */

    /// Get the channel name.
    const QString& name() const;

    /// Get the channel description;
    const QString& description() const;

    /// Set the channel name.
    void setName(const QString& name);

    /// Set the channel description.
    void setDescription(const QString& description);

    /*
     * Main image interface
     */

    /// Get the list of output channels.
    const QList<tcChannel*>& channels() const;

    /// Get configuration widget.
    virtual tcChannelConfigWidget* configWidget();

    /// Get a reference to the pixel data of a portion of one of the output channels.
    Reference<tcAbstractPixelData> portion(int channel, double x1, double y1, double x2, double y2, bool changed);

  //protected:

    /*
     * Interface for derived class to implement
     */

    /// Round coordinates to sensible values.
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2) = 0;

    /// Load portions of pixel data for each output channel.
    virtual void loadPortions(double x1, double y1, double x2, double y2, bool changed) = 0;

  public slots:

    /*
     * Interface for derived classes
     */

    /** Invalidate this channel.
     * For example if one of the inputs has changed.
     */
    void invalidate();

  protected:

    /*
     * Protected variables
     */

    /// Pixel data of a portion of each channel.
    QList< Reference<tcAbstractPixelData> > m_portions;

  private:

    /*
     * Variables
     */

    /// Name of the channel.
    QString m_name;

    /// Description of the channel.
    QString m_description;

    /// Output channels.
    QList<tcChannel*> m_channels;

    /// Portion coordinates.
    double m_portionPosition[2][2];
};

#endif
