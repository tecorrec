/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcExportText_h_
#define _tcExportText_h_

/**
 * @file tcExportText.h
 * @brief Exports a set of channels to a file.
 */

#include <QWidget>
#include <QList>

class tcChannel;
class tcGeoImageData;

class QComboBox;
class QListWidget;

/// Exports a set of channels to a file.
class tcExportText : public QWidget
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcExportText(const QList<tcGeoImageData*>& imagery, QWidget* parent);

    /// Destructor.
    virtual ~tcExportText();

  public slots:

    /*
     * Public slots
     */

    /// Export to a file.
    void exportToFile();

  private slots:

    /*
     * Private slots
     */

    /// Set the channel to classify the pixels.
    void setClassificationChannel(int channel);

    /// Set the channel to filter which pixels to export.
    void setFilterChannel(int channel);

    /// Set the type of filtering.
    void setFilterType(int channel);

  private:

    /*
     * Variables
     */

    /// Image data.
    QList<tcGeoImageData*> m_imagery;

    /// Combo box to select classification channel.
    QComboBox* m_comboClassificationChannel;

    /// Combo box to select filter channel.
    QComboBox* m_comboFilterChannel;

    /// Combo box to select filter type.
    QComboBox* m_comboFilterType;

    /// List box of channels to export.
    QListWidget* m_listChannels;
};

#endif
