/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcElevation.cpp
 * @brief Elevation map including void highlighting.
 */

#include "tcElevation.h"
#include "tcGeoImageData.h"

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcElevation::tcElevation(const tcGeoImageData* refImagery, tcChannel* reference, tcSrtmModel* dem, tcGeoImageData* imagery)
: tcChannelDem(dem, imagery,
               tr("Elevation"),
               tr("Straight forward elevation."))
, m_referenceChannel(reference)
, m_refTransform(imagery->texToGeo() * refImagery->geoToTex())
{
}

/// Destructor.
tcElevation::~tcElevation()
{
}

/*
 * Interface for derived class to implement
 */

void tcElevation::roundPortion(double* x1, double* y1, double* x2, double* y2)
{
  //m_referenceChannel->roundPortion(x1,y1,x2,y2);
}

tcAbstractPixelData* tcElevation::loadPortion(double x1, double y1, double x2, double y2, bool changed)
{
  maths::Vector<2,double> xy1 = m_refTransform * maths::Vector<2,double>(x1,y1);
  maths::Vector<2,double> xy2 = m_refTransform * maths::Vector<2,double>(x2,y2);
  Reference<tcAbstractPixelData> channelData = m_referenceChannel->loadPortion(xy1[0], xy1[1], xy2[0], xy2[1], changed);
  int width = channelData->width();
  int height = channelData->height();

  // Create a new pixel buffer
  tcPixelData<float>* data = new tcPixelData<float>(width, height);
  startProcessing("Sampling elevation data");
  int index = 0;
  for (int j = 0; j < height; ++j)
  {
    progress((float)j/(height-1));
    for (int i = 0; i < width; ++i)
    {
      // Transform coordinates
      maths::Vector<2,float> coord(x1 + (x2-x1)*i / (width  - 1),
                                   y1 + (y2-y1)*j / (height - 1));
      tcGeo geoCoord = geoAt(coord);

      // Get some elevation data
      bool accurate;
      float altitude        = altitudeAt(geoCoord, false, &accurate);
      if (false && !accurate)
      {
        altitude = 0.0f;
      }
      data->buffer()[index] = altitude/4000.0f;
      ++index;
    }
  }
  endProcessing();
  return data;
}
