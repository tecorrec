/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcGlobe_h_
#define _tcGlobe_h_

/**
 * @file tcGlobe.h
 * @brief Manages data for a globe.
 */

#include "tcSrtmModel.h"

#include <QList>

class tcGeo;
class tcGeoImageData;
class tcObserver;

/** Manages data for a globe.
 * Holds terrain elevation and imagery data.
 */
class tcGlobe
{
  public:

    /*
     * Enumerations
     */

    /// Elevation modes.
    enum ElevationMode
    {
      NoElevation,
      RawElevation,
      CorrectedElevation
    };

    /// Colour codings.
    enum ColourCoding {
      NoColourCoding = 0,
      ElevationSampleAlignment
    };

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcGlobe(double meanRadius);

    /// Destructor.
    virtual ~tcGlobe();

    /*
     * Data sets
     */

    /// Add some imagery data.
    void addImagery(tcGeoImageData* data);

    /// Get the imagery data.
    const QList<tcGeoImageData*>& imagery() const;

    /// Get the elevation model.
    tcSrtmModel* dem() const;

    /*
     * Rendering
     */

    /// Render from the POV of an observer.
    void render(tcObserver* const observer = 0, bool adaptive = true, const tcGeo* extent = 0);

    /// Set the elevation mode to render in.
    void setElevationMode(int dem, ElevationMode mode);

    /// Set the level of interpolation.
    void setElevationInterpolation(float interpolation);

    /// Set the elevation data set name.
    void setElevationDataSet(int dem, const QString& name);

    /// Set colour coding method.
    void setColourCoding(ColourCoding colourCoding);

    /// Adjust the mapping between bands and colour channels.
    void setColourMapping(int outputChannel, int inputBand, int inputGroup);

    /*
     * Accessors
     */

    /// Get the mean radius.
    double meanRadius() const;

    /// Get the altitude above sea level at a sample in a render state.
    double altitudeAt(const tcSrtmModel::RenderState& state, int x, int y, tcGeo* outCoord, bool* isAccurate = 0) const;

    /// Get the altitude above sea level at a coordinate.
    double altitudeAt(const tcGeo& coord, bool* isAccurate = 0) const;

    /// Get the radius at a coordinate.
    double radiusAt(const tcGeo& coord) const;

    /// Get the texture coordinate of the effective texture at a geographical coordinate.
    maths::Vector<2,double> textureCoordOfGeo(const tcGeo& coord) const;

    /// Get the current normal at a coordinate.
    maths::Vector<3,float> normalAt(int norm, const tcGeo& coord) const;

  private:

    /*
     * Rendering
     */

    /// Draw a line of latitude.
    void drawLineOfLatitude(double latitude) const;

    /// Render a cell.
    void renderCell(tcObserver* const observer, const tcGeo& swCorner, const tcGeo& neCorner, int samples, bool normals = false,
                    bool northEdge = false, bool eastEdge = false, bool southEdge = false, bool westEdge = false) const;

  private:

    /*
     * Variables
     */

    /// Mean radius in metres.
    double m_meanRadius;

    /// Elevation model.
    tcSrtmModel* m_elevation;

    /// Image data.
    QList<tcGeoImageData*> m_imagery;

    /*
     * Rendering options
     */

    /// Current elevation mode.
    ElevationMode m_elevationMode[2];

    /// Current level of elevation interpolation.
    float m_elevationInterpolation;

    /// Colour coding method.
    ColourCoding m_colourCoding;

    /// Mapping of bands to colour channels.
    int m_colourMapping[6][2];
};

#endif

