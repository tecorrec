/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcChannel_h_
#define _tcChannel_h_

/**
 * @file tcChannel.h
 * @brief A single abstract image channel.
 */

#include "tcPixelData.h"
#include "tcGeo.h"

#include <QObject>
#include <QString>
#include <QList>

#include <GL/gl.h>

class tcChannelConfigWidget;
class tcChannelManager;

/// A single abstract image channel.
class tcChannel : public QObject
{
    Q_OBJECT

  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcChannel(const QString& name, const QString& description);

    /// Destructor.
    virtual ~tcChannel();

    /*
     * Metadata
     */

    /// Get the channel manager.
    tcChannelManager* channelManager() const;

    /// Set the channel manager.
    void setChannelManager(tcChannelManager* channelManager);

    /// Get the channel name.
    const QString& name() const;

    /// Get the channel description;
    const QString& description() const;

    /// Set the channel name.
    void setName(const QString& name);

    /// Set the channel description.
    void setDescription(const QString& description);

    /// Set radiance offset and gain.
    void setRadianceTransform(float offset, float gain);

    /// New cross section selected.
    void newCrossSection(const tcGeo& p1, const tcGeo& p2);

    /*
     * Dependencies
     */

    /// Add derivitive of this channel.
    void addDerivitive(tcChannel* derivitive);

    /// Remove a derivitive of this channel.
    void removeDerivitive(tcChannel* derivitive);

    /*
     * Main image interface
     */

    /// Get configuration widget.
    virtual tcChannelConfigWidget* configWidget();

    /// Get GL texture ID of thumbnail of the channel.
    virtual GLuint thumbnailTexture();

    /// Get a reference to the pixel data of the current portion of the image.
    Reference<tcAbstractPixelData> portion() const;

    /// Get a reference to the pixel data of a portion of the image.
    Reference<tcAbstractPixelData> portion(double* x1, double* y1, double* x2, double* y2);

    /// Get a reference to the pixel data of a portion of the image.
    Reference<tcAbstractPixelData> portion(double x1, double y1, double x2, double y2);

    /// Get GL texture ID of portion of the channel.
    GLuint portionTexture(double* x1, double* y1, double* x2, double* y2);

    /// Radiance scaling.
    float valueToRadiance(float value)
    {
      return m_radianceOffset + value*m_radianceGain;
    }

    /// Get the last portion position.
    const double* portionPosition() const;

  //protected:

    /*
     * Interface for derived class to implement
     */

    /// Round coordinates to sensible values.
    virtual void roundPortion(double* x1, double* y1, double* x2, double* y2) = 0;

    /// Load a portion of pixel data from the channel source, wherever that may be.
    virtual tcAbstractPixelData* loadPortion(double x1, double y1, double x2, double y2, bool changed) = 0;

  public slots:

    /*
     * Interface for derived classes
     */

    /** Invalidate this channel.
     * For example if one of the inputs has changed.
     */
    void invalidate();

  signals:

    /*
     * Signals
     */

    /// Emitted to indicate progress of processing.
    void progressing(const QString& message);

  protected:

    /*
     * Progress functions
     */

    /// Start a processing step.
    void startProcessing(const QString& step);

    /// Update progress.
    void progress(float progress);

    /// Update progress.
    void progress(const QString& info);

    /// End processing step.
    void endProcessing();

  private:

    /*
     * Variables
     */

    /// Channel manager.
    tcChannelManager* m_channelManager;

    /// Name of the channel.
    QString m_name;

    /// Description of the channel.
    QString m_description;

    /// List of channels which are derived from this channel.
    QList<tcChannel*> m_derivitives;

    /// Pixel data of a portion of the channel.
    Reference<tcAbstractPixelData> m_portion;

    /// Portion coordinates.
    double m_portionPosition[2][2];

    /// Radiance scaling information.
    float m_radianceOffset, m_radianceGain;

  protected:

    /// Whether cross sectioned.
    bool m_crossSectioned;

    /// Current cross section.
    tcGeo m_crossSection[2];

  private:

    /*
     * Static variables
     */

    /// Current processing step.
    static QString s_processingStep;

    /// Last progress percent.
    static int s_processingLastPercent;
};

#endif
