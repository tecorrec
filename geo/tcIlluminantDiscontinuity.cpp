/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcIlluminantDiscontinuity.cpp
 * @brief Illuminant discontinuity in both dimentions.
 */

#include "tcIlluminantDiscontinuity.h"
#include "tcChannel.h"

#include <QObject>

#include <cmath>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcIlluminantDiscontinuity::tcIlluminantDiscontinuity(const QList<tcChannel*>& newChromaticities)
: tcIlluminantDirection(newChromaticities, 2, tr("Illuminant discontinuity measure"), tr(""))
, m_threshold(0.3f)
{
  channels()[0]->setName(tr("idm.x"));
  channels()[1]->setName(tr("idm.y"));
  channels()[0]->setDescription(tr("Illuminant discontinuity measure in the x direction"));
  channels()[1]->setDescription(tr("Illuminant discontinuity measure in the y direction"));
  foreach (tcChannel* channel, chromaticities())
  {
    for (int i = 0; i < 2; ++i)
    {
      channel->addDerivitive(channels()[i]);
    }
  }
}

/// Destructor.
tcIlluminantDiscontinuity::~tcIlluminantDiscontinuity()
{
  foreach (tcChannel* channel, chromaticities())
  {
    for (int i = 0; i < 2; ++i)
    {
      channel->removeDerivitive(channels()[i]);
    }
  }
}

/*
 * Interface for derived class to implement
 */

void tcIlluminantDiscontinuity::loadPortions(const QList< Reference< tcPixelData<float> > >& chromaticities, int width, int height)
{
  Reference< tcPixelData<float> > discontinuityData[2];
  for (int i = 0; i < 2; ++i)
  {
    discontinuityData[i] = new tcPixelData<GLfloat>(width, height);
    m_portions += discontinuityData[i];
  }

  // Go through the pixels
  maths::VarVector<float> lastLogChromaticity(numChromaticities());
  maths::VarVector<float> logChromaticity(numChromaticities());
  maths::VarVector<float> temp(numChromaticities());
  float lastDisc = 0.0f;
  int lastSaturation = 0;
  for (int i = 0; i < width; ++i)
  {
    for (int j = 0; j < height; ++j)
    {
      int index = j*width + i;
      // Fill log chromaticity vector
      int saturation = 0;
      for (int channel = 0; channel < numChromaticities(); ++channel)
      {
        if (chromaticities[channel]->buffer()[index] != 0.0f)
        {
          ++saturation;
          logChromaticity[channel] = logf(chromaticities[channel]->buffer()[index]);
        }
        else
        {
          logChromaticity[channel] = 0.0f;
        }
      }

      temp = logChromaticity;
      temp -= lastLogChromaticity;
      temp *= (float)lastSaturation / numChromaticities();

      float discontinuity = lastDisc;
      float sqr = temp.sqr();
      if (sqr > 0.0f)
      {
        float mag = sqrtf(sqr);
        if (mag > 0.0f)
        {
          discontinuity = fabs(temp * illuminantDirection()) / mag;
        }
      }
      lastDisc = discontinuity*0.5f;
      lastSaturation = saturation;

      lastLogChromaticity = logChromaticity;

      discontinuity = (discontinuity > m_threshold ? discontinuity : 0.0f);
      if (saturation < numChromaticities())
      {
        discontinuity = 0.0f;
      }
      //discontinuity *= (float)saturation / numChromaticities();
      discontinuityData[0]->buffer()[index] = discontinuity;
    }
  }
  lastDisc = 0.0f;
  lastSaturation = 0;
  for (int j = 0; j < height; ++j)
  {
    for (int i = 0; i < width; ++i)
    {
      int index = j*width + i;
      // Fill log chromaticity vector
      int saturation = 0;
      for (int channel = 0; channel < numChromaticities(); ++channel)
      {
        if (chromaticities[channel]->buffer()[index] != 0.0f)
        {
          ++saturation;
          logChromaticity[channel] = logf(chromaticities[channel]->buffer()[index]);
        }
        else
        {
          logChromaticity[channel] = 0.0f;
        }
      }

      temp = logChromaticity;
      temp -= lastLogChromaticity;
      temp *= (float)lastSaturation / numChromaticities();

      float discontinuity = lastDisc;
      float sqr = temp.sqr();
      if (sqr > 0.0f)
      {
        float mag = sqrtf(sqr);
        if (mag > 0.0f)
        {
          discontinuity = fabs(temp * illuminantDirection()) / mag;
        }
      }
      lastDisc = discontinuity*0.5f;
      lastSaturation = saturation;

      lastLogChromaticity = logChromaticity;

      discontinuity = (discontinuity > m_threshold ? discontinuity : 0.0f);
      if (saturation < numChromaticities())
      {
        discontinuity = 0.0f;
      }
      //discontinuity *= (float)saturation / numChromaticities();
      discontinuityData[1]->buffer()[index] = discontinuity;
    }
  }
}
