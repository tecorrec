/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcRangeSpectrum.h
 * @brief Frequency spectrum of a uniform value in a range.
 */

#include "tcRangeSpectrum.h"

#include <QtGlobal>

/*
 * Constructors + destructor
 */

/// Default constructor.
tcRangeSpectrum::tcRangeSpectrum(float max, float x, float y)
: tcSpectrum()
, m_max(max)
, m_x(x)
, m_y(y)
{
  Q_ASSERT(y >= x);
}

/// Destructor.
tcRangeSpectrum::~tcRangeSpectrum()
{
}

/*
 * Virtual interface
 */

float tcRangeSpectrum::mean() const
{
  return (m_x + m_y) / 2.0f;
}

float tcRangeSpectrum::integrate() const
{
  return (m_y - m_x) * m_max;
}

float tcRangeSpectrum::integrate(float x) const
{
  if (x <= m_x)
  {
    return 0.0f;
  }
  else
  {
    if (x >= m_y)
    {
      x = m_y;
    }
    return (x - m_x) * m_max;
  }
}
