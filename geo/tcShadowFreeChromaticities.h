/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _tcShadowFreeChromaticities_h_
#define _tcShadowFreeChromaticities_h_

/**
 * @file tcShadowFreeChromaticities.h
 * @brief Remove the shadows from a set of chromaticities.
 */

#include "tcIlluminantDirection.h"

/// Remove the shadows from a set of chromaticities.
class tcShadowFreeChromaticities : public tcIlluminantDirection
{
  public:

    /*
     * Constructors + destructor
     */

    /// Primary constructor.
    tcShadowFreeChromaticities(const QList<tcChannel*>& chromaticities);

    /// Destructor.
    virtual ~tcShadowFreeChromaticities();

  protected:

    /*
     * Interface for derived class to implement
     */

    // Reimplemented
    virtual void loadPortions(const QList< Reference< tcPixelData<float> > >& chromaticities, int width, int height);
};

#endif
