/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcLandsatMetaData.cpp
 * @brief Landsat metadata file.
 */

#include "tcLandsatMetaData.h"

#include <QFile>
#include <QRegExp>

#include <QtDebug>

/*
 * Constructors + destructor
 */

/// Load metadata from a file.
tcLandsatMetaData::tcLandsatMetaData(QFile& file)
{
  static QRegExp regexp("^\\s*(\\w+)\\s*=\\s*(\"(.*)\"|(\\S(.*\\S)?))\\s*$");
  static QRegExp end("^\\s*END\\s*$");
  static QRegExp blank("^\\s*$");
  static QRegExp strlist("^\\s*\\((.*)\\)\\s*$");
  static QRegExp str("^\\s*\"([^\"]*)\"\\s*$");
  while (!file.atEnd())
  {
    QByteArray line = file.readLine();
    if (regexp.exactMatch(line))
    {
      QString name = regexp.cap(1);
      if ("GROUP" == name)
      {
        name = regexp.cap(2);
        if (m_groups.contains(name))
        {
          delete m_groups[name];
          qDebug() << "Landsat meta data has conflicting groups:" << line;
        }
        tcLandsatMetaData* newMeta = new tcLandsatMetaData(file);
        m_groups[name] = newMeta;
      }
      else if ("END_GROUP" == name)
      {
        return;
      }
      if ("OBJECT" == name)
      {
        name = regexp.cap(2);
        tcLandsatMetaData* newMeta = new tcLandsatMetaData(file);
        m_objects[name] += newMeta;
      }
      else if ("END_OBJECT" == name)
      {
        return;
      }
      else
      {
        QVariant value;
        QString valueStr = regexp.cap(3);
        if (valueStr.isEmpty())
        {
          valueStr = regexp.cap(4);
          // Try turning it into a double
          bool ok;
          double d = valueStr.toDouble(&ok);
          if (ok)
          {
            value = d;
          }
          else
          {
            // perhaps its a list
            if (strlist.exactMatch(valueStr))
            {
              QStringList values = strlist.cap(1).split(",");
              QList<QVariant> varValues;
              for (int i = 0; i < values.size(); ++i)
              {
                // of strings?
                if (str.exactMatch(values[i]))
                {
                  varValues += str.cap(1);
                }
                else
                {
                  // or maybe doubles
                  bool ok;
                  double d = values[i].toDouble(&ok);
                  if (ok)
                  {
                    varValues += d;
                  }
                  else
                  {
                    varValues += values[i];
                  }
                }
              }
              value = varValues;
            }
            else
            {
              value = valueStr;
            }
          }
        }
        else
        {
          value = valueStr;
        }
        m_values[name] = value;
      }
    }
    else if (end.exactMatch(line))
    {
      return;
    }
    else if (blank.exactMatch(line))
    {
    }
    else
    {
      qDebug() << "Parsing error reading landsat meta data:" << line;
    }
  }
}

/// Destructor.
tcLandsatMetaData::~tcLandsatMetaData()
{
  foreach (tcLandsatMetaData* group, m_groups)
  {
    delete group;
  }
  QStringList objectNames = m_objects.keys();
  foreach (QString objectName, objectNames)
  {
    QList<tcLandsatMetaData*>& objects = m_objects[objectName];
    foreach (tcLandsatMetaData* object, objects)
    {
      delete object;
    }
  }
}

/*
 * Accessors
 */

/// Get the list of group names.
QStringList tcLandsatMetaData::groupNames() const
{
  return m_groups.keys();
}

/// Get the list of object names.
QStringList tcLandsatMetaData::objectNames() const
{
  return m_objects.keys();
}

/// Get the list of value names.
QStringList tcLandsatMetaData::valueNames() const
{
  return m_values.keys();
}

/// Find whether a certain group exists.
bool tcLandsatMetaData::hasGroup(const QString& name) const
{
  return m_groups.contains(name);
}

/// Find whether a certain object exists.
bool tcLandsatMetaData::hasObject(const QString& name) const
{
  return m_groups.contains(name);
}

/// Find whether a certain value exists.
bool tcLandsatMetaData::hasValue(const QString& name) const
{
  return m_values.contains(name);
}

/// Get the number of objects with a particular name.
int tcLandsatMetaData::numObjects(const QString& name) const
{
  if (m_objects.contains(name))
  {
    return m_objects[name].size();
  }
  else
  {
    return 0;
  }
}

/// Access a group.
tcLandsatMetaData* tcLandsatMetaData::group(const QString& name) const
{
  if (m_groups.contains(name))
  {
    return m_groups[name];
  }
  else
  {
    return 0;
  }
}

/// Access an object.
tcLandsatMetaData* tcLandsatMetaData::object(const QString& name, int index) const
{
  if (m_objects.contains(name) && index >= 0 && index < m_objects[name].size())
  {
    return m_objects[name][index];
  }
  else
  {
    return 0;
  }
}

/// Access a value.
QVariant tcLandsatMetaData::value(const QString& name) const
{
  if (m_values.contains(name))
  {
    return m_values[name];
  }
  else
  {
    return QVariant();
  }
}
