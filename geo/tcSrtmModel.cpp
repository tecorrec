/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcSrtmModel.cpp
 * @brief SRTM elevation model.
 */

#include "tcSrtmModel.h"
#include "tcSrtmCache.h"
#include "tcSrtmData.h"

#include <inttypes.h>

/*
 * Constructors + destructor
 */

/// Default constructor.
tcSrtmModel::tcSrtmModel()
: m_cache(0)
{
}

/// Destructor.
tcSrtmModel::~tcSrtmModel()
{
  delete m_cache;
}

/*
 * Main interface
 */

/// Get information to allow alignment with actual samples.
void tcSrtmModel::sampleAlign(const tcGeo& swCorner, const tcGeo& neCorner, RenderState* state, int maxSamples)
{
  state->swCorner = swCorner;
  state->neCorner = neCorner;
  state->moreAvailableLon = false;
  state->moreAvailableLat = false;

  tcSrtmData* data = 0;
  if (0 != m_cache)
  {
    data = m_cache->findData(swCorner, neCorner);
    if (data)
    {
      tcGeo resolution = data->sampleResolution();
      maths::Vector<2,double> swSampleSpace = swCorner / resolution;
      maths::Vector<2,double> neSampleSpace = neCorner / resolution;
      maths::Vector<2,double> first(floor(swSampleSpace[0] + 1.0),
                                    floor(swSampleSpace[1] + 1.0));
      maths::Vector<2,double> last( ceil( neSampleSpace[0] - 1.0),
                                    ceil( neSampleSpace[1] - 1.0));
      state->samplesLon = 3 + (int)(last[0] - first[0]);
      state->samplesLat = 3 + (int)(last[1] - first[1]);
      state->swSample = swCorner + resolution*(first - swSampleSpace);
      state->sampleDelta = resolution;

      // Sample evenly
      if (maxSamples >= 2)
      {
        if (state->samplesLon > maxSamples)
        {
          state->sampleDelta.setLon((neCorner.lon()-swCorner.lon()) / (maxSamples-1));
          state->swSample.setLon(swCorner.lon() + state->sampleDelta.lon());
          state->samplesLon = maxSamples;
          state->moreAvailableLon = true;
        }
        if (state->samplesLat > maxSamples)
        {
          state->sampleDelta.setLat((neCorner.lat()-swCorner.lat()) / (maxSamples-1));
          state->swSample.setLat(swCorner.lat() + state->sampleDelta.lat());
          state->samplesLat = maxSamples;
          state->moreAvailableLat = true;
        }
      }
      return;
    }
  }

  int samplesLon = (int)((neCorner.lon() - swCorner.lon())/(M_PI/180.0/60.0));
  int samplesLat = (int)((neCorner.lat() - swCorner.lat())/(M_PI/180.0/60.0));
  if (samplesLon > maxSamples)
  {
    state->moreAvailableLon = true;
  }
  if (samplesLat > maxSamples)
  {
    state->moreAvailableLat = true;
  }


  state->samplesLon = maxSamples;
  state->samplesLat = maxSamples;
  state->sampleDelta = (neCorner - swCorner)/(maxSamples-1);
  state->swSample = swCorner + state->sampleDelta;
}

/// Get the altitude at a sample in a render state.
double tcSrtmModel::altitudeAt(const RenderState& state, int x, int y, tcGeo* outCoord, bool corrected, bool* accurate)
{
  double lon;
  if (x <= 0)
  {
    lon = state.swCorner.lon();
  }
  else if (x >= state.samplesLon - 1)
  {
    lon = state.neCorner.lon();
  }
  else
  {
    lon = state.swSample.lon() + state.sampleDelta.lon() * (x-1);
  }
  double lat;
  if (y <= 0)
  {
    lat = state.swCorner.lat();
  }
  else if (y >= state.samplesLat - 1)
  {
    lat = state.neCorner.lat();
  }
  else
  {
    lat = state.swSample.lat() + state.sampleDelta.lat() * (y-1);
  }
  return altitudeAt(*outCoord = tcGeo(lon, lat), corrected, accurate);
}

/// Get the altitude at a coordinate.
double tcSrtmModel::altitudeAt(const tcGeo& coord, bool corrected, bool* accurate)
{
  // Round down to nearest degree
  tcSrtmData* data = 0;
  if (0 != m_cache)
  {
    data = m_cache->loadData(coord);
  }
  if (0 != data)
  {
    return data->sampleAltitude(coord, corrected, accurate);
  }
  return 0.0;
}

/// Get the normal at a coordinate.
maths::Vector<3,float> tcSrtmModel::normalAt(const tcGeo& coord, bool corrected, bool* accurate)
{
  if (0 != m_cache)
  {
    tcSrtmData* data = m_cache->loadData(coord);
    if (0 != data)
    {
      // Sample altitudes a fixed angle in each direction.
      const double radiusEarth = 6378.137e3;
      tcGeo angRes = data->sampleResolution();
      bool alta, xpa, xma, ypa, yma;
      double alt = altitudeAt(coord, corrected, &alta);
      double xp = altitudeAt(coord + tcGeo(angRes.lon()*0.5, 0.0), corrected, &xpa);
      double xm = altitudeAt(coord - tcGeo(angRes.lon()*0.5, 0.0), corrected, &xma);
      double yp = altitudeAt(coord + tcGeo(0.0, angRes.lat()*0.5), corrected, &ypa);
      double ym = altitudeAt(coord - tcGeo(0.0, angRes.lat()*0.5), corrected, &yma);
      double dx = xp-xm;
      double dy = yp-ym;
      if (0 != accurate)
      {
        *accurate = xpa && xma && ypa && yma;
      }
      maths::Vector<2,float> res((radiusEarth+alt) * angRes.lon() * cos(coord.lat()),
                                 (radiusEarth+alt) * angRes.lat());
      // (1.0f, 0.0f, dx/res[0]) x (0.0f, -1.0f, dy/res[1])
      maths::Vector<3,float> norm(-dx/res[0], -dy/res[1], 1.0f);
      norm.normalize();
      return norm;
    }
  }
  if (0 != accurate)
  {
    *accurate = false;
  }
  return maths::Vector<3,float>(0.0f, 0.0f, 1.0f);
}

/// Use raytracing to find how lit a point is.
float tcSrtmModel::litAt(const tcGeo& coord, const maths::Vector<3,float>& light, float angularRadius, bool corrected, bool* accurate)
{
  if (0 != m_cache)
  {
    tcSrtmData* data = m_cache->loadData(coord);
    if (0 != data)
    {
      // Sample altitudes a fixed angle in each direction.
      const float radiusEarth = 6378.137e3;
      tcGeo angRes = data->sampleResolution();
      float distance = 0.0f;
      float step = (angRes.lon() + angRes.lat()) / 4.0;
      bool ac = true;
      float result = 1.0f;
      bool xa;
      tcGeo cur = coord;
      float alt = altitudeAt(cur, corrected, &xa);
      ac = ac && xa;
      Q_ASSERT(light[2] > 0.0);
      while (alt < 8000.0)
      {
        // Move towards light source
        cur.setLon(cur.lon() + step*light[0] / cos(cur.lat()));
        cur.setLat(cur.lat() + step*light[1]);
        alt += radiusEarth*step*light[2];
        distance += radiusEarth*step;
        // Check for collision
        float x = altitudeAt(cur, corrected, &xa);
        ac = ac && xa;
        float flex = distance*angularRadius;
        if (x >= alt + flex)
        {
          result = 0.0f;
          break;
        }
        else if (x >= alt - flex)
        {
          float portion = (x - alt + flex) / (2.0f*flex);
          if (result > portion)
          {
            result = portion;
          }
        }
        // Can take bigger steps the further we are from the ground
        // here we are assuming that the terrain isn't extremely steep
        step = 0.5f * (alt - x + flex) / radiusEarth;
      }
      if (0 != accurate)
      {
        *accurate = ac;
      }
      return result;
    }
  }
  if (0 != accurate)
  {
    *accurate = false;
  }
  return 1.0f;
}

/// Set the data set to use.
void tcSrtmModel::setDataSet(const QString& name)
{
  delete m_cache;
  if (!name.isEmpty())
  {
    m_cache = new tcSrtmCache(name);
    if (0 != m_cache)
    {
      connect(m_cache, SIGNAL(progressing(const QString&)), this, SIGNAL(progressing(const QString&)));
    }
  }
  else
  {
    m_cache = 0;
  }
  emit dataSetChanged();
}

/// Update any samples affected by the elevation field.
void tcSrtmModel::updateFromElevationData(const tcElevationData* elevation)
{
  if (0 != m_cache)
  {
    m_cache->updateFromElevationData(elevation);
    emit dataSetChanged();
  }
}
