/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcViewportWidget.cpp
 * @brief OpenGL viewport widget.
 */

#include "tcViewportWidget.h"
#include <tcObserver.h>
#include <tcGlobe.h>
#include <tcGeoImageData.h>
#include <tcChannelManager.h>
#include <tcChannel.h>
#include <Vector.h>
#include <glVector.h>

#include <QMouseEvent>
#include <QtDebug>

#include <cmath>

/*
 * Constructors + destructor
 */

/// Primary constructor.
tcViewportWidget::tcViewportWidget(QWidget* parent)
: QGLWidget(parent)
, m_adaptiveQuality(true)
, m_polygonMode(GL_FILL)
, m_observer(new tcObserver())
, m_globe(0)
, m_interactionMode(Navigation)
, m_mouseInteracting(false)
, m_mousePos()
, m_mouseIntersecting(false)
, m_mouseCoord()
, m_mouseSlicing(false)
, m_mouseCrossing(false)
, m_sliced(false)
, m_crossSectioned(false)
, m_texturePointObject(0)
, m_texturePointMember(0)
{
  setMouseTracking(true);
  setMinimumSize(400,300);
}

/// Destructor.
tcViewportWidget::~tcViewportWidget()
{
  delete m_observer;
}

/*
 * Modifiers
 */

/// Set the globe object.
void tcViewportWidget::setGlobe(tcGlobe* globe)
{
  m_globe = globe;
  tcGeo pos( 7.03227 * M_PI/180, 45.92093 * M_PI/180);
  m_observer->setFocus(pos, globe->radiusAt(pos));
}

/*
 * Accessors
 */

/// Find the coordinates under the mouse.
tcGeo tcViewportWidget::geoAt(float x, float y, bool* ok)
{
  maths::Vector<2,double> screenXy(x / width(), 1.0 - y / height());
  maths::Vector<3,double> obs = m_observer->position();
  maths::Vector<3,double> ray = m_observer->ray(screenXy, (double)width() / height());
  double r = m_observer->focusAltitude();
  double ro = obs*ray;
  double roro = ro*ro;
  bool intersecting = false;
  // find intersection P of ray and globe
  // given globe radius r, observer O, ray R, where |R|=1
  // globe: P*P = r*r
  // line:  P = O + t*R
  //        (O+t*R)*(O+t*R) = r*r
  //        O*O + 2*O*t*R + t*t*R*R = r*r
  //        O*O + 2*O*t*R + t*t - r*r = 0
  // quadratic in parameter t
  // a = 1, b = 2*RO c = O*O-r*r RO = O*R
  // t = (-b +- sqrt(b*b - 4ac)) / 2a
  //   = (-2RO +- sqrt(4*RO*RO - 4*O*O + 4*r*r)) / 2
  //   = (-2*RO +- 2*sqrt(RO*RO - O*O + r*r)) / 2
  //   = RO += sqrt(RO*RO - O*O + r*r)
  double discriminant = roro - obs.sqr() + r*r;
  if (discriminant >= 0.0)
  {
    discriminant = sqrt(discriminant);
    double t = -ro - discriminant;
    if (t > 0.0)
    {
      obs += ray*t;
      obs /= r; // should now be normalized
      intersecting = true;
    }
  }
  else
  {
    // Hmm, doesn't work
#if 0
    // mouse is outside of globe
    // use closest point on outline circle of globe
    // sphere             P*P=r*r
    // tangential circle: P*(P-O) = 0
    //                    P*P - P*O = 0
    //                    r*r - P*O = 0
    // line:              P/s = O + tR
    //                    P = s(O + tR) (s>0)
    //                    r*r - s(O + tR)O = 0
    //                    r*r - s(OO + tRO) = 0
    //                    s(OO + tRO) = r*r
    //                    s = r*r/(OO+tRO)
    //                    t = (r*r/s - OO)/RO
    //
    //                    (sO + tsR)(sO + tsR - O) = 0
    //                    (O + tR)(sO + tsR - O) = 0
    //                    sOO + tsRO + tsOR + ttsRR - OO - tRO = 0
    //                    sOO + 2tsRO + tts - OO - tRO = 0
    //                    s(OO + 2tRO + tt) = OO - tRO
    //                    substitute s=rr/(OO+tRO)
    //                    rr/(OO+tRO)(OO + 2tRO + tt) = OO - tRO
    //                    rr(OO + 2tRO + tt) = (OO - tRO)(OO+tRO)
    //                    rrOO + 2rrtRO + rrtt = OO*OO - ttRO*RO
    //                    tt(rr + RO*RO) + t(2rrRO) + (rrOO - OO*OO)
    // a = rr + RO*RO, b = 2rrRO, c = rr00 - OO*OO
    // t = (-2rrRO +- sqrt(4rrrrRO*RO - 4(rr + RO*RO)(rr00 - OO*OO))) / 2(rr + RO*RO)
    // t = (-rrRO +- sqrt(rrrrRO*RO - (rr + RO*RO)(rr00 - OO*OO))) / (rr + RO*RO)
    double rr = r*r;
    double oo = obs*obs;
    double oooo = oo*oo;
    double discriminant = rr*rr*roro - (rr + roro)*(rr*oo - oo*oo);
    if (discriminant >= 0)
    {
      discriminant = sqrt(discriminant);
      double t = (-rr*ro + discriminant) / (rr + roro);
      double s = rr/(oo + t*ro);
      if (t >= 0.0)
      {
        obs += ray*t;
        obs /= s;
        obs /= r; // should now be normalized
        obs.normalize();
        intersecting = true;
      }
    }
#endif
  }
  if (0 != ok)
  {
    *ok = intersecting;
  }
  if (intersecting)
  {
    return tcGeo(obs, true);
  }
  else
  {
    return tcGeo();
  }
}

/*
 * Observer control
 */

/// Change to sun view.
void tcViewportWidget::sunView(int imagery)
{
  m_observer->setView(tcGeo(m_globe->imagery()[imagery]->sunDirection(m_observer->focus()), true));
  updateGL();
}

/*
 * Interaction control
 */

/// Enable navigation mode.
void tcViewportWidget::navigationMode()
{
  m_interactionMode = Navigation;
}

/// Enable texture point selection mode.
void tcViewportWidget::texturePointMode(QObject* receiver, const char* member)
{
  m_interactionMode = TexturePoint;
  m_texturePointObject = receiver;
  m_texturePointMember = member;
}

/// Set slice.
void tcViewportWidget::setSlice(const tcGeo& sw, const tcGeo& ne)
{
  m_sliced = true;
  m_slice[0] = sw;
  m_slice[1] = ne;
  updateGL();
}

/// Set cross section.
void tcViewportWidget::setCrossSection(const tcGeo& p1, const tcGeo& p2)
{
  m_crossSectioned = true;
  m_crossSection[0] = p1;
  m_crossSection[1] = p2;
  // Let all channels know of changes
  QList<tcGeoImageData*> imagery = m_globe->imagery();
  foreach (tcGeoImageData* image, imagery)
  {
    for (int i = 0; i < image->channelManager()->numChannels(); ++i)
    {
      image->channelManager()->channel(i)->newCrossSection(p1, p2);
    }
  }
  updateGL();
}


/*
 * General rendering slots
 */

/// Change the quality to adaptive.
void tcViewportWidget::setQualityAdaptive()
{
  m_adaptiveQuality = true;
  updateGL();
}

/// Change the quality to full resolution.
void tcViewportWidget::setQualityFull()
{
  m_adaptiveQuality = false;
  updateGL();
}

/// Change the polygon mode to normal.
void tcViewportWidget::setPolygonModeNormal()
{
  m_polygonMode = GL_FILL;
  updateGL();
}

/// Change the polygon mode to wireframe.
void tcViewportWidget::setPolygonModeWireframe()
{
  m_polygonMode = GL_LINE;
  updateGL();
}

/// Remove colour coding.
void tcViewportWidget::setColourCodingNone()
{
  m_globe->setColourCoding(tcGlobe::NoColourCoding);
  updateGL();
}

/// Set colour coding to elevation sample alignment.
void tcViewportWidget::setColourCodingElevationSampleAlignment()
{
  m_globe->setColourCoding(tcGlobe::ElevationSampleAlignment);
  updateGL();
}

/// Set colour mapping for an output channel to an input band.
void tcViewportWidget::setColourMapping(int output, int input, int inputGroup)
{
  m_globe->setColourMapping(output, input, inputGroup);
  updateGL();
}

/*
 * Elevation modification slots
 */

/// Set the primary elevation data set name.
void tcViewportWidget::setPrimaryElevationDataSet(const QString& name)
{
  if (0 != m_globe)
  {
    m_globe->setElevationDataSet(0, name);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Set the secondary elevation data set name.
void tcViewportWidget::setSecondaryElevationDataSet(const QString& name)
{
  if (0 != m_globe)
  {
    m_globe->setElevationDataSet(1, name);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Go to flat primary elevation mode.
void tcViewportWidget::setPrimaryElevationFlat()
{
  if (0 != m_globe)
  {
    m_globe->setElevationMode(0, tcGlobe::NoElevation);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Go to flat secondary elevation mode.
void tcViewportWidget::setSecondaryElevationFlat()
{
  if (0 != m_globe)
  {
    m_globe->setElevationMode(1, tcGlobe::NoElevation);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Go to raw SRTM primary elevation mode.
void tcViewportWidget::setPrimaryElevationRaw()
{
  if (0 != m_globe)
  {
    m_globe->setElevationMode(0, tcGlobe::RawElevation);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Go to raw SRTM secondary elevation mode.
void tcViewportWidget::setSecondaryElevationRaw()
{
  if (0 != m_globe)
  {
    m_globe->setElevationMode(1, tcGlobe::RawElevation);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Go to corrected primary elevation mode.
void tcViewportWidget::setPrimaryElevationCorrected()
{
  if (0 != m_globe)
  {
    m_globe->setElevationMode(0, tcGlobe::CorrectedElevation);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Go to corrected secondary elevation mode.
void tcViewportWidget::setSecondaryElevationCorrected()
{
  if (0 != m_globe)
  {
    m_globe->setElevationMode(1, tcGlobe::CorrectedElevation);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/// Set the interpolation value.
void tcViewportWidget::setElevationInterpolation(int interpolation)
{
  if (0 != m_globe)
  {
    m_globe->setElevationInterpolation((float)interpolation/100);
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
    updateGL();
  }
}

/*
 * Rendering
 */

void tcViewportWidget::initializeGL()
{
  glClearColor(0.0, 0.3, 0.7, 0.0);
}

void tcViewportWidget::resizeGL(int w, int h)
{
  glViewport(0, 0, (GLint)w, (GLint)h);

  double aspect = (double)w / h;
  m_observer->setupProjection(aspect);
}

void tcViewportWidget::paintGL()
{
  static int maxTexUnits = 0;
  if (maxTexUnits == 0)
  {
    glGetIntegerv(GL_MAX_TEXTURE_UNITS, &maxTexUnits);
    if (maxTexUnits < 3)
    {
      qDebug() << tr("Maximum texture units is %1, but Tecorrec needs at least 3").arg(maxTexUnits);
    }
  }


  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glPolygonMode(GL_FRONT_AND_BACK, m_polygonMode);

  m_observer->setupModelView();

  m_globe->render(m_observer,
                  !m_sliced || m_adaptiveQuality,
                  (m_sliced ? m_slice : 0)
                  );

  float meanRadius = m_globe->meanRadius();

  glLineWidth(3);
  double spotlightExtent = m_observer->range()*0.4;
  // Draw a spotlight for the mouse
  if (m_mouseIntersecting)
  {
    maths::Vector<3,double> vec = m_mouseCoord;
    float altitude = m_globe->altitudeAt(m_mouseCoord);
    glBegin(GL_LINES);
    {
      glColor4f(1.0f, 1.0f, 0.0f, 1.0f);
      glVertex3(vec*meanRadius);
      glVertex3(vec*(meanRadius + altitude));
    }
    {
      glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
      glVertex3(vec*(meanRadius + altitude));
      glColor4f(1.0f, 1.0f, 1.0f, 0.0f);
      glVertex3(vec*(meanRadius + altitude + spotlightExtent));
    }
    glEnd();
  }
  // Draw a spotlight for the focus
  {
    maths::Vector<3,double> vec = m_observer->focus();
    float altitude = m_globe->altitudeAt(m_observer->focus());
    glBegin(GL_LINES);
    {
      glColor4f(1.0f, 1.0f, 0.0f, 1.0f);
      glVertex3(vec*meanRadius);
      glVertex3(vec*(meanRadius + altitude));
    }
    {
      glColor4f(0.0f, 1.0f, 1.0f, 1.0f);
      glVertex3(vec*(meanRadius + altitude));
      glColor4f(1.0f, 1.0f, 1.0f, 0.0f);
      glVertex3(vec*(meanRadius + altitude + spotlightExtent));
    }
    glEnd();
  }
  glLineWidth(1);

  // Draw a fence around the selected region
  for (int i = 0; i < 1; ++i)
  {
    tcGeo coord;
    tcGeo ending;
    float colour[4] = {1.0f, 0.5f, 1.0f, 1.0f};
    if (0 == i && m_mouseSlicing && m_mouseIntersecting)
    {
      coord = m_mouseCoord;
      ending = m_mouseStartCoord;
    }
    else if (1 == i && m_sliced)
    {
      coord = m_slice[0];
      ending = m_slice[1];
      colour[0] = 0.0f;
      colour[3] = 0.5f;
    }
    else
    {
      continue;
    }

    tcGeo delta = ending - coord;
    int nlon = 2 + fabs(delta.lon())*10*meanRadius / m_observer->range();
    int nlat = 2 + fabs(delta.lat())*10*meanRadius / m_observer->range();
    double dlon = delta.lon() / nlon;
    double dlat = delta.lat() / nlat;

    glDisable(GL_CULL_FACE);
    glBegin(GL_TRIANGLE_STRIP);
    for (int i = 0; i <= nlon*2 + nlat*2; ++i)
    {
      float radius = m_globe->radiusAt(coord);
      maths::Vector<3,double> coordVec = coord;
      glColor4fv(colour);
      glVertex3(coordVec*radius);
      glColor4f(1.0f, 1.0f, 1.0f, 0.0f);
      glVertex3(coordVec*(radius + m_observer->range()*0.1));

      if (i < nlon)
      {
        coord.setLon(coord.lon() + dlon);
      }
      else if (i < nlon + nlat)
      {
        coord.setLat(coord.lat() + dlat);
      }
      else if (i < nlon + nlat + nlon)
      {
        coord.setLon(coord.lon() - dlon);
      }
      else
      {
        coord.setLat(coord.lat() - dlat);
      }
    }
    glEnd();
    glEnable(GL_CULL_FACE);
  }
  // Draw the cross section area
  for (int i = 0; i < 2; ++i)
  {
    tcGeo coord;
    tcGeo ending;
    float colour[4] = {1.0f, 0.5f, 1.0f, 1.0f};
    if (0 == i && m_mouseCrossing && m_mouseIntersecting)
    {
      coord = m_mouseCoord;
      ending = m_mouseStartCoord;
    }
    else if (1 == i && m_crossSectioned)
    {
      coord = m_crossSection[0];
      ending = m_crossSection[1];
      colour[0] = 0.0f;
      colour[3] = 0.5f;
    }
    else
    {
      continue;
    }

    tcGeo delta = ending - coord;
    int nseg = 2 + tcGeo::angleBetween(ending, coord)*100*meanRadius / m_observer->range();
    double dlon = delta.lon() / (nseg-1);
    double dlat = delta.lat() / (nseg-1);

    glDisable(GL_CULL_FACE);
    // Lower bit
    tcGeo tmp = coord;
    glBegin(GL_TRIANGLE_STRIP);
    for (int i = 0; i < nseg; ++i)
    {
      float radius = m_globe->radiusAt(tmp);
      maths::Vector<3,double> coordVec = tmp;
      glColor4fv(colour);
      glVertex3(coordVec*radius);
      glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
      glVertex3(coordVec*(radius + m_observer->range()*0.1));

      tmp.setLon(tmp.lon() + dlon);
      tmp.setLat(tmp.lat() + dlat);
    }
    glEnd();
    // Upper bit
    tmp = coord;
    glBegin(GL_TRIANGLE_STRIP);
    glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
    for (int i = 0; i < nseg; ++i)
    {
      float radius = m_globe->radiusAt(tmp);
      maths::Vector<3,double> coordVec = tmp;
      glVertex3(coordVec*(radius + m_observer->range()*0.1));
      glVertex3(coordVec*(radius + m_observer->range()*0.6));

      tmp.setLon(tmp.lon() + dlon);
      tmp.setLat(tmp.lat() + dlat);
    }
    glEnd();
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(2);
    tcSrtmModel* model = m_globe->dem();
    for (int dem = 0; dem < 2; ++dem)
    {
      tmp = coord;
      glBegin(GL_LINE_STRIP);
      for (int i = 0; i < nseg; ++i)
      {
        bool accurate;
        float radius = m_globe->meanRadius() + model[dem].altitudeAt(tmp, true, &accurate);
        maths::Vector<3,double> coordVec = tmp;
        if (accurate)
        {
          glColor4f(0.0f, 0.5f*dem, 0.5f*(1.0f-dem), 1.0f);
        }
        else
        {
          glColor4f(0.5f, 0.5f*dem, 0.5f*(1.0f-dem), 1.0f);
        }
        glVertex3(coordVec*(radius + m_observer->range()*0.3));

        tmp.setLon(tmp.lon() + dlon);
        tmp.setLat(tmp.lat() + dlat);
      }
      glEnd();
    }
    glLineWidth(1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
  }

}

/*
 * Mouse events
 */

void tcViewportWidget::mouseMoveEvent(QMouseEvent* event)
{
  if (m_mouseInteracting)
  {
    QPointF dpos = event->posF() - m_mousePos;
    m_mousePos = event->posF();

    // Pan the scene, adjust the observer focus
    m_observer->moveFocusRelative(dpos.x() / height(), dpos.y() / width());
    m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));
  }

  QString mouseDescription;
  bool ok;
  m_mouseCoord = geoAt(event->posF().x(), event->posF().y(), &ok);
  m_mouseIntersecting = ok;
  if (ok)
  {
    mouseDescription = m_mouseCoord.describe() + " " + tr("%1 m, range: %2 m")
                                                          .arg(m_globe->altitudeAt(m_mouseCoord))
                                                          .arg(tcGeo::angleBetween(m_mouseCoord,m_observer->focus()) * m_globe->meanRadius());
    emit mouseGeoChanged(m_mouseCoord);
  }
  else
  {
    mouseDescription = tr("--");
  }
  emit mouseGeoTextChanged(tr("Focus: %1 %2, Mouse: %3")
                                .arg(m_observer->focus().describe())
                                .arg(tr("%1 m","metres")
                                        .arg(m_globe->altitudeAt(m_observer->focus())))
                                .arg(mouseDescription));

  updateGL();
}

void tcViewportWidget::mousePressEvent(QMouseEvent* event)
{
  if (m_interactionMode == Navigation)
  {
    if (event->button() == Qt::LeftButton)
    {
      m_mouseInteracting = true;
      m_mousePos = event->posF();
    }
    else if (event->button() == Qt::RightButton)
    {
      bool ok;
      m_mouseStartCoord = geoAt(event->posF().x(), event->posF().y(), &ok);
      m_mouseIntersecting = false;
      if (ok)
      {
        m_mouseSlicing = true;
      }
    }
    else if (event->button() == Qt::MidButton)
    {
      bool ok;
      m_mouseStartCoord = geoAt(event->posF().x(), event->posF().y(), &ok);
      m_mouseIntersecting = false;
      if (ok)
      {
        m_mouseCrossing = true;
      }
    }
  }
  else if (m_interactionMode == TexturePoint)
  {
    bool ok;
    tcGeo geoCoord = geoAt(event->posF().x(), event->posF().y(), &ok);
    if (ok)
    {
      maths::Vector<2,float> texturePoint(m_globe->textureCoordOfGeo(geoCoord));
      // need to revert interaction mode first in case slot at other end requests another
      m_interactionMode = Navigation;
      QObject* currentObj = m_texturePointObject;
      const char* currentMember = m_texturePointMember;
      connect(this, SIGNAL(texturePointSelected(const maths::Vector<2,float>&)),
              currentObj, currentMember);
      emit texturePointSelected(texturePoint);
      disconnect(this, SIGNAL(texturePointSelected(const maths::Vector<2,float>&)),
                 currentObj, currentMember);
    }
  }
}

#include <QtDebug>
void tcViewportWidget::mouseReleaseEvent(QMouseEvent* event)
{
  if (m_mouseInteracting)
  {
    m_mouseInteracting = false;
  }
  else if (m_mouseSlicing)
  {
    m_mouseSlicing = false;
    m_sliced = m_mouseIntersecting;
    if (m_mouseIntersecting)
    {
      m_slice[0] = m_mouseStartCoord;
      m_slice[1] = m_mouseCoord;
    }
    updateGL();
  }
  else if (m_mouseCrossing)
  {
    m_mouseCrossing = false;
    if (m_mouseIntersecting)
    {
      setCrossSection(m_mouseStartCoord, m_mouseCoord);
    }
    else
    {
      m_crossSectioned = false;
    }
    updateGL();
  }
}

void tcViewportWidget::wheelEvent(QWheelEvent* event)
{
  float delta = M_PI/180.0 * event->delta()/8;
  delta *= 0.5f;
  if (event->orientation() == Qt::Vertical)
  {
    if (event->modifiers().testFlag(Qt::ControlModifier))
    {
      m_observer->adjustElevation(delta);
    }
    else
    {
      m_observer->adjustRange(-delta);
    }
  }
  else
  {
    m_observer->adjustAzimuth(-delta);
  }
  m_observer->setFocusAltitude(m_globe->radiusAt(m_observer->focus()));

  updateGL();
}
