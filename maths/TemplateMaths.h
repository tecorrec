/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file TemplateMaths.h
 * @brief Template maths functions, some unecessary.
 * @author James Hogan <james@albanarts.com>
 * @note Copyright (C) 2007
 * 
 * @version 1 : From Computer Graphics and Visualisation Open Assessment.
 */

#ifndef _maths_TemplateMaths_h
#define _maths_TemplateMaths_h

#include <cmath>

// Defines template functions for merging fp functions with 1 argument
#define DEFINE_FP1_TEMPLATE_FUNCTIONS_SAFE(NAME, FLOAT, DOUBLE) \
  template <typename T> \
  inline T NAME(T arg) \
  { \
    return arg.NAME(); \
  } \
  template <> inline float NAME<float>(float arg) \
  { \
    return FLOAT(arg); \
  } \
  template <> inline double NAME<double>(double arg) \
  { \
    return DOUBLE(arg); \
  } \

// Defines template functions for merging fp functions with 1 argument
#define DEFINE_FP1_TEMPLATE_FUNCTIONS(NAME, FLOAT, DOUBLE) \
  template <typename T> \
  inline T NAME(T arg); \
  template <> inline float NAME<float>(float arg) \
  { \
    return FLOAT(arg); \
  } \
  template <> inline double NAME<double>(double arg) \
  { \
    return DOUBLE(arg); \
  } \

// Defines template functions for merging fp functions with 2 arguments
#define DEFINE_FP2_TEMPLATE_FUNCTIONS(NAME, FLOAT, DOUBLE) \
  template <typename T> \
  inline T NAME(T arg1, T arg2); \
  template <> inline float NAME<float>(float arg1, float arg2) \
  { \
    return FLOAT(arg1, arg2); \
  } \
  template <> inline double NAME<double>(double arg1, double arg2) \
  { \
    return DOUBLE(arg1, arg2); \
  } \

namespace maths {
  DEFINE_FP1_TEMPLATE_FUNCTIONS_SAFE(sqrt, std::sqrt, std::sqrt)
  
  DEFINE_FP1_TEMPLATE_FUNCTIONS(sin, std::sin, std::sin)
  DEFINE_FP1_TEMPLATE_FUNCTIONS(cos, std::cos, std::cos)
  DEFINE_FP1_TEMPLATE_FUNCTIONS(tan, std::tan, std::tan)
  
  DEFINE_FP1_TEMPLATE_FUNCTIONS(asin, std::asin, std::asin)
  DEFINE_FP1_TEMPLATE_FUNCTIONS(acos, std::acos, std::acos)
  DEFINE_FP1_TEMPLATE_FUNCTIONS(atan, std::atan, std::atan)
  
  DEFINE_FP2_TEMPLATE_FUNCTIONS(atan2, std::atan2, std::atan2)
  
  // Cleanup internal preprocessor definitions.
  #undef DEFINE_FP1_TEMPLATE_FUNCTIONS
  #undef DEFINE_FP2_TEMPLATE_FUNCTIONS
  
  /// Write values into a 2 dimentional vector.
  /**
   * @param T Data type of vector components.
   * @param vec Vector pointer to array of @a T.
   * @param x X coordinate of vector.
   * @param y Y coordinate of vector.
   */
  template <typename T>
  inline T * setVector2(const T * vec, const T x, const T y)
  {
    vec[0] = x;
    vec[1] = y;
    return vec;
  }
  
  /// Write values into a 3 dimentional vector.
  /**
   * @param T Data type of vector components.
   * @param vec Vector pointer to array of @a T.
   * @param x X coordinate of vector.
   * @param y Y coordinate of vector.
   * @param z Z coordinate of vector.
   */
  template <typename T>
  inline T * setVector3(T * const vec, const T x, const T y, const T z)
  {
    vec[0] = x;
    vec[1] = y;
    vec[2] = z;
    return vec;
  }
  
  /// Write values into a 3 dimentional vector.
  /**
   * @param T Data type of vector components.
   * @param vec Vector pointer to array of @a T.
   * @param x X coordinate of vector.
   * @param y Y coordinate of vector.
   * @param z Z coordinate of vector.
   */
  template <typename T>
  inline T * setVector4(T * const vec, const T x, const T y, const T z, const T w)
  {
    vec[0] = x;
    vec[1] = y;
    vec[2] = z;
    vec[3] = w;
    return vec;
  }
  
  
  /// Find the square of a number
  /**
   * Useful when the expression is complex and a macro should be avoided
   */
  template <typename T>
  inline T sqr(T value)
  {
    return value*value;
  }

  /// Zero in different types
  /**
   */
  template <typename T>
  inline T zero()
  {
    return (T)0;
  }
}

#endif // _maths_TemplateMaths_h
