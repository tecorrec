/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file Vector.h
 * @brief Vector of values, designed for spacial coordinate vectors.
 * @author James Hogan <james@albanarts.com>
 * @note Copyright (C) 2007
 * 
 * @version 1 : From Computer Graphics and Visualisation Open Assessment.
 */

#ifndef _maths_Vector_h
#define _maths_Vector_h

#include "TemplateMaths.h"

#include <ostream>
#include <sstream>

namespace maths
{
  
  /// Vector.
  /**
   * @param N int Number of components in the vector.
   * @param T typename Type of the components of the vector.
   */
  template <int N, typename T>
  class Vector
  {
    public:
      T v[N];
      
      enum {
        length = N
      };
      typedef T Scalar;
  
      /// Default constructor (Does not initialise the values!)
      inline Vector()
      {
      }
      
      inline explicit Vector(T value)
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] = value;
        }
      }
      inline Vector(T x, T y)
      {
        v[0] = x;
        v[1] = y;
        if (N > 2)
        {
          for (int i = 2; i < N; ++i)
          {
            v[i] = maths::zero<T>();
          }
        }
      }
      inline Vector(T x, T y, T z)
      {
        v[0] = x;
        v[1] = y;
        if (N > 2)
        {
          v[2] = z;
          if (N > 3)
          {
            for (int i = 3; i < N; ++i)
            {
              v[i] = maths::zero<T>();
            }
          }
        }
      }
      inline Vector(T x, T y, T z, T w)
      {
        v[0] = x;
        v[1] = y;
        if (N > 2)
        {
          v[2] = z;
          if (N > 3)
          {
            v[3] = w;
            if (N > 4)
            {
              for (int i = 4; i < N; ++i)
              {
                v[i] = maths::zero<T>();
              }
            }
          }
        }
      }
      inline explicit Vector(const T * vec)
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] = vec[i];
        }
      }
      template <int M, typename U> inline explicit Vector(const Vector<M,U> & copy)
      {
        int i = 0;
        for (; i < N && i < M; ++i)
        {
          v[i] = (T)copy.v[i];
        }
        for (; i < N; ++i)
        {
          v[i] = maths::zero<T>();
        }
      }
      
      inline const Vector & set()
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] = maths::zero<T>();
        }
        return *this;
      }
      inline const Vector & set(T all)
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] = all;
        }
        return *this;
      }
      inline const Vector & set(T x, T y)
      {
        v[0] = x;
        v[1] = y;
        if (N > 2)
        {
          for (int i = 2; i < N; ++i)
          {
            v[i] = maths::zero<T>();
          }
        }
        return *this;
      }
      inline const Vector & set(T x, T y, T z)
      {
        v[0] = x;
        v[1] = y;
        if (N > 2)
        {
          v[2] = z;
          if (N > 3)
          {
            for (int i = 3; i < N; ++i)
            {
              v[i] = maths::zero<T>();
            }
          }
        }
        return *this;
      }
      inline const Vector & set(T x, T y, T z, T w)
      {
        v[0] = x;
        v[1] = y;
        if (N > 2)
        {
          v[2] = z;
          if (N > 3)
          {
            v[3] = w;
            if (N > 4)
            {
              for (int i = 4; i < N; ++i)
              {
                v[i] = maths::zero<T>();
              }
            }
          }
        }
        return *this;
      }
      inline const Vector & set(const Vector<3,T> xyz, T w)
      {
        v[0] = xyz[0];
        v[1] = xyz[1];
        if (N > 2)
        {
          v[2] = xyz[2];
          if (N > 3)
          {
            v[3] = w;
            if (N > 4)
            {
              for (int i = 4; i < N; ++i)
              {
                v[i] = maths::zero<T>();
              }
            }
          }
        }
        return *this;
      }
      
      /// Setter for specific component.
      template <int X>
      inline void setComponent(const T newValue)
      {
        v[X] = newValue;
      }
      
      /// Getter for specific component.
      template <int X>
      inline T getComponent() const
      {
        return v[X];
      }
      
      /// Concatination operator, append a number.
      inline Vector<N+1, T> operator , (T f) const
      {
        Vector<N+1,T> result;
        for (int i = 0; i < N; ++i)
        {
          result[i] = v[i];
        }
        result[N] = f;
        return result;
      }
      
      /// Concatination operator, prepend a number.
      inline friend Vector<N+1, T> operator , (T f, const Vector & vector)
      {
        Vector<N+1,T> result;
        result[0] = f;
        for (int i = 0; i < N; ++i)
        {
          result[i+1] = vector[i];
        }
        return result;
      }
      
      /// Concatination operator, append a vector.
      template <int M>
      inline Vector<N+M, T> operator , (const Vector<M,T> & vector) const
      {
        Vector<N+M,T> result;
        for (int i = 0; i < N; ++i)
        {
          result[i] = v[i];
        }
        for (int i = 0; i < M; ++i)
        {
          result[N+i] = vector[i];
        }
        return result;
      }
      
      /// Slice out of the vector a reference.
      template <int S, int L>
      inline Vector<L, T> & slice()
      {
        return *(Vector<L,T>*)(v+S);
      }
      
      /// Slice out of the vector a reference.
      template <int S, int L>
      inline const Vector<L, T> & slice() const
      {
        return *(Vector<L,T>*)(v+S);
      }
      
      inline T & operator [] (int index)
      {
        return v[index];
      }
      inline const T & operator [] (int index) const
      {
        return v[index];
      }
  
      inline operator T * ()
      {
        return v;
      }
      inline operator const T * () const
      {
        return v;
      }
      
      inline Vector operator - () const
      {
        Vector ret;
        for (int i = 0; i < N; ++i)
        {
          ret[i] = -v[i];
        }
        return ret;
      }
  
      template <typename U>
      inline Vector & operator += (const Vector<N,U> & rhs)
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] += rhs.v[i];
        }
        return *this;
      }
      template <typename U>
      inline Vector operator + (const Vector<N,U> & rhs) const
      {
        Vector ret = *this;
        return ret += rhs;
      }
  
      inline Vector & operator -= (const Vector & rhs)
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] -= rhs.v[i];
        }
        return *this;
      }
      inline Vector operator - (const Vector & rhs) const
      {
        Vector ret = *this;
        return ret -= rhs;
      }

      inline Vector & operator *= (const T rhs)
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] *= rhs;
        }
        return *this;
      }
      inline Vector operator * (const T rhs) const
      {
        Vector ret = *this;
        return ret *= rhs;
      }
      inline friend Vector operator * (const T lhs, const Vector & rhs)
      {
        return rhs * lhs;
      }
  
      /// Dot product
      inline T operator * (const Vector & rhs) const
      {
        T fDot = maths::zero<T>();
        for (int i = 0; i < N; ++i)
        {
          fDot += v[i]*rhs.v[i];
        }
        return fDot;
      }
  
      /// Dot quotient
      inline T operator / (const Vector & rhs) const
      {
        // a = B/C = (B*C)/(C*C)
        return operator * (rhs) / rhs.sqr();
      }
  
      inline Vector & operator /= (const T rhs)
      {
        for (int i = 0; i < N; ++i)
        {
          v[i] /= rhs;
        }
        return *this;
      }
      inline Vector operator / (const T rhs) const
      {
        Vector ret = *this;
        return ret /= rhs;
      }
      inline friend Vector operator / (const T lhs, const Vector & rhs)
      {
        return rhs * (lhs / rhs.sqr());
      }
  
      inline T sqr() const
      {
        return tsqr<T>();
      }
      template <typename U>
      inline U tsqr() const
      {
        U ret = maths::zero<U>();
        for (int i = 0; i < N; ++i)
        {
          ret += maths::sqr<U>(v[i]);
        }
        return ret;
      }
      
      inline Vector inv() const
      {
        return *this / sqr();
      }
  
      inline T mag() const
      {
        return tmag<T>();
      }
      template <typename U>
      inline U tmag() const
      {
        return maths::sqrt<U>(tsqr<U>());
      }
  

      inline Vector & normalize()
      {
        return operator /= (mag());
      }
      inline Vector normalized() const
      {
        return operator / (mag());
      }
      inline Vector & resize(T tLength)
      {
        return operator *= (tLength / mag());
      }
      inline Vector resized(T tLength) const
      {
        return operator * (tLength / mag());
      }
      
      
      // predicates
      
      /// Equality
      inline bool operator == (const Vector & rhs) const 
      {
        for (int i = 0; i < N; ++i)
        {
          if (v[i] != rhs.v[i])
          {
            return false;
          }
        }
        return true;
      }
      /// Nonequality
      inline bool operator != (const Vector & rhs) const
      {
        for (int i = 0; i < N; ++i)
        {
          if (v[i] != rhs.v[i])
          {
            return true;
          }
        }
        return false;
      }
      
      /// Compare the magnitude of vectors.
      inline bool operator < (const Vector & rhs) const
      {
        return sqr() < rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator < (const T & rhs) const
      {
        return sqr() < rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator < (const T & lhs, const Vector & rhs)
      {
        return lhs*lhs < rhs.sqr();
      }
      /// Compare the magnitude of vectors.
      inline bool operator <= (const Vector & rhs) const
      {
        return sqr() <= rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator <= (const T & rhs) const
      {
        return sqr() <= rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator <= (const T & lhs, const Vector & rhs)
      {
        return lhs*lhs <= rhs.sqr();
      }
      /// Compare the magnitude of vectors.
      inline bool operator > (const Vector & rhs) const
      {
        return sqr() > rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator > (const T & rhs) const
      {
        return sqr() > rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator > (const T & lhs, const Vector & rhs)
      {
        return lhs*lhs > rhs.sqr();
      }
      /// Compare the magnitude of vectors.
      inline bool operator >= (const Vector & rhs) const
      {
        return sqr() >= rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator >= (const T & rhs) const
      {
        return sqr() >= rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator >= (const T & lhs, const Vector & rhs)
      {
        return lhs*lhs >= rhs.sqr();
      }
      
      /// Find whether the vector zero.
      inline bool zero() const
      {
        for (int i = 0; i < N; ++i)
        {
          if (v[i])
          {
            return false;
          }
        }
        return true;
      }
      
      
      inline operator std::string() const;
  }; // ::maths::Vector
  
  template <int N, typename T>
  inline Vector<N,T> & add (Vector<N,T> & dest, const Vector<N,T> & lhs, const Vector<N,T> & rhs)
  {
    for (int i = 0; i < N; ++i)
    {
      dest.v[i] = lhs.v[i] + rhs.v[i];
    }
    return dest;
  }

  template <int N, typename T>
  inline Vector<N,T> & sub (Vector<N,T> & dest, const Vector<N,T> & lhs, const Vector<N,T> & rhs)
  {
    for (int i = 0; i < N; ++i)
    {
      dest.v[i] = lhs.v[i] - rhs.v[i];
    }
    return dest;
  }

  template <typename T>
  inline Vector<3,T> & cross (Vector<3,T> & dest, const Vector<3,T> & lhs, const Vector<3,T> & rhs)
  {
    dest.v[0] = lhs.v[1]*rhs.v[2] - lhs.v[2]*rhs.v[1];
    dest.v[1] = lhs.v[2]*rhs.v[0] - lhs.v[0]*rhs.v[2];
    dest.v[2] = lhs.v[0]*rhs.v[1] - lhs.v[1]*rhs.v[0];
    return dest;
  }

  template <typename T>
  inline Vector<3,T> cross (const Vector<3,T> & lhs, const Vector<3,T> & rhs)
  {
    Vector<3, T> dest;
    dest.v[0] = lhs.v[1]*rhs.v[2] - lhs.v[2]*rhs.v[1];
    dest.v[1] = lhs.v[2]*rhs.v[0] - lhs.v[0]*rhs.v[2];
    dest.v[2] = lhs.v[0]*rhs.v[1] - lhs.v[1]*rhs.v[0];
    return dest;
  }

  template <typename T>
  inline Vector<4,T> homogenous(Vector<3,T> & vec, T homogenousness = (T)1)
  {
    Vector<4,T> ret(vec);
    ret[3] = homogenousness;
    return ret;
  }
  
  /// Get the distance from a plane.
  template <typename T>
  inline T plane(const Vector<4, T> & plane, const Vector<3, T> & point)
  {
    return plane[0]*point[0] + plane[1]*point[1] + plane[2]*point[2] + plane[3];
  }
  

  /// Write a vector to an output stream.
  template <int N, typename T>
  inline std::ostream & operator << (std::ostream & out, const Vector<N,T> & v)
  {
    out << "(" << v[0];
    for (int i = 1; i < N; ++i)
    {
      out << ", " << v[i];
    }
    return out << ")";
  }
  /// Convert to a string using the above stream operator
  template <int N, typename T>
  inline Vector<N,T>::operator std::string() const
  {
    std::stringstream ss;
    ss << v;
    return ss.str();
  }
  
  /// Basic function to create a one dimentional vector (scalar) which can then be expanded by concatination.
  template <typename T>
  inline maths::Vector<1,T> Scalar(const T a)
  {
    return maths::Vector<1,T>(a);
  }

} // ::maths

#endif // _maths_vector_h
