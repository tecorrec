/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#ifndef _maths_glVector_h
#define _maths_glVector_h

/*
 * maths::gl::vec.h
 *
 * Inline function helpers for maths::gl::vec<n><t> classes (from maths library)
 *  These are overloaded to take multiple data types
 */

#include <Vector.h>
#include <GL/gl.h>

namespace maths {
  namespace gl {
    // Make use of maths library vector classes
    typedef maths::Vector<2,GLfloat>  vec2f;
    typedef maths::Vector<3,GLfloat>  vec3f;
    typedef maths::Vector<4,GLfloat>  vec4f;
    typedef maths::Vector<2,GLdouble> vec2d;
    typedef maths::Vector<3,GLdouble> vec3d;
    typedef maths::Vector<4,GLdouble> vec4d;
  } // ::maths::gl
} // ::maths

typedef maths::gl::vec2f GLvec2f;
typedef maths::gl::vec3f GLvec3f;
typedef maths::gl::vec4f GLvec4f;
typedef maths::gl::vec2d GLvec2d;
typedef maths::gl::vec3d GLvec3d;
typedef maths::gl::vec4d GLvec4d;

// Send a two dimentional vertex to OpenGL
inline void glVertex2(const maths::gl::vec2f & v)     { glVertex2fv(v); }
inline void glVertex2(const maths::gl::vec2d & v)     { glVertex2dv(v); }

// Send a three dimentional vertex to OpenGL
inline void glVertex3(const maths::gl::vec3f & v)     { glVertex3fv(v); }
inline void glVertex3(const maths::gl::vec3d & v)     { glVertex3dv(v); }

// Send a four dimentional vertex to OpenGL
inline void glVertex4(const maths::gl::vec4f & v)     { glVertex4fv(v); }
inline void glVertex4(const maths::gl::vec4d & v)     { glVertex4dv(v); }

// Send a normal vector to OpenGL
inline void glNormal(const maths::gl::vec3f & v)     { glNormal3fv(v); }
inline void glNormal(const maths::gl::vec4f & v)     { glNormal3fv(v); }
inline void glNormal(const maths::gl::vec3d & v)     { glNormal3dv(v); }
inline void glNormal(const maths::gl::vec4d & v)     { glNormal3dv(v); }

// Send a two dimentional texture coordinate to OpenGL
inline void glTexCoord2(const maths::gl::vec2f & v)   { glTexCoord2fv(v); }
inline void glTexCoord2(const maths::gl::vec2d & v)   { glTexCoord2dv(v); }

// Send a three dimentional texture coordinate to OpenGL
inline void glTexCoord3(const maths::gl::vec3f & v)   { glTexCoord3fv(v); }
inline void glTexCoord3(const maths::gl::vec3d & v)   { glTexCoord3dv(v); }

// Send a two dimentional mulitexture coordinate to OpenGL
inline void glMultiTexCoord2(GLenum target, const maths::gl::vec2f & v)   { glMultiTexCoord2fv(target, v); }
inline void glMultiTexCoord2(GLenum target, const maths::gl::vec2d & v)   { glMultiTexCoord2dv(target, v); }

// Send a three dimentional mulitexture coordinate to OpenGL
inline void glMultiTexCoord3(GLenum target, const maths::gl::vec3f & v)   { glMultiTexCoord3fv(target, v); }
inline void glMultiTexCoord3(GLenum target, const maths::gl::vec3d & v)   { glMultiTexCoord3dv(target, v); }

// Translate the modelview matrix by a vector
inline void glTranslate(const maths::gl::vec2f & v,   GLfloat  z = 0.0f) { glTranslatef(v[0], v[1], z);    }
inline void glTranslate(const maths::gl::vec3f & v)                      { glTranslatef(v[0], v[1], v[2]); }
inline void glTranslate(const maths::gl::vec4f & v)                      { glTranslatef(v[0], v[1], v[2]); }
inline void glTranslate(const maths::gl::vec2d & v,   GLdouble z = 0.0 ) { glTranslated(v[0], v[1], z);    }
inline void glTranslate(const maths::gl::vec3d & v)                      { glTranslated(v[0], v[1], v[2]); }
inline void glTranslate(const maths::gl::vec4d & v)                      { glTranslated(v[0], v[1], v[2]); }

// Rotate the modelview matrix rad radians about axis
inline void glRotate(const float rad, const maths::gl::vec3f & axis)  { glRotatef(rad*57.29577951f, axis[0], axis[1], axis[2]); }
inline void glRotate(const double rad, const maths::gl::vec3d & axis) { glRotated(rad*57.29577951, axis[0], axis[1], axis[2]); }

#endif
