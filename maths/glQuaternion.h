/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/*
 * glQuaternion.h
 *
 * Inline function helpers for GLquaternion<t> classes (from maths library)
 *  These are overloaded to take multiple data types
 *  Header for basic data types is glBasic.h
 *
 */

// Stop multiple inclusions
#ifndef _maths_glQuaternion_h_
#define _maths_glQuaternion_h_

// Maths Library - Quaternion Classes
#include "Quaternion.h"

namespace maths {
  namespace gl {
    // Make use of maths library quaternion classes
    typedef maths::Quaternion<float> quatf;
    typedef maths::Quaternion<double> quatd;
  }
}

typedef maths::gl::quatf GLquatf;
typedef maths::gl::quatd GLquatd;

// Rotate the modelview matrix by a quaternion
inline void glRotate(const maths::gl::quatf & q)
{
  // Convert the quaternion data into simple axis & angle so that it fits in glRotatef
  maths::gl::quatf Q = q.ToAxisAngle();
  // Convert Q.w into degrees (from radians) before passing into OpenGL
  glRotatef(57.295779513f * Q[3], Q[0], Q[1], Q[2]);
}

// Rotate the modelview matrix by a quaternion
inline void glRotate(const maths::gl::quatd & q)
{
  // Convert the quaternion data into simple axis & angle so that it fits in glRotatef
  maths::gl::quatd Q = q.ToAxisAngle();
  // Convert Q.w into degrees (from radians) before passing into OpenGL
  glRotated(57.295779513 * Q[3], Q[0], Q[1], Q[2]);
}

#endif
