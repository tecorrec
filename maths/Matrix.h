/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file Matrix.h
 * @brief Square matrix class.
 * @author James Hogan (james@albanarts.com)
 * @note Copyright (C) 2007
 * 
 * @version 1 : From Computer Graphics and Visualisation Open Assessment.
 */

#ifndef _maths_Matrix_h
#define _maths_Matrix_h

#include "Vector.h"

// For memcpy
#include <cstring>

namespace maths
{
  // This isn't always used with matricies. include Quaternion.h if you wish to use it.
  template <typename T>
      class Quaternion;
  
  template <int N, typename T>
      class Matrix;
      
  /// Get an arbitrary matrix where the z axis points in the direction of Z.
  Matrix<3,float> getTbnMatrix(Vector<3, float> vZ);
  
  Matrix<4,float> RotateRadMatrix44(char axis, float rad);
  Matrix<4,float> RotateRadMatrix44(const maths::Vector<3,float> &axis, float rad);
  Matrix<4,float> TranslateMatrix44(const maths::Vector<3,float> & v);
  Matrix<4,float> ScaleMatrix44(const maths::Vector<4,float> & v);
  inline Matrix<4,float> ScaleMatrix44(const maths::Vector<3,float> & v);
  
  /// Square tranformation matrix.
  /**
   * The matrix is stored in a column major fashion so as to be directly.
   *  compatible with OpenGL
   * @param N int Size of width and height of the matrix.
   * @param T typename Type of the components of the matrix.
   */
  template <int N, typename T>
      class Matrix
  {
    public:
      /// Columns
      Vector<N, T> col[N];
  
      /// Default constructor (does not initialise values).
      inline Matrix()
      {
        // let the vector constructor deal with the initialisation
      }
      
      /// Diagonal matrix constructor
      inline explicit Matrix(T diagonal)
      {
        for (int i = 0; i < N; ++i) {
          for (int j = 0; j < i; ++j) {
            col[i][j] = col[j][i] = maths::zero<T>();
          }
          col[i][i] = diagonal;
        }
      }
      
      /// Diagonal matrix constructor
      inline explicit Matrix(Vector<N, T> diagonal)
      {
        for (int i = 0; i < N; ++i) {
          for (int j = 0; j < i; ++j) {
            col[i][j] = col[j][i] = maths::zero<T>();
          }
          col[i][i] = diagonal[i];
        }
      }
      
      inline Matrix(Vector<N, T> x, Vector<N, T> y)
      {
        col[0] = x;
        col[1] = y;
        if (N > 2) {
          for (int i = 2; i < N; ++i) {
            col[i] = (Vector<N, T>)maths::zero<T>();
          }
        }
      }
      inline Matrix(Vector<N, T> x, Vector<N, T> y, Vector<N, T> z)
      {
        col[0] = x;
        col[1] = y;
        if (N > 2) {
          col[2] = z;
          if (N > 3) {
            for (int i = 3; i < N; ++i) {
              col[i] = (Vector<N, T>)maths::zero<T>();
            }
          }
        }
      }
      inline Matrix(Vector<N, T> x, Vector<N, T> y, Vector<N, T> z, Vector<N, T> w)
      {
        col[0] = x;
        col[1] = y;
        if (N > 2) {
          col[2] = z;
          if (N > 3) {
            col[3] = w;
            if (N > 4) {
              for (int i = 4; i < N; ++i) {
                col[i] = (Vector<N, T>)maths::zero<T>();
              }
            }
          }
        }
      }
  
      /// Copy the matrix directly from an array.
      /**
       * @param vec Pointer to array of values in column major format.
       */
      inline explicit Matrix(const T * vec)
      {
        std::memcpy(&col[0][0], vec, sizeof(T)*(N*N));
      }
  
      /// Copy the matrix from an array of vectors.
      /**
       * @param columns Pointer to array of column vectors.
       */
      inline explicit Matrix(const Vector<N, T> * columns)
      {
        std::memcpy(col, columns.col, sizeof(T)*(N*N));
      }
      /// Convert quaternion to matrix
      explicit Matrix(const Quaternion<T> &);
      
      /// From something else
      /**
       * Basically we want to prevent accidental double conversions between types.
       */
      template <typename U>
          explicit Matrix(const U &);
  
      /// Convert, downsize or upsize another matrix.
      /**
       * If the matrix is being downsized, the extreme rows and columns are
       *  ignored.
       * If the matrix is being upsized, the extreme rows and columns are filled
       *  with the identity matrix.
       * @param copy The matrix object to construct from.
       */
      template <int M, typename U> inline explicit Matrix(const Matrix<M,U> & copy)
      {
        const int copyRange = (N < M ? N : M);
    // Copy as many columns as possible from copy
        int i = 0;
        for (; i < copyRange; ++i) {
      // Copy as many values as possible from the column
          int j = 0;
          for (; j < copyRange; ++j)
            col[i][j] = (T)copy.col[i][j];
          if (M < N) {
        // Generate any remaining values from the identity matrix.
            for (; j < N; ++j)
              col[i][j] = (i==j ? (T)1 : maths::zero<T>());
          }
        }
        if (M < N) {
      // Generate any remaining columns from the identity matrix.
          for (; i < N; ++i) {
            for (int j = 0; j < N; ++j)
              col[i][j] = (i==j ? (T)1 : maths::zero<T>());
          }
        }
      }
  
      /// Index a column in the matrix.
      /**
       * @param columnIndex The index of the column starting at 0.
       * @return The column as a vector.
       */
      inline Vector<N, T> & operator [] (int columnIndex)
      {
        return col[columnIndex];
      }
      /// Index a column in the matrix.
      /**
       * @param columnIndex The index of the column starting at 0.
       * @return The column as a vector.
       */
      inline const Vector<N, T> & operator [] (int columnIndex) const
      {
        return col[columnIndex];
      }
  
      /// Get a pointer to the values of the matrix.
      /**
       * @return Pointer to the values of the array in column major format.
       */
      inline operator T * ()
      {
        return (T*)col;
      }
      /// Get a pointer to the values of the matrix.
      /**
       * @return Pointer to the values of the array in column major format.
       */
      inline operator const T * () const
      {
        return (const T*)col;
      }
  
      inline Matrix & operator += (const Matrix & rhs)
      {
        for (int i = 0; i < N; ++i)
          col[i] += rhs.col[i];
        return *this;
      }
      inline Matrix operator + (const Matrix & rhs) const
      {
        Matrix ret = *this;
        return ret += rhs;
      }
  
      inline Matrix & operator -= (const Matrix & rhs)
      {
        for (int i = 0; i < N; ++i)
          col[i] -= rhs.col[i];
        return *this;
      }
      inline Matrix operator - (const Matrix & rhs) const
      {
        Matrix ret = *this;
        return ret -= rhs;
      }

      inline Matrix & operator *= (const T rhs)
      {
        for (int i = 0; i < N; ++i)
          col[i] *= rhs;
        return *this;
      }
      inline Matrix operator * (const T rhs) const
      {
        Matrix ret = *this;
        return ret *= rhs;
      }
      
      inline friend Matrix operator * (const T lhs, const Matrix & rhs)
      {
        return rhs*lhs;
      }
      
      template <typename U>
      inline friend Vector<N,U> operator * (const Matrix & lhs, const Vector<N,U> & rhs)
      {
        Vector<N,U> ret;
        for (int row = 0; row < N; ++row) {
          ret[row] = maths::zero<U>();
          for (int i = 0; i < N; ++i) {
            ret[row] += lhs[i][row] * rhs[i];
          }
        }
        return ret;
      }
      template <typename U>
      inline friend Vector<N,U> operator * (const Vector<N,U> & lhs, const Matrix & rhs)
      {
        Vector<N,U> ret;
        for (int col = 0; col < N; ++col) {
          ret[col] = maths::zero<T>();
          for (int i = 0; i < N; ++i) {
            ret[col] += lhs[i] * rhs[col][i];
          }
        }
        return ret;
      }
  
      inline friend Matrix operator * (const Matrix & lhs, const Matrix & rhs)
      {
        Matrix ret;
        for (int col = 0; col < N; ++col) {
          for (int row = 0; row < N; ++row) {
            T val = maths::zero<T>();
            for (int i = 0; i < N; ++i) {
              val += lhs[i][row] * rhs[col][i];
            }
            ret[col][row] = val;
          }
        }
        return ret;
      }
      inline Matrix & operator *= (const Matrix & rhs)
      {
        return (*this = *this * rhs);
      }
  
      inline Matrix & operator /= (const T rhs)
      {
        for (int i = 0; i < N; ++i)
          col[i] /= rhs;
        return *this;
      }
      inline Matrix operator / (const T rhs) const
      {
        Matrix ret = *this;
        return ret /= rhs;
      }
  
      /// Make the matrix an identity matrix.
      inline Matrix & identity()
      {
        for (int i = 0; i < N; ++i) {
          for (int j = 0; j < i; ++j) {
            col[i][j] = col[j][i] = maths::zero<T>();
          }
          col[i][i] = 1;
        }
        return *this;
      }
  
      /// Transpose the matrix.
      inline Matrix & transpose()
      {
        // go through lower left cells and swap with upper right
        for (int i = 1; i < N; ++i) {
          for (int j = 0; j < i; ++j) {
            T temp = col[i][j];
            col[i][j] = col[j][i];
            col[j][i] = temp;
          }
        }
        return *this;
      }
  
      /// Get the transposed matrix.
      inline Matrix transposed() const
      {
        Matrix ret;
        for (int i = 0; i < N; ++i) {
          for (int j = 0; j < N; ++j) {
            ret[j][i] = col[i][j];
          }
        }
        return ret;
      }
  
      /// Invert the matrix.
      Matrix & invert(bool* singular = 0);
  
      /// Get the inverted matrix.
      inline Matrix inverted(bool* singular = 0) const
      {
        Matrix ret = *this;
        return ret.invert(singular);
      }
  }; // ::maths::Matrix
  
  inline Matrix<4,float> ScaleMatrix44(const maths::Vector<3,float> & v)
  {
    return ScaleMatrix44((v, 1.0f));
  }
  
  template <int N, typename T>
  inline maths::Matrix<N,T> outer(const maths::Vector<N,T> & vertical, const maths::Vector<N,T> & horizontal)
  {
    maths::Matrix<N,T> result;
    for (int i = 0; i < N; ++i) {
      result[i] = horizontal[i] * vertical;
    }
    return result;
  }
} // ::maths

#endif
