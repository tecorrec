/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file VarVector.h
 * @brief Resizable vector of values, designed for spacial coordinate vectors.
 * @author James Hogan <james@albanarts.com>
 * @note Copyright (C) 2008
 */

#ifndef _maths_VarVector_h
#define _maths_VarVector_h

#include "TemplateMaths.h"
#include "Vector.h"

#include <QtGlobal>

#include <ostream>
#include <sstream>

namespace maths
{
  
  /// VarVector.
  /**
   * @param N int Number of components in the vector.
   * @param T typename Type of the components of the vector.
   */
  template <typename T>
  class VarVector
  {
    private:
      int m_length;
      T* v;

    public:
      typedef T Scalar;
  
      /// Default constructor (zero length, use setLength before doing anything)
      inline explicit VarVector()
      : m_length(0)
      , v(0)
      {
      }
  
      /// Default constructor (Does not initialise the values!)
      inline explicit VarVector(int components)
      : m_length(components)
      , v(new T[components])
      {
      }
      
      inline VarVector(int components, T value)
      : m_length(components)
      , v(new T[components])
      {
        for (int i = 0; i < m_length; ++i)
        {
          v[i] = value;
        }
      }
      inline VarVector(int components, const T * vec)
      : m_length(components)
      , v(new T[components])
      {
        for (int i = 0; i < m_length; ++i)
        {
          v[i] = vec[i];
        }
      }

      /// Copy constructor
      inline VarVector(const VarVector & copy)
      : m_length(copy.m_length)
      , v(new T[m_length])
      {
        for (int i = 0; i < m_length; ++i)
        {
          v[i] = copy[i];
        }
      }
      template <typename U> inline explicit VarVector(const VarVector<U> & copy)
      : m_length(copy.m_length)
      , v(new T[m_length])
      {
        for (int i = 0; i < m_length; ++i)
        {
          v[i] = copy[i];
        }
      }
      template <int M, typename U> inline explicit VarVector(const Vector<M,U> & copy)
      : m_length(M)
      , v(new T[M])
      {
        for (int i = 0; i < M; ++i)
        {
          v[i] = copy[i];
        }
      }

      /// Destructor
      inline ~VarVector()
      {
        delete [] v;
      }

      /// Get the length of the vector.
      int length() const
      {
        return m_length;
      }

      /// Resize of the vector (values are lost).
      void setLength(int newLength)
      {
        if (newLength != m_length)
        {
          Q_ASSERT(newLength > 0);
          delete [] v;
          m_length = newLength;
          v = new T [newLength];
        }
      }

      /// Assignment operator.
      VarVector& operator = (const VarVector& other)
      {
        setLength(other.m_length);
        for (int i = 0; i < m_length; ++i)
        {
          v[i] = other.v[i];
        }
        return *this;
      }

      /// Assignment operator.
      template <typename U>
      VarVector& operator = (const VarVector<U>& other)
      {
        setLength(other.m_length);
        for (int i = 0; i < m_length; ++i)
        {
          v[i] = other.v[i];
        }
        return *this;
      }
      
      /// Setter for specific component.
      template <int X>
      inline void setComponent(const T newValue)
      {
        v[X] = newValue;
      }
      
      /// Getter for specific component.
      template <int X>
      inline T getComponent() const
      {
        return v[X];
      }
      
      inline T & operator [] (int index)
      {
        return v[index];
      }
      inline const T & operator [] (int index) const
      {
        return v[index];
      }
  
      inline operator T * ()
      {
        return v;
      }
      inline operator const T * () const
      {
        return v;
      }
      
      inline VarVector operator - () const
      {
        VarVector ret(m_length);
        for (int i = 0; i < m_length; ++i)
        {
          ret[i] = -v[i];
        }
        return ret;
      }
  
      template <typename U>
      inline VarVector & operator += (const VarVector<U> & rhs)
      {
        Q_ASSERT(m_length == rhs.m_length);
        for (int i = 0; i < m_length; ++i)
        {
          v[i] += rhs.v[i];
        }
        return *this;
      }
      template <typename U>
      inline VarVector operator + (const VarVector<U> & rhs) const
      {
        VarVector ret = *this;
        return ret += rhs;
      }
  
      inline VarVector & operator -= (const VarVector & rhs)
      {
        Q_ASSERT(m_length == rhs.m_length);
        for (int i = 0; i < m_length; ++i)
        {
          v[i] -= rhs.v[i];
        }
        return *this;
      }
      inline VarVector operator - (const VarVector & rhs) const
      {
        VarVector ret = *this;
        return ret -= rhs;
      }

      inline VarVector & operator *= (const T rhs)
      {
        for (int i = 0; i < m_length; ++i)
        {
          v[i] *= rhs;
        }
        return *this;
      }
      inline VarVector operator * (const T rhs) const
      {
        VarVector ret = *this;
        return ret *= rhs;
      }
      inline friend VarVector operator * (const T lhs, const VarVector & rhs)
      {
        return rhs * lhs;
      }
  
      /// Dot product
      inline T operator * (const VarVector & rhs) const
      {
        Q_ASSERT(m_length == rhs.m_length);
        T fDot = maths::zero<T>();
        for (int i = 0; i < m_length; ++i)
        {
          fDot += v[i]*rhs.v[i];
        }
        return fDot;
      }
  
      /// Dot quotient
      inline T operator / (const VarVector & rhs) const
      {
        // a = B/C = (B*C)/(C*C)
        return operator * (rhs) / rhs.sqr();
      }
  
      inline VarVector & operator /= (const T rhs)
      {
        for (int i = 0; i < m_length; ++i)
        {
          v[i] /= rhs;
        }
        return *this;
      }
      inline VarVector operator / (const T rhs) const
      {
        VarVector ret = *this;
        return ret /= rhs;
      }
      inline friend VarVector operator / (const T lhs, const VarVector & rhs)
      {
        return rhs * (lhs / rhs.sqr());
      }
  
      inline T sqr() const
      {
        return tsqr<T>();
      }
      template <typename U>
      inline U tsqr() const
      {
        U ret = maths::zero<U>();
        for (int i = 0; i < m_length; ++i)
        {
          ret += maths::sqr<U>(v[i]);
        }
        return ret;
      }
      
      inline VarVector inv() const
      {
        return *this / sqr();
      }
  
      inline T mag() const
      {
        return tmag<T>();
      }
      template <typename U>
      inline U tmag() const
      {
        return maths::sqrt<U>(tsqr<U>());
      }
  

      inline VarVector & normalize()
      {
        return operator /= (mag());
      }
      inline VarVector normalized() const
      {
        return operator / (mag());
      }
      inline VarVector & resize(T tLength)
      {
        return operator *= (tLength / mag());
      }
      inline VarVector resized(T tLength) const
      {
        return operator * (tLength / mag());
      }
      
      
      // predicates
      
      /// Equality
      inline bool operator == (const VarVector & rhs) const 
      {
        Q_ASSERT(m_length == rhs.m_length);
        for (int i = 0; i < m_length; ++i)
        {
          if (v[i] != rhs.v[i])
          {
            return false;
          }
        }
        return true;
      }
      /// Nonequality
      inline bool operator != (const VarVector & rhs) const
      {
        Q_ASSERT(m_length == rhs.m_length);
        for (int i = 0; i < m_length; ++i)
        {
          if (v[i] != rhs.v[i])
          {
            return true;
          }
        }
        return false;
      }
      
      /// Compare the magnitude of vectors.
      inline bool operator < (const VarVector & rhs) const
      {
        return sqr() < rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator < (const T & rhs) const
      {
        return sqr() < rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator < (const T & lhs, const VarVector & rhs)
      {
        return lhs*lhs < rhs.sqr();
      }
      /// Compare the magnitude of vectors.
      inline bool operator <= (const VarVector & rhs) const
      {
        return sqr() <= rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator <= (const T & rhs) const
      {
        return sqr() <= rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator <= (const T & lhs, const VarVector & rhs)
      {
        return lhs*lhs <= rhs.sqr();
      }
      /// Compare the magnitude of vectors.
      inline bool operator > (const VarVector & rhs) const
      {
        return sqr() > rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator > (const T & rhs) const
      {
        return sqr() > rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator > (const T & lhs, const VarVector & rhs)
      {
        return lhs*lhs > rhs.sqr();
      }
      /// Compare the magnitude of vectors.
      inline bool operator >= (const VarVector & rhs) const
      {
        return sqr() >= rhs.sqr();
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline bool operator >= (const T & rhs) const
      {
        return sqr() >= rhs*rhs;
      }
      /// Compare the magnitude of the vector with a scalar magnitude.
      inline friend bool operator >= (const T & lhs, const VarVector & rhs)
      {
        return lhs*lhs >= rhs.sqr();
      }
      
      /// Find whether the vector zero.
      inline bool zero() const
      {
        for (int i = 0; i < m_length; ++i)
        {
          if (v[i])
          {
            return false;
          }
        }
        return true;
      }
      
      inline operator std::string() const;
  }; // ::maths::VarVector
  
  /// Write a vector to an output stream.
  template <typename T>
  inline std::ostream & operator << (std::ostream & out, const VarVector<T> & v)
  {
    out << "(" << v[0];
    for (int i = 1; i < v.length(); ++i)
    {
      out << ", " << v[i];
    }
    return out << ")";
  }
  /// Convert to a string using the above stream operator
  template <typename T>
  inline VarVector<T>::operator std::string() const
  {
    std::stringstream ss;
    ss << v;
    return ss.str();
  }
  
} // ::maths

#endif // _maths_VarVector_h
