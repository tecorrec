/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file maths/SplineDefinitions.h
 * @brief Spline types.
 */

#ifndef _FILE_maths_splinedefinitions_h
#define _FILE_maths_splinedefinitions_h

#include "Spline.h"

namespace maths {
  
  /// B-Spline.
  /**
   * - Doesn't intersect control points.
   * - Good continuity (nice and smooooth).
   */
  extern const maths::CubicSplineDefinition<char> bSpline;

  /// Catmull-Rom Spline.
  /**
   * - Intersects control points.
   * - Not as continuous as B-Spline.
   */
  extern const maths::CubicSplineDefinition<char> catmullRomSpline;
  
}

#endif // _FILE_maths_splinedefinitions_h
