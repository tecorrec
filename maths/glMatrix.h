/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/*
 * maths::gl::vec.h
 *
 * Inline function helpers for maths::gl::vec<n><t> classes (from maths library)
 *  These are overloaded to take multiple data types
 *  Header for basic data types is glBasic.h
 *
 */

#ifndef _maths_glMatrix_h
#define _maths_glMatrix_h

// Main matrix header
#include "Matrix.h"

#include <GL/gl.h>

namespace maths {
  namespace gl {
    // Make use of maths library vector classes
    typedef maths::Matrix<2,float>  mat2f;
    typedef maths::Matrix<3,float>  mat3f;
    typedef maths::Matrix<4,float>  mat4f;
    typedef maths::Matrix<2,double> mat2d;
    typedef maths::Matrix<3,double> mat3d;
    typedef maths::Matrix<4,double> mat4d;
  } // ::maths::gl
} // ::maths

typedef maths::gl::mat2f GLmat2f;
typedef maths::gl::mat3f GLmat3f;
typedef maths::gl::mat4f GLmat4f;
typedef maths::gl::mat2d GLmat2d;
typedef maths::gl::mat3d GLmat3d;
typedef maths::gl::mat4d GLmat4d;

inline void glMultMatrix(const maths::gl::mat3f & matrix)
{
  maths::gl::mat4f m(matrix);
  glMultMatrixf(&m.col[0][0]);
}
inline void glMultMatrix(const maths::gl::mat3d & matrix)
{
  maths::gl::mat4d m(matrix);
  glMultMatrixd(&m.col[0][0]);
}
inline void glMultMatrix(const maths::gl::mat4f & matrix)
{
  glMultMatrixf(&matrix.col[0][0]);
}
inline void glMultMatrix(const maths::gl::mat4d & matrix)
{
  glMultMatrixd(&matrix.col[0][0]);
}

inline void glGetv(GLenum pname, maths::gl::mat4f* matrix)
{
  glGetFloatv(pname, (GLfloat*)(*matrix));
}
inline void glGetv(GLenum pname, maths::gl::mat4d* matrix)
{
  glGetDoublev(pname, (GLdouble*)(*matrix));
}

#endif
