/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

#include <QString>
#include <QFile>
#include <QtDebug>
#include <QFileInfo>

#include <cmath>
#include <inttypes.h>

int cpVoids(const QString filename, char* voids)
{
  qDebug() << "Reading" << filename;

  // Find corresponding file in void directory
  QFileInfo fi(filename);
  QString name = fi.fileName();

  QFile srtm(filename);
  if (srtm.open(QIODevice::ReadWrite))
  {
    QString pathFname = QString("%1/%2").arg(voids).arg(name);
    QFile voidfs(pathFname);
    if (voidfs.open(QIODevice::ReadOnly))
    {
      // Check they're the same size
      int len = srtm.size();
      if (voidfs.size() == len)
      {
        QDataStream srtmStream(&srtm);
        QDataStream voidStream(&voidfs);
        srtmStream.setByteOrder(QDataStream::BigEndian);
        voidStream.setByteOrder(QDataStream::BigEndian);
        for (int i = 0; i < len; i += 2)
        {
          // Read value and voidness
          uint16_t srtmness;
          uint16_t voidness;
          srtmStream >> srtmness;
          voidStream >> voidness;

          // Copy voidness over
          srtmness = (srtmness & 0x7FFF) | (voidness & 0x8000);
          srtm.seek(i);
          srtmStream << srtmness;
        }
      }
      else
      {
        qDebug() << "Size mismatch with " << filename << " and " << pathFname;
        return 8;
      }
    }
    else
    {
      qDebug() << "Could not access " << pathFname;
      return 4;
    }
  }
  else
  {
    qDebug() << "Could not access " << filename;
    return 2;
  }
  return 0;
}

int main(int argc, char** argv)
{
  qDebug() << "Welcome to cpVoids";
  int error = 0;
  // Copy void data into HGT files specified
  char * voids = 0;
  for (int i = 1; i < argc; ++i)
  {
    if (0 == strcmp(argv[i], "--voids"))
    {
      // next argument is voids directory
      if (++i < argc)
      {
        voids = argv[i];
        qDebug() << "Using voids from " << voids;
      }
    }
    else
    {
      if (0 == voids)
      {
        qDebug() << "Cannot copy voids into " << argv[i] << ", no void directory specified, use --voids void/dir before HGT files";
        return 1;
      }
      error |= cpVoids(argv[i], voids);
    }
  }
  return error;
}
