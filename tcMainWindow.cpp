/***************************************************************************
 *   This file is part of Tecorrec.                                        *
 *   Copyright 2008 James Hogan <james@albanarts.com>                      *
 *                                                                         *
 *   Tecorrec is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   Tecorrec is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with Tecorrec.  If not, write to the Free Software Foundation,  *
 *   Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.   *
 ***************************************************************************/

/**
 * @file tcMainWindow.h
 * @brief Main application window.
 */

#include "tcMainWindow.h"
#include "tcViewportWidget.h"
#include "tcColourMapWidget.h"
#include <tcGlobe.h>
#include <tcSrtmCache.h>
#include <tcGeoImageData.h>
#include <tcChannelManager.h>
#include <tcChannel.h>
#include <tcChannelConfigWidget.h>
#include <tcExportText.h>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>

#include <QHBoxLayout>
#include <QWidget>
#include <QMenuBar>
#include <QMenu>
#include <QStatusBar>
#include <QDockWidget>
#include <QLabel>
#include <QComboBox>
#include <QRadioButton>
#include <QSlider>
#include <QPushButton>
#include <QButtonGroup>

/*
 * Constructors + destructor
 */

/// Default constructor.
tcMainWindow::tcMainWindow()
: QMainWindow()
, m_viewport(new tcViewportWidget(this))
, m_colourMap(new tcColourMapWidget(QStringList()
                  << tr("Red") << tr("Green") << tr("Blue") << tr("A") << tr("B") << tr("C"), this))
, m_globe(new tcGlobe(6378.137e3))
, m_exportText(0)
{
  m_viewport->setGlobe(m_globe);
  setCentralWidget(m_viewport);

  {
    // Menu bar
    QMenuBar* menus = menuBar();
    {
      QMenu* menuFile = menus->addMenu(tr("File", "The menu"));
      {
        QMenu* menuFileExport = menuFile->addMenu(tr("Export", "The menu"));
        {
          QAction* menuFileExportText = menuFileExport->addAction(tr("Export as Text"),
              this, SLOT(exportText()));
        }
      }
      QMenu* menuView = menus->addMenu(tr("View", "The menu"));
      {
        QMenu* menuViewQuality = menuView->addMenu(tr("Elevation Quality", "The menu"));
        {
          QAction* menuViewQualityAdaptive = menuViewQuality->addAction(tr("Adaptive Resolution"),
              m_viewport, SLOT(setQualityAdaptive()));
          QAction* menuViewQualityFull = menuViewQuality->addAction(tr("Full Resolution"),
              m_viewport, SLOT(setQualityFull()));
        }
        QMenu* menuViewRender = menuView->addMenu(tr("Render Options", "The menu"));
        {
          QAction* menuViewRenderNormal = menuViewRender->addAction(tr("Normal", "Polygon mode"),
              m_viewport, SLOT(setPolygonModeNormal()));
          QAction* menuViewRenderWireframe = menuViewRender->addAction(tr("Wireframe", "Polygon mode"),
              m_viewport, SLOT(setPolygonModeWireframe()));
        }
        QMenu* menuViewRenderColour = menuView->addMenu(tr("Colour Coding", "The menu"));
        {
          QAction* menuViewRenderColourNone = menuViewRenderColour->addAction(tr("No colour coding"),
              m_viewport, SLOT(setColourCodingNone()));
          QAction* menuViewRenderColourElevationSampleAlignment = menuViewRenderColour->addAction(tr("Alignment of elevation samples"),
              m_viewport, SLOT(setColourCodingElevationSampleAlignment()));
        }
      }
    }
  }

  {
    // Status bar
    QStatusBar* statusBar = new QStatusBar(this);
    connect(m_viewport, SIGNAL(mouseGeoTextChanged(const QString&)),
            statusBar,  SLOT(showMessage(const QString&)));
    foreach (tcGeoImageData* imagery, m_globe->imagery())
    {
      connect(imagery->channelManager(), SIGNAL(progressing(const QString&)),
              statusBar, SLOT(showMessage(const QString&)));
    }
    connect(m_globe->dem(), SIGNAL(progressing(const QString&)),
            statusBar, SLOT(showMessage(const QString&)));
    setStatusBar(statusBar);
  }

  QDockWidget* dockElevation;
  {
    // Docker for elevation data settings
    QDockWidget* docker = new QDockWidget(tr("Elevation Data"), this);
    dockElevation = docker;
      QWidget* dockerWidget = new QWidget(docker);
      QVBoxLayout* layout = new QVBoxLayout(dockerWidget);
      docker->setWidget(dockerWidget);
    addDockWidget(Qt::LeftDockWidgetArea, docker);

    // Combo box to select data set
    QStringList elevDataSets = tcSrtmCache::dataSets();
    QString elevTypeNames[2] = {
      tr("Primary DEM"),
      tr("Secondary DEM")
    };
    const char* elevDatasetSlot[2] = { SLOT(setPrimaryElevationDataSet(const QString&)), SLOT(setSecondaryElevationDataSet(const QString&)) };
    const char* elevFlatSlot[2] = { SLOT(setPrimaryElevationFlat()), SLOT(setSecondaryElevationFlat()) };
    const char* elevRawSlot[2] = { SLOT(setPrimaryElevationRaw()), SLOT(setSecondaryElevationRaw()) };
    const char* elevCorrectedSlot[2] = { SLOT(setPrimaryElevationCorrected()), SLOT(setSecondaryElevationCorrected()) };
    for (int i = 0; i < 2; ++i)
    {
      QLabel* elevInterpolationLabel = new QLabel(elevTypeNames[i], dockerWidget);
        layout->addWidget(elevInterpolationLabel);

      QComboBox* elevDataSet = new QComboBox(dockerWidget);
        layout->addWidget(elevDataSet);
        elevDataSet->addItem("");
        foreach (QString dataSet, elevDataSets)
        {
          elevDataSet->addItem(dataSet);
        }
        connect(elevDataSet, SIGNAL(currentIndexChanged(const QString&)), m_viewport, elevDatasetSlot[i]);
        if (-1 != elevDataSet->currentIndex())
        {
          m_viewport->setPrimaryElevationDataSet(elevDataSet->currentText());
        }

      // Radio buttons
      QButtonGroup* elevGroup = new QButtonGroup(dockerWidget);
      QRadioButton* elevFlat = new QRadioButton(tr("Don't Show Elevation (Flat)"), dockerWidget);
        layout->addWidget(elevFlat);
        elevGroup->addButton(elevFlat);
        connect(elevFlat, SIGNAL(pressed()), m_viewport, elevFlatSlot[i]);
      QRadioButton* elevRaw = new QRadioButton(tr("Raw SRTM Elevation Data"), dockerWidget);
        layout->addWidget(elevRaw);
        elevGroup->addButton(elevRaw);
        connect(elevRaw, SIGNAL(pressed()), m_viewport, elevRawSlot[i]);
      QRadioButton* elevCorrected = new QRadioButton(tr("Corrected SRTM Elevation Data"), dockerWidget);
        layout->addWidget(elevCorrected);
        elevGroup->addButton(elevCorrected);
        elevCorrected->setChecked(true);
        connect(elevCorrected, SIGNAL(pressed()), m_viewport, elevCorrectedSlot[i]);

      // Spacer
      layout->addStretch();
    }

    QLabel* elevInterpolationLabel = new QLabel(tr("Interpolate between DEMs"), dockerWidget);
      layout->addWidget(elevInterpolationLabel);
    QSlider* elevCorrectionLevel = new QSlider(Qt::Horizontal, dockerWidget);
      layout->addWidget(elevCorrectionLevel);
      elevCorrectionLevel->setRange(0,100);
      elevCorrectionLevel->setValue(100);
      connect(elevCorrectionLevel, SIGNAL(valueChanged(int)), m_viewport, SLOT(setElevationInterpolation(int)));

    // Spacer
    layout->addStretch();
  }

  {
    // Docker for satellite imagery
    QDockWidget* docker = new QDockWidget(tr("Imagery Data"), this);
      QWidget* dockerWidget = new QWidget(docker);
      QVBoxLayout* layout = new QVBoxLayout(dockerWidget);
      docker->setWidget(dockerWidget);
    tabifyDockWidget(dockElevation, docker);

    // Grid of colours
    layout->addWidget(m_colourMap);
    foreach (tcGeoImageData* imagery, m_globe->imagery())
    {
      const tcChannelManager* manager = imagery->channelManager();
      int numChannels = manager->numChannels();
      m_colourMap->addInputGroupSeparator(imagery->name());
      for (int i = 0; i < numChannels; ++i)
      {
        tcChannel* channel = manager->channel(i);
        m_colourMap->addInputBand(channel->name(), channel->description());
      }
    }
    connect(m_colourMap, SIGNAL(inputBandClicked(int, int)), this, SLOT(configureChannel(int, int)));
    connect(m_colourMap, SIGNAL(inputGroupClicked(int)), m_viewport, SLOT(sunView(int)));
    connect(m_colourMap, SIGNAL(inputBandChanged(int, int, int)), m_viewport, SLOT(setColourMapping(int, int, int)));
    for (int i = 0; i < 3; ++i)
    {
      m_colourMap->setInputBand(i, 2-i);
    }
  }

  {
    // Docker for processing
    QDockWidget* docker = new QDockWidget(tr("Processing"), this);
      QWidget* dockerWidget = new QWidget(docker);
      QVBoxLayout* layout = new QVBoxLayout(dockerWidget);
      docker->setWidget(dockerWidget);
    tabifyDockWidget(dockElevation, docker);

    {
      QwtPlot* plot = new QwtPlot(this);
      layout->addWidget(plot);
      plot->setTitle("This is a test");
      //plot->setAutoLegend(true);
      //plot->setLegendPosition(Qwt::bottom);
      //plot->setAxisTitle(xBottom, "x");
      //plot->setAxisTitle(yLeft, "y");
      QwtPlotCurve* cSin = new QwtPlotCurve("y=sin(x)");
      cSin->setPen(QPen(Qt::red));
      const int points = 500;
      double x[points];
      double sn[points];
      for (int i = 0; i < points; ++i)
      {
        x[i] = (3.0 * 3.14 / double(points))*double(i);
        sn[i] = 2.0*sin(x[i]);
      }
      cSin->setData(x, sn, points);
      cSin->attach(plot);
      plot->replot();
    }

    // Spacer
    layout->addStretch();
  }
}

/// Destructor.
tcMainWindow::~tcMainWindow()
{
  delete m_globe;
}

/*
 * Private slots
 */

/// Show configuration for a channel.
void tcMainWindow::configureChannel(int channel, int group)
{
  tcChannelManager* manager = m_globe->imagery()[group]->channelManager();
  tcChannelConfigWidget* configWidget = manager->channel(channel)->configWidget();
  if (0 != configWidget)
  {
    connect(configWidget, SIGNAL(updated()),
            m_viewport, SLOT(updateGL()));
    connect(configWidget, SIGNAL(newSlice(const tcGeo&, const tcGeo&)),
            m_viewport, SLOT(setSlice(const tcGeo&, const tcGeo&)));
    connect(configWidget, SIGNAL(texturePointRequested(QObject*, const char*)),
            m_viewport, SLOT(texturePointMode(QObject*, const char*)));
    configWidget->setWindowFlags(Qt::Tool | Qt::WindowStaysOnTopHint);
    configWidget->show();
  }
}

/// Export as text.
void tcMainWindow::exportText()
{
  if (0 == m_exportText)
  {
    m_exportText = new tcExportText(m_globe->imagery(), this);
  }
  m_exportText->setWindowFlags(Qt::Dialog | Qt::WindowStaysOnTopHint);
  m_exportText->show();
}
